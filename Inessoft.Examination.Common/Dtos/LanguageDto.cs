﻿using NetCoreDI;

namespace Inessoft.Examination.Common.Dtos
{
    public class LanguageDto
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public bool IsCurentLanguage => AmbientContext.Current.Localization.CurrentLanguageCode == Code;

        public static string DefaultLanguage => "ru";
    }
}