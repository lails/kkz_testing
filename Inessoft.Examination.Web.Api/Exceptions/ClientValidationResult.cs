﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Inessoft.Examination.Web.Api.Exceptions
{
    public class ClientValidationResult : ValidationResult
    {
        public ClientValidationResult(ValidationResult validationResult)
            : base(validationResult)
        {
            var clientValidationResult = validationResult as ClientValidationResult;
            if (clientValidationResult != null)
            {
                this.Validator = clientValidationResult.Validator;
                this.Data = clientValidationResult.Data;
            }
        }

        public ClientValidationResult(string errorMessage, IEnumerable<string> memberNames, string validator, object data = null)
            : base(errorMessage, memberNames)
        {
            if (string.IsNullOrEmpty(errorMessage))
            {
                // Note: иначе метод GetValidationResult создаст новый объект ValidationResult
                throw new ArgumentException($"{nameof(ClientValidationResult)}: parameter should not be empty or null", nameof(errorMessage));
            }

            this.Validator = validator;
            this.Data = data;
        }

        public string Validator { get; set; }

        public object Data { get; set; }
    }
}
