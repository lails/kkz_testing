﻿using Inessoft.Examination.Web.Api.Enums;

namespace Inessoft.Examination.Web.Api.Exceptions
{
    public sealed class FaultContract
    {
        public FaultContract(FaultLevel faultLevel, string faultMessage, object faultDetails = null, object error = null)
        {
            this.FaultDetails = faultDetails;
            this.FaultLevel = faultLevel;
            this.FaultMessage = faultMessage;
            this.Error = error;
        }

        public object FaultDetails { get; private set; }

        public FaultLevel FaultLevel { get; private set; }

        public string FaultMessage { get; private set; }

        public object Error { get; private set; }
    }
}
