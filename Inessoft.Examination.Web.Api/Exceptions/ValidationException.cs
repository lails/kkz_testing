using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Inessoft.Examination.Web.Api.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(IEnumerable<ClientValidationResult> errors)
            : base("server_validation_errors")
        {
            this.ValidationMessages = errors.ToList();
        }

        protected ValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public List<ClientValidationResult> ValidationMessages { get; private set; }
    }
}
