using System;
using System.Runtime.Serialization;
using Inessoft.Examination.Web.Api.Enums;

namespace Inessoft.Examination.Web.Api.Exceptions
{
    [Serializable]
    public class FaultException : Exception
    {
        public FaultException(FaultLevel faultLevel, string faultMessage, object faultDetails = null, object error = null)
        {
            this.Fault = new FaultContract(faultLevel, faultMessage, faultDetails, error);
        }

        protected FaultException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public FaultContract Fault { get; private set; }
    }
}
