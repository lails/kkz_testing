﻿using System;
using System.Net;
using Inessoft.Examination.Web.Api.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Inessoft.Examination.Web.Api.Exceptions
{
    public class ApiExceptionFilter : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception == null)
            {
                return;
            }

            var errorDescriptor = GetErrorDescriptor(context.Exception);
            var exceptionResponse = new ObjectResult(errorDescriptor) { StatusCode = (int)errorDescriptor.StatusCode };
            context.Result = exceptionResponse;
            //context.Exception = null;
        }

        private static ErrorDescriptor GetErrorDescriptor(Exception exception)
        {
            var errorDescriptor = new ErrorDescriptor();

            //if (exception is HttpRequestValidationException)
            //{
            //    errorDescriptor.StatusCode = HttpStatusCode.BadRequest;
            //    errorDescriptor.FaultContract = new FaultContract(FaultLevel.Error, Exceptions.PotentiallyDangerousRequest, null, exception.ToString());
            //    return errorDescriptor;
            //}

            if (exception is BusinessLogicException)
            {
                errorDescriptor.StatusCode = HttpStatusCode.BadRequest;
                errorDescriptor.FaultContract = new FaultContract(FaultLevel.Error, exception.Message, null, exception.ToString());
                return errorDescriptor;
            }

            if (exception is FaultException faultException)
            {
                errorDescriptor.StatusCode = HttpStatusCode.BadRequest;
                errorDescriptor.FaultContract = faultException.Fault;
                return errorDescriptor;
            }

            if (exception is ValidationException validationException)
            {
                errorDescriptor.StatusCode = HttpStatusCode.BadRequest;
                errorDescriptor.FaultContract = new FaultContract(FaultLevel.Error, validationException.Message, validationException.ValidationMessages, exception.ToString());
                return errorDescriptor;
            }

            if (exception is NotFoundException notFoundException)
            {
                errorDescriptor.StatusCode = HttpStatusCode.NotFound;
                errorDescriptor.FaultContract = new FaultContract(FaultLevel.Error, notFoundException.Message, null, exception.ToString());
                return errorDescriptor;
            }

            errorDescriptor.StatusCode = HttpStatusCode.InternalServerError;
            errorDescriptor.FaultContract = new FaultContract(FaultLevel.Error, exception.ToString(), null, exception.ToString());
            return errorDescriptor;
        }
    }

    public class ErrorDescriptor
    {
        public HttpStatusCode StatusCode { get; set; }

        public FaultContract FaultContract { get; set; }
    }
}