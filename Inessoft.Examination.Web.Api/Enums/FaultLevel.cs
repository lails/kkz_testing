namespace Inessoft.Examination.Web.Api.Enums
{
    public enum FaultLevel
    {
        None = 0,
        Info = 1,
        Warning = 2,
        Error = 3,
        Fatal = 4
    }
}
