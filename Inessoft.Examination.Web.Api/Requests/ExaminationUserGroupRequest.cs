﻿using System;
using System.Collections.Generic;
using NetCoreIdentityHttpClient.Requests;

namespace Inessoft.Examination.Web.Api.Requests
{
    public class ExaminationUserGroupRequest
    {
        public List<Guid> ExcludeUserIds { get; set; }
        public string Iin { get; set; }
        public string MiddleName { get; set; }
        public Guid? RoleId { get; set; }
        public int Take { get; set; }

        public GetUsersPagedListRequest ToGetUsersPagedListRequest => new GetUsersPagedListRequest
        {
            Skip = 0,
            Take = Take == 0 ? int.MaxValue - 1 : Take,
            Inn = Iin,
            MiddleName = MiddleName,
            RoleId = RoleId
        };
    }
}