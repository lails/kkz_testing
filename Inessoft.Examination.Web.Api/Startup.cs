﻿using Inessoft.Examination.Authentication;
using Inessoft.Examination.BusinessLogic;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain;
using Inessoft.Examination.ExcelDataHandler;
using Inessoft.Examination.Web.Api.Exceptions;
using Inessoft.Examination.Web.Api.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NetCoreCQRS;
using NetCoreDataAccess.UnitOfWork;
using NetCoreDI;
using NetCoreIdentityClientExtensions;
using NetCoreIdentityHttpClient;
using NetCoreLocalization;
using Swashbuckle.AspNetCore.Swagger;
using System.Text;

namespace Inessoft.Examination.Web.Api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment hosting, IHostingEnvironment environment)
        {
            var configFileName = "appsettings.json";

            if (environment.IsProduction())
            {
                configFileName = "appsettings-test.json";
            }

            var builder = new ConfigurationBuilder()
                .SetBasePath(hosting.ContentRootPath)
                .AddJsonFile(configFileName, false, true);

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContextPool<ExaminationDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("ExaminationDBConnetcionString")))
                .AddTransient<DbContext, ExaminationDbContext>()
                .AddTransient<IExecutor, Executor>()
                .AddTransient<IAmbientContext, AmbientContext>()
                .AddTransient<IUnitOfWork, UnitOfWork>()
                .AddTransient<IObjectResolver, ObjectResolver>()
                .AddTransient<INetCoreIdentityHttpClient, NetCoreIdentityHttpClient.NetCoreIdentityHttpClient>()
                .AddTransient<IDateTimeProvider, DateTimeProvider>()
                .AddTransient<IExcelDataHandlersService, ExcelHandlerService>()
                .AddNetCoreLocalizationService();

            services.AddTransient<IConfigurationRoot>(conf => Configuration);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddCors(options =>
            {
                options.AddPolicy("allowAnyOrigin", policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
                options.AddPolicy("localOriginOnly", policy =>
                {
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });

            services
                .AddExaminationCQRSBusinessLogic()
                .AddAuthenticationDependencies();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "InessSoft.Examination.Web.Api", Version = "v1" });
            });

            services
                .AddNetCoreIdentityAuthentication(Configuration);

            services.AddMvc(options =>
            {
                options.Filters.Add(new ApiExceptionFilter());
            });

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);


            //Создается экземпляр в конце этого метода
            var _ = new AmbientContext(services.BuildServiceProvider());
            var __ = new ExaminationAmbientContext(services.BuildServiceProvider(), Configuration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Use(ChangeContextToHttps);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.SetupAngularRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "InessSoft.Examination.Web.Api");
            });

            //накатим миграцию при первом пуске приложения
            using (var db = AmbientContext.Current.Resolver.ResolveObject<ExaminationDbContext>())
            {
                db.Database.Migrate();
            }
        }

        private static RequestDelegate ChangeContextToHttps(RequestDelegate next)
        {
            return async context =>
            {
                context.Request.Scheme = "https";
                await next(context);
            };
        }
    }
}
