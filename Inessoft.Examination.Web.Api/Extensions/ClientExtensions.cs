﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace Inessoft.Examination.Web.Api.Extensions
{
    public static class ClientExtensions
    {
        public static IApplicationBuilder SetupAngularRouting(this IApplicationBuilder app)
        {
            var env = (IHostingEnvironment)app.ApplicationServices.GetService(typeof(IHostingEnvironment));

            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        "default",
                        "{controller=home}/{action=index}/{id?}");

                    routes.MapRoute(
                        "api",
                        "api/{controller}/{action}/{id?}");
                });

            return app;
        }
    }
}
