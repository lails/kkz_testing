﻿using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.BusinessLogic.ExaminationCategorySettings;
using Inessoft.Examination.BusinessLogic.ExaminationSettings;
using Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos;
using Microsoft.AspNetCore.Mvc;
using NetCoreDataAccess.BaseRequests;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/examenSettings")]
    public class ExamenSettingsController : BaseExaminationApiController
    {
        [HttpPost]
        [Route("")]
        public ActionResult GetExamens([FromBody] PagedListRequest pagedListRequest)
        {
            var examinationResult = Executor.GetQuery<GetExaminationSettingsQuery>().Process(q => q.Execute(pagedListRequest));
            return ProcessResultResponse(examinationResult);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult GetExamenById(Guid id)
        {
            var examResult = Executor.GetQuery<GetExaminationSettingByIdQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(examResult);
        }

        [HttpPost]
        [Route("create")]
        public ActionResult CreateExamination([FromBody]ExaminationSettingDto examinationSettingDto)
        {
            var examCreatorResult = Executor.GetCommand<CreateExaminationSettingCommand>().Process(c => c.Execute(examinationSettingDto));
            return ProcessResultResponse(examCreatorResult);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult UpdateExamination([FromBody]ExaminationSettingDto examinationSettingDto)
        {
            Executor.CommandChain()
                .AddCommand<UpdateExaminationSettingCommand>(c => c.Execute(examinationSettingDto))
                .AddCommand<DeleteExaminationSettingCategoriesCommand>(c => c.Execute(examinationSettingDto.ExaminationCategories, examinationSettingDto.Id))
                .AddCommand<UpdateExaminationSettingCategoriesCommand>(c => c.Execute(examinationSettingDto.ExaminationCategories))
                .AddCommand<CreateExaminationSettingCategoriesCommand>(c => c.Execute(examinationSettingDto.ExaminationCategories, examinationSettingDto.Id))
                .ExecuteAllWithTransaction();

            return Ok();
        }

        [HttpGet]
        [Route("{id}/categories")]
        public ActionResult GetExaminationCategory(Guid? id)
        {
            if (id == null || id == Guid.Empty)
            {
                return Ok(new List<ExaminationCategoryDto>());
            }

            var examinationCategories = Executor.GetQuery<GetExaminationSettingCategoriesQuery>().Process(q => q.Execute(id.Value));

            return ProcessResultResponse(examinationCategories);
        }        
    }
}