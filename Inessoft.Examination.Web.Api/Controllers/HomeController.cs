﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController(IHttpContextAccessor httpContextAccessor)
        {
            var test = httpContextAccessor.HttpContext.User;
            var isadmin = test.IsInRole("superadmin");
            var roleAdmin = test.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.Role && c.Value == "superadmin");
            var roles = test.IsInRole("");
            var hasClaim = test.HasClaim(ClaimTypes.Role, "superadmin");
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Logout()
        {
            await Microsoft.AspNetCore.Authentication.AuthenticationHttpContextExtensions.SignOutAsync(HttpContext, "Cookies"); ;
            await Microsoft.AspNetCore.Authentication.AuthenticationHttpContextExtensions.SignOutAsync(HttpContext, "oidc"); ;

            return View("Index");
        }
    }
}