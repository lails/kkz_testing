﻿using Inessoft.Examination.BusinessLogic.Examinations;
using Inessoft.Examination.BusinessLogic.PassingExaminations;
using Inessoft.Examination.BusinessLogic.PassingExaminations.Dtos;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Web.Api.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/passingExaminations")]
    public class PassingExaminationsController : BaseExaminationApiController
    {
        [HttpGet]
        [Route("getExaminerExams")]
        public ActionResult GetExaminerExams()
        {
            var currentExaminerExamsResult = Executor.GetQuery<GetCurrentExaminerExamsQuery>().Process(r => r.Execute());
            return ProcessResultResponse(currentExaminerExamsResult);
        }

        [HttpPost]
        [Route("{examId}/setExaminStartDate")]
        public ActionResult SetExaminStartDateByExamId(Guid examId)
        {
            Executor.GetCommand<SetStartExamDateByExamIdCommand>().Process(r => r.Execute(examId));

            return Ok();
        }

        [HttpGet]
        [Route("{examId}/getCurrentUserGroupByExamIdQuery")]
        public ActionResult GetCurrentUserGroupByExamIdQuery(Guid examId)
        {
            var userGroupResult = Executor.GetCommand<GetCurrentUserGroupByExamenIdQuery>().Process(r => r.Execute(examId));

            return ProcessResultResponse(userGroupResult);
        }

        [HttpGet]
        [Route("{examId}/getNextQueston/{questionNumber}")]
        public ActionResult GetNextQuestionByExamenId(Guid examId, int questionNumber)
        {
            var nextQuestionResult = Executor.GetQuery<GetNextQuestionByExamenIdQuery>().Process(r => r.Execute(examId, questionNumber));

            if (nextQuestionResult.IsFailure)
            {
                throw new BusinessLogicException(nextQuestionResult.Error);
            }

            Executor.GetCommand<AddQuestionToUserAnswerHistoryCommand>().Process(r => r.Execute(nextQuestionResult.Value.Id, examId));

            return ProcessResultResponse(nextQuestionResult);
        }

        [HttpGet]
        [Route("{examId}/getAnswers/{questionId}")]
        public ActionResult GetAnswersByQuestionId(Guid examId, Guid questionId)
        {
            var answersResult = Executor.GetQuery<GetExamenationQuestionAnswersQuery>().Process(r => r.Execute(examId, questionId));

            return ProcessResultResponse(answersResult);
        }


        [HttpPost]
        [Route("{examId}/updateUserAnswer")]
        public ActionResult UpdateUserAnswerHistory([FromBody]QuestionDto questionDto, Guid examId)
        {
            var updateUserAnswerHistoryCommandResponse = Executor.GetCommand<UpdateUserAnswerHistoryCommand>().Process(r => r.Execute(questionDto, examId));

            Executor.CommandChain()
                    .AddCommand<DeleteUserAnswerHistoryAnswerCommand>(r => r.Execute(updateUserAnswerHistoryCommandResponse.Value))
                    .AddCommand<CreateUserAnswerHistoryAnswerCommand>(r => r.Execute(updateUserAnswerHistoryCommandResponse.Value, questionDto.Answers))
                    .ExecuteAllWithTransaction();

            return Ok(updateUserAnswerHistoryCommandResponse);
        }

        [HttpPost]
        [Route("{examId}/checkExamenBeforeFinish")]
        public ActionResult CheckExamenBeforeFinish(Guid examId)
        {
            var currentExamResult = Executor.GetQuery<GetExaminationByIdQuery>().Process(r => r.Execute(examId));
            var userAnswerHistory = Executor.GetCommand<GetUserAnswerHistoryQuery>().Process(r => r.Execute(examId));

            //соотвествует ли количество вопросов отвеченным
            var questionAnsweredCount = userAnswerHistory.Value.Count(r => r.IsQuestionAnswered == true);
            if (questionAnsweredCount != currentExamResult.Value.QuestionCount)
            {
                throw new BusinessLogicException("Необходимо ответить на все вопросы");
            }

            //все ли не отвеченны
            var hasNotAnsweredQuestions = userAnswerHistory.Value.Any(r => r.IsQuestionAnswered == false);
            if (hasNotAnsweredQuestions == true)
            {
                throw new BusinessLogicException("Необходимо ответить на все вопросы");
            }

            return Ok();
        }


        [HttpPost]
        [Route("{examId}/finishExam")]
        public ActionResult FinishExam(Guid examId)
        {
            var userAnswerHistory = Executor.GetCommand<GetUserAnswerHistoryQuery>().Process(r => r.Execute(examId));
            var finishExamenresult = Executor.GetCommand<FinishExamenCommand>().Process(r => r.Execute(examId, userAnswerHistory.Value));

            return ProcessResultResponse(finishExamenresult);
        }

        [HttpGet]
        [Route("userResult/{userGroupId}")]
        public ActionResult GetExaminationUserGroupResult(Guid userGroupId)
        {
            var userGroupResult = Executor.GetCommand<GetCurrentUserGroupByIdQuery>().Process(r => r.Execute(userGroupId));

            if (userGroupResult.IsFailure)
            {
                throw new BusinessLogicException(userGroupResult.Error);
            }

            var userGroup = userGroupResult.Value;
            var exam = userGroup.Examination;
            var userGroupEvaluation = userGroup.TestResultEvaluation;

            var userGroupResultDto = new UserGroupResultDto
            {
                ExaminerFio = userGroup.GetFio(),
                ExamenName = exam.GetSystemLanguageQuestionTranslate().Text,
                UserGroupExamStatusName = userGroup.ExaminationResultType.GetSystemLanguageQuestionTranslate().Text,
                TestResultEvaluationSummary = userGroupEvaluation,
                TestResultPoint = userGroupEvaluation >= exam.PointFive
                                                    ? 5
                                                        : userGroupEvaluation >= exam.PointFour
                                                        ? 4
                                                            : userGroupEvaluation >= exam.PointThree
                                                            ? 3 : 2,
                TestQuestionTotalCount = exam.QuestionCount
            };

            return Ok(userGroupResultDto);
        }
    }
}