﻿using Inessoft.Examination.Di;
using Inessoft.Examination.Web.Api.Exceptions;
using Microsoft.AspNetCore.Mvc;
using NetCoreCQRS;
using NetCoreDomain;

namespace Inessoft.Examination.Web.Api.Controllers
{
    public class BaseExaminationApiController : Controller
    {
        private IExecutor _executor;
        protected IExecutor Executor => _executor ?? (_executor = ExaminationAmbientContext.Current.Executor);

        protected ActionResult ProcessResultResponse<T>(Result<T> result)
        {
            if (result.IsFailure)
            {
                throw new BusinessLogicException(result.Error);
            }

            var response = result.Value;
            return Ok(response);
        }
    }
}
