﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inessoft.Examination.Authentication;
using Inessoft.Examination.BusinessLogic.Users;
using Inessoft.Examination.Web.Api.Requests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NetCoreIdentityHttpClient;
using NetCoreIdentityHttpClient.Requests;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/users/")]
    public class UsersController : BaseExaminationApiController
    {
        private readonly INetCoreIdentityHttpClient _coreIdentityHttpClient;
        private readonly IHttpContextAccessor _iHttpContextAccessor;
        private readonly IConfigurationRoot _iConfigurationRoot;

        //TODO+: общее замечание для юзеров при создании/обновлении нужно бы делать trim для ФИО, inn, и остальных строковых полей, не фильтруется по полю middleName
        //Нужен АPI, который вернет признак наличия юзера по ИИН
        public UsersController(INetCoreIdentityHttpClient coreIdentityHttpClient,
            IHttpContextAccessor httpContextAccessor,
            IConfigurationRoot iIConfigurationRoot)
        {
            _coreIdentityHttpClient = coreIdentityHttpClient;
            _iHttpContextAccessor = httpContextAccessor;
            _iConfigurationRoot = iIConfigurationRoot;
        }

        // GET: api/usrs
        [HttpPost]
        [Route("")]
        public async Task<ActionResult> GetUsersPagedList([FromBody]GetUsersPagedListRequest request)
        {
            var usersPagedListResponse = await _coreIdentityHttpClient.GetUsersPagedList(request);
            return ProcessResultResponse(usersPagedListResponse);
        }

        // GET: api/users/5
        [HttpGet("{id}")]
        public async Task<ActionResult> GetUserById(Guid id)
        {
            var userByIdResoinse = await _coreIdentityHttpClient.GetUserById(id);
            return ProcessResultResponse(userByIdResoinse);
        }

        // POST: api/users/create
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> CreateUser([FromBody]TestingUserDto testingUserDto)
        {
            var createUserResponse = await _coreIdentityHttpClient.CreateUser(testingUserDto.ToUserDto);

            if (createUserResponse.IsSuccess)
            {
                testingUserDto.Id = createUserResponse.Value;
                Executor.GetCommand<CreateUserSettingCommand>().Process(c => c.Execute(testingUserDto));
            }

            return ProcessResultResponse(createUserResponse);
        }

        // PUT: api/users/update
        [HttpPut]
        [Route("update")]
        public async Task<ActionResult> UpdateUser([FromBody]TestingUserDto testingUserDto)
        {
            var updateUserResponse = await _coreIdentityHttpClient.UpdateUser(testingUserDto.ToUserDto);

            if (updateUserResponse.IsSuccess)
            {
                Executor.GetCommand<UpdateUserSettingCommand>().Process(c => c.Execute(testingUserDto));
            }

            return ProcessResultResponse(updateUserResponse);
        }

        [HttpGet]
        [Route("{id}/userSetting")]
        public ActionResult GetUserSetting(Guid id)
        {
            var userSettingResponse = Executor.GetQuery<GetUserSettingQueryByUserId>().Process(c => c.Execute(id));

            return ProcessResultResponse(userSettingResponse);
        }

        // DELETE: api/users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            var deleteUserResponse = await _coreIdentityHttpClient.DeleteUser(id);
            return ProcessResultResponse(deleteUserResponse);
        }

        // GET: api/users/roles
        [HttpGet]
        [Route("roles")]
        public async Task<ActionResult> GetRoles()
        {
            var rolesResponse = await _coreIdentityHttpClient.GetRoles();
            return ProcessResultResponse(rolesResponse);
        }

        // GET: api/users/fakes
        [HttpPost]
        [Route("examination-users")]
        public async Task<ActionResult> GetFakeUsers([FromBody] ExaminationUserGroupRequest request)
        {
            request.Take = 25;

            var coreIdentityHttpClient = new NetCoreIdentityHttpClient.NetCoreIdentityHttpClient(_iHttpContextAccessor, _iConfigurationRoot);

            var rolesResponse = await coreIdentityHttpClient.GetRoles();
            if (rolesResponse.IsSuccess)
            {
                var role = rolesResponse.Value.FirstOrDefault(r => r.Name == UserRoles.Employee);
                request.RoleId = role?.Id;
            }

            var usersPagedListResponse = await _coreIdentityHttpClient.GetUsersPagedList(request.ToGetUsersPagedListRequest);
            var response = usersPagedListResponse.Value;
            response.Items = response.Items.Where(u => request.ExcludeUserIds.Contains(u.Id) == false).ToArray();
            return Ok(response);
        }
    }
}
