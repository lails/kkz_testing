﻿using System.Threading.Tasks;
using Inessoft.Examination.Authentication;
using Inessoft.Examination.Di;
using Inessoft.Examination.Web.Api.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetCoreDI;
using NetCoreIdentityHttpClient;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/security")]
    [Authorize]
    public class SecurityController : BaseExaminationApiController
    {
        private readonly INetCoreIdentityHttpClient _coreIdentityHttpClient;

        //TODO+: общее замечание для юзеров при создании/обновлении нужно бы делать trim для ФИО, inn, и остальных строковых полей, не фильтруется по полю middleName
        //Нужен АPI, который вернет признак наличия юзера по ИИН
        public SecurityController(INetCoreIdentityHttpClient coreIdentityHttpClient)
        {
            _coreIdentityHttpClient = coreIdentityHttpClient;
        }

        // GET: api/Security
        [HttpGet]
        [Route("user-data")]
        public async Task<ActionResult> GetAuthenticatedUserDataAsync()
        {
            var identity = ExaminationAmbientContext.Current.AuthenticationService.Identity;

            var userByIdResoinse = await _coreIdentityHttpClient.GetUserById(identity.UserId);

            if (userByIdResoinse.IsFailure)
            {
                return ProcessResultResponse(userByIdResoinse);
            }

            var user = userByIdResoinse.Value;
            var authenticatedUser = new AuthenticatedUserDto
            {
                UserName = $"{user.FirstName} {user.MiddleName} {user.SecondName}",
                Iin = user.Inn,
                CanAccessAdministration = identity.IsInRole(UserRoles.Administrator),
                CanAccessExamination = identity.IsInRole(UserRoles.Employee)
            };

            return Ok(authenticatedUser);
        }

        [HttpGet]
        [Route("user/current/in-role/{roleName}")]
        public ActionResult IsUserInRole(string roleName)
        {
            return Ok(true);
            return Ok(ExaminationAmbientContext.Current.AuthenticationService.Identity.IsInRole(roleName));
        }
    }
}
