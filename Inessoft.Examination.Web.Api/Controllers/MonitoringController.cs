﻿using Inessoft.Examination.BusinessLogic.Common.Enums;
using Inessoft.Examination.BusinessLogic.ExaminationResultTypes;
using Inessoft.Examination.BusinessLogic.Examinations;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Requests;
using Inessoft.Examination.Web.Api.Enums;
using Inessoft.Examination.Web.Api.Exceptions;
using Microsoft.AspNetCore.Mvc;
using NetCoreDataAccess.BaseResponses;
using System.Linq;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/monitoring")]
    public class MonitoringController : BaseExaminationApiController
    {
        [HttpGet]
        [Route("examinations")]
        public ActionResult GetExaminations()
        {
            var activeExaminationResult = Executor.GetQuery<GetActiveExaminationsQuery>().Process(q => q.Execute());

            if (activeExaminationResult.IsFailure)
            {
                throw new FaultException(FaultLevel.Error, activeExaminationResult.Error);
            }

            return Ok(activeExaminationResult.Value);
        }

        [HttpPost]
        [Route("observed-users")]
        public ActionResult GetExaminationUserGroup([FromBody] MonitoringExaminationUserGroupPagedRequest request)
        {
            if (request == null || request.HasExaminationId == false)
            {
                return Ok(new PagedListResponse<MonitoringExaminationUserGroupDto>());
            }

            var userGroupResult = Executor.GetQuery<GetMonitoringExaminationUserGroupQuery>().Process(q => q.Execute(request));

            if (userGroupResult.IsFailure)
            {
                throw new BusinessLogicException(userGroupResult.Error);
            }

            return Ok(userGroupResult.Value);
        }

        [HttpGet]
        [Route("result-types")]
        public ActionResult GetGetExaminationResultTypes()
        {
            var resultTypesResult = Executor.GetQuery<GetExaminationResultTypesQuery>().Process(q => q.Execute());
            if (resultTypesResult.IsFailure)
            {
                throw new BusinessLogicException(resultTypesResult.Error);
            }

            var resultTypes = resultTypesResult.Value;

            var excludeTypes = new string[]
            {
                ExaminationResultTypeCodes.Passed,
                ExaminationResultTypeCodes.NotPassed
            };

            resultTypes = resultTypes.Where(r => excludeTypes.Contains(r.Code) == false).ToList();

            return Ok(resultTypes);
        }

        [HttpPut]
        [Route("update")]
        public ActionResult UpdateExaminationUserGroup([FromBody] MonitoringExaminationUserGroupDto userGroupDto)
        {
            var updateResult = Executor.GetCommand<UpdateExaminationUserGroupCommand>().Process(c => c.Execute(userGroupDto));
            if (updateResult.IsFailure)
            {
                throw new BusinessLogicException(updateResult.Error);
            }

            return Ok(updateResult.Value);
        }

        [HttpPost]
        [Route("individual-result-report")]
        public ActionResult GetIndividualResultReportData([FromBody] MonitoringIndividualResultReportRequest request)
        {
            var reportDataResult = Executor.GetQuery<MonitoringIndividualResultReportQuery>().Process(q => q.Execute(request));
            if (reportDataResult.IsFailure)
            {
                throw new BusinessLogicException(reportDataResult.Error);
            }

            return Ok(reportDataResult.Value);
        }
    }
}
