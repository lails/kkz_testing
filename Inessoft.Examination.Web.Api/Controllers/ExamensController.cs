﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inessoft.Examination.Authentication;
using Inessoft.Examination.BusinessLogic.ExaminationCategories;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.BusinessLogic.Examinations;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups;
using Inessoft.Examination.BusinessLogic.Users;
using Inessoft.Examination.Di;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetCoreDataAccess.BaseRequests;
using NetCoreDomain;
using NetCoreIdentityHttpClient;
using NetCoreIdentityHttpClient.Dtos;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/examinations")]
    public class ExamensController : BaseExaminationApiController
    {
        private readonly INetCoreIdentityHttpClient _coreIdentityHttpClient;

        public ExamensController(INetCoreIdentityHttpClient coreIdentityHttpClient)
        {
            _coreIdentityHttpClient = coreIdentityHttpClient;
        }

        [HttpPost]
        [Route("")]
        public ActionResult GetExamens([FromBody] PagedListRequest pagedListRequest)
        {
            var examinationResult = Executor.GetQuery<GetExaminationsQuery>().Process(q => q.Execute(pagedListRequest));
            return ProcessResultResponse(examinationResult);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult GetExamenById(Guid id)
        {
            var examResult = Executor.GetQuery<GetExaminationByIdQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(examResult);
        }

        [HttpPost]
        [Route("create")]
        public ActionResult CreateExamination([FromBody]ExaminationDto examinationDto)
        {
            var examCreatorResult = Executor.GetCommand<CreateExaminationCommand>().Process(c => c.Execute(examinationDto));
            return ProcessResultResponse(examCreatorResult);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult UpdateExamination([FromBody]ExaminationDto examinationDto)
        {
            Executor.CommandChain()
                .AddCommand<UpdateExaminationCommand>(c => c.Execute(examinationDto))
                .AddCommand<DeleteExaminationCategoriesCommand>(c => c.Execute(examinationDto.ExaminationCategories, examinationDto.Id))
                .AddCommand<UpdateExaminationCategoriesCommand>(c => c.Execute(examinationDto.ExaminationCategories))
                .AddCommand<CreateExaminationCategoriesCommand>(c => c.Execute(examinationDto.ExaminationCategories, examinationDto.Id))
                .AddCommand<DeleteExaminationUserGroupsCommand>(c => c.Execute(examinationDto.ExaminationUserGroups, examinationDto.Id))
                .AddCommand<UpdateExaminationUserGroupsCommand>(c => c.Execute(examinationDto.ExaminationUserGroups, examinationDto.Id))
                .AddCommand<CreateExaminationUserGroupsCommand>(c => c.Execute(examinationDto.ExaminationUserGroups, examinationDto.Id))
                .ExecuteAllWithTransaction();

            return Ok();
        }

        [HttpGet]
        [Route("chairmans")]
        public async Task<ActionResult> GetExaminationChairmans()
        {
            var userId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;
            var usersettings = Executor.GetQuery<GetUserSettingQueryByUserId>().Process(q => q.Execute(userId));
            var userCategory = usersettings.Value.CategoryId;

            var chairmansResult = await _coreIdentityHttpClient.GetUsersByRole(UserRoles.Chairman);

            if (userCategory.HasValue == false)
            {
                return ProcessResultResponse(chairmansResult);
            }

            var userSettingsForCategory = Executor.GetQuery<GetUserSettingsQueryByCategoryId>().Process(q => q.Execute(userCategory.Value));
            var includedUserIds = userSettingsForCategory.Value.Select(c => c.UserId).ToList();

            var includedChairmans = chairmansResult.Value.Where(c => includedUserIds.Contains(c.Id)).ToList();
            return ProcessResultResponse(Result<List<UserDto>>.Ok(includedChairmans));
        }

        [HttpGet]
        [Route("{id}/users")]
        public ActionResult GetExaminationUsers(Guid id)
        {
            var examinationUsersResult = Executor.GetQuery<GetExaminationUserGroupsQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(examinationUsersResult);
        }

        [HttpGet]
        [Route("{id}/categories")]
        public ActionResult GetExaminationCategory(Guid? id)
        {
            if (id == null || id == Guid.Empty)
            {
                return Ok(new List<ExaminationCategoryDto>());
            }

            var examinationCategories = Executor.GetQuery<GetExaminationCategoriesQuery>().Process(q => q.Execute(id.Value));

            return ProcessResultResponse(examinationCategories);
        }
    }
}