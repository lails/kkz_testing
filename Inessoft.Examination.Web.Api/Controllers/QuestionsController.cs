﻿using System;
using Inessoft.Examination.BusinessLogic.Answers;
using Inessoft.Examination.BusinessLogic.Questions;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.BusinessLogic.Questions.Requests;
using Inessoft.Examination.BusinessLogic.Translations.Questions;
using Inessoft.Examination.BusinessLogic.Translations.Questions.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetCoreDataAccess.BaseResponses;
using NetCoreDomain;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/questions")]
    public class QuestionsController : BaseExaminationApiController
    {
        [HttpPost]
        [Route("")]
        public ActionResult GetQuestionsPagedList([FromBody] QuestionsPagedListRequest questionsPagedListRequest)
        {
            Result<PagedListResponse<QuestionDto>> questionsResult = new Result<PagedListResponse<QuestionDto>>(new PagedListResponse<QuestionDto> { }, true, ""); ;

            if (questionsPagedListRequest.HasCategoryId == true)
            {
                questionsResult = Executor.GetQuery<GetQuestionsQuery>().Process(q => q.Execute(questionsPagedListRequest));
            }

            return ProcessResultResponse(questionsResult);
        }

        [HttpPost]
        [Route("translation")]
        public ActionResult GetQuestionsWithTwoLanguages([FromBody] RequestQuestionTrnaslationDto requestQuestionTrnaslationDto)
        {
            var questionsResult = Executor.GetQuery<GetQuestionTranslateByLanguageCodesQuery>().Process(q => q.Execute(requestQuestionTrnaslationDto));
            return ProcessResultResponse(questionsResult);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult GetQuestionById(Guid id)
        {
            var questionResult = Executor.GetCommand<GetQuestionByIdQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(questionResult);
        }

        [HttpPost]
        [Route("create")]
        public ActionResult CreateQuestion([FromBody] QuestionDto questionDto)
        {
            var question = questionDto.CreateQuestion;

            Executor.CommandChain()
                .AddCommand<CreateQuestionCommand>(c => c.Execute(question))
                .AddCommand<CreateAnswersCommand>(c => c.Execute(questionDto, question.Id))
                .ExecuteAllWithTransaction();

            return Ok(question.Id);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult UpdateQuestion([FromBody] QuestionDto questionDto)
        {
            Executor.CommandChain()
                .AddCommand<UpdateQuestionCommand>(c => c.Execute(questionDto))
                .AddCommand<DeleteAnswerCommand>(c => c.Execute(questionDto))
                .AddCommand<UpdateAnswerCommand>(c => c.Execute(questionDto))
                .AddCommand<CreateAnswersCommand>(c => c.Execute(questionDto))
                .ExecuteAllWithTransaction();

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteQuestion(Guid id)
        {
            Executor
                .CommandChain()
                .AddCommand<DeleteQuestionAnswersCommand>(c => c.Execute(id))
                .AddCommand<DeleteQuestionCommand>(c => c.Execute(id))
                .ExecuteAllWithTransaction();

            return Ok();
        }

        [HttpGet]
        [Route("{id}/answers")]
        public ActionResult GetQuestionAnswers(Guid id)
        {
            var questionAnswersResult = Executor.GetQuery<GetQuestionAnswersQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(questionAnswersResult);
        }

        [HttpPost]
        [Route("createOrUpdateQuestoinTranslate")]
        public ActionResult CreateQuestionTranslate([FromBody] QuestionTranslate questionTranslate)
        {
            var answerTranslateResult = Executor.GetCommand<CreateOrUpdateQuestionTranslateCommand>().Process(q => q.Execute(questionTranslate));
            return ProcessResultResponse(answerTranslateResult);
        }

        [HttpPost]
        [Route("createOrUpdateAnswerTranslate")]
        public ActionResult CreateOrUpdateAnswerTranslate([FromBody] AnswerTranslate answerTranslate)
        {
            var answerTranslateResult = Executor.GetCommand<CreateOrUpdateAnswerTranslateCommand>().Process(q => q.Execute(answerTranslate));
            return ProcessResultResponse(answerTranslateResult);
        }
    }
}