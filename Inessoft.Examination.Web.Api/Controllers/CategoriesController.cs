﻿using System;
using Microsoft.AspNetCore.Mvc;
using Inessoft.Examination.BusinessLogic.Categories;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Inessoft.Examination.Web.Api.Exceptions;
using Inessoft.Examination.BusinessLogic.Translations.Dtos;
using Inessoft.Examination.BusinessLogic.Translations.Categories;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using Microsoft.AspNetCore.Authorization;
using Inessoft.Examination.Di;
using Inessoft.Examination.BusinessLogic.Users;
using System.Linq;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/categories")]
    public class CategoriesController : BaseExaminationApiController
    {
        [HttpPost]
        [Route("")]
        public ActionResult GetCategories()
        {

            var categoriesResult = Executor.GetQuery<GetCategoriesQuery>().Process(q => q.Execute());

            var userId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;
            var usersettings = Executor.GetQuery<GetUserSettingQueryByUserId>().Process(q => q.Execute(userId));
            var userCategory = usersettings.Value.CategoryId;

            if (userCategory.HasValue == false)
            {
                return ProcessResultResponse(categoriesResult);
            }
            categoriesResult.Value.Items = categoriesResult.Value.Items
                .Where(r => (r.ParentId.HasValue == false && r.Id == userCategory.Value)
                            || r.ParentId.HasValue == true).ToArray();


            return ProcessResultResponse(categoriesResult);
        }

        [HttpPost]
        [Route("translation")]
        public ActionResult GetCategoriesWithTwoLanguages([FromBody] TrnaslationDto trnaslationDto)
        {
            var categoriesResult = Executor.GetQuery<GetCategoryTranslateByLanguageCodesQuery>()
                .Process(q => q.Execute(trnaslationDto));

            return ProcessResultResponse(categoriesResult);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult GetCategoryById(Guid id)
        {
            var categoryResult = Executor.GetQuery<GetCategotyByIdQuery>().Process(q => q.Execute(id));
            return ProcessResultResponse(categoryResult);
        }

        [HttpPost]
        [Route("create")]
        public ActionResult CreateCategory([FromBody] CategoryDto categoryDto)
        {
            var createdCategoryResult = Executor.GetCommand<CreateCategoryCommand>().Process(c => c.Execute(categoryDto));
            return ProcessResultResponse(createdCategoryResult);
        }

        [HttpPost]
        [Route("update")]
        public ActionResult UpdateCategory([FromBody] CategoryDto categoryDto)
        {
            var updatedCategoryResult = Executor.GetCommand<UpdateCategoryCommand>().Process(c => c.Execute(categoryDto));
            return ProcessResultResponse(updatedCategoryResult);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteCategory(Guid id)
        {
            var categoryHasChildrenResult = Executor.GetQuery<CheckCategoryHasChildrenQuery>().Process(q => q.Execute(id));
            if (categoryHasChildrenResult.IsFailure)
            {
                throw new BusinessLogicException(categoryHasChildrenResult.Error);
            }

            if (categoryHasChildrenResult.Value.HasValue && categoryHasChildrenResult.Value == true)
            {
                throw new BusinessLogicException("Невозможно удалить категорию. Категория имеет дочерние элементы");
            }

            var categoryHasQuestionsResult = Executor.GetQuery<CheckCategoryHasQuestionsQuery>().Process(q => q.Execute(id));
            if (categoryHasQuestionsResult.IsFailure)
            {
                throw new BusinessLogicException(categoryHasQuestionsResult.Error);
            }

            if (categoryHasQuestionsResult.Value.HasValue && categoryHasQuestionsResult.Value == true)
            {
                throw new BusinessLogicException("Невозможно удалить категорию. Категория имеет вопросы");
            }

            Executor.CommandChain()
                .AddCommand<DeleteCategoryByIdCommand>(c => c.Execute(id))
                .AddCommand<DeleteCategoryTranslatesCommand>(c => c.Execute(id))
                .ExecuteAllWithTransaction();

            return Ok();
        }


        [HttpPost]
        [Route("createOrUpdateCategoryTranslate")]
        public ActionResult CreateQuestionTranslate([FromBody] CategoryTranslate categoryTranslate)
        {
            var categoryTranslateResult = Executor.GetCommand<CreateOrUpdateCategoryTranslateCommand>().Process(q => q.Execute(categoryTranslate));
            return ProcessResultResponse(categoryTranslateResult);
        }
    }
}