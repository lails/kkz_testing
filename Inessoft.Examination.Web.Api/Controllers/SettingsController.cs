﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Di;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/settings")]
    public class SettingsController : Controller
    {
        [HttpGet]
        [Route("getAllLanguages")]
        public ActionResult GetAllLanguages()
        {
            var languages = new List<LanguageDto>();
            ExaminationAmbientContext.Current.Configuration.GetSection("Languages").Bind(languages);
            
            return Ok(languages);
        }
    }
}