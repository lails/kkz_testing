﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Inessoft.Examination.BusinessLogic.Answers;
using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.BusinessLogic.Questions;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.BusinessLogic.Users;
using Inessoft.Examination.Di;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetCoreIdentityHttpClient;
using NetCoreIdentityHttpClient.Dtos;

namespace Inessoft.Examination.Web.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/files")]
    public class FilesController : BaseExaminationApiController
    {
        private readonly INetCoreIdentityHttpClient _coreIdentityHttpClient;

        //TODO+: общее замечание для юзеров при создании/обновлении нужно бы делать trim для ФИО, inn, и остальных строковых полей, не фильтруется по полю middleName
        //Нужен АPI, который вернет признак наличия юзера по ИИН
        public FilesController(INetCoreIdentityHttpClient coreIdentityHttpClient)
        {
            _coreIdentityHttpClient = coreIdentityHttpClient;
        }

        [HttpPost]
        [Route("uploadUsers")]
        public async Task<object> UploadUsers()
        {
            var files = GetFiles(Request);

            var parsedUsers = new List<UserDto>();

            foreach (var formFile in files)
            {
                var filePath = Path.GetTempFileName();

                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
                {
                    formFile.CopyTo(stream);

                    var users = ExaminationAmbientContext.Current.ExcelDataHandlersService.UserDataHandlers.FormUserDto(stream);

                    parsedUsers.AddRange(users);
                }

                System.IO.File.Delete(filePath);
            }

            var failureUsers = new List<string>();
            var uploadedUsers = new List<UserDto>();

            if (parsedUsers.Count > 0)
            {
                #region Логика создания/обновления пользователей

                foreach (var user in parsedUsers)
                {
                    try
                    {
                        var testingUserDto = new TestingUserDto
                        {
                            Id = user.Id,
                            Inn = user.Inn,
                            MiddleName = user.MiddleName,
                            FirstName = user.FirstName,
                            SecondName = user.SecondName,
                            IsActive = user.IsActive
                        };

                        if (Guid.Empty == user.Id)
                        {
                            var createUserResponse = await _coreIdentityHttpClient.CreateUser(testingUserDto.ToUserDto);
                            if (createUserResponse.IsSuccess)
                            {
                                user.Id = createUserResponse.Value;
                                Executor.GetCommand<CreateUserSettingCommand>().Process(c => c.Execute(testingUserDto));

                                var createdUser = new UserDto
                                {
                                    Id = createUserResponse.Value,
                                    Inn = testingUserDto.Inn,
                                    FirstName = testingUserDto.FirstName,
                                    MiddleName = testingUserDto.MiddleName,
                                    SecondName = testingUserDto.SecondName
                                };
                                uploadedUsers.Add(createdUser);
                            }
                        }
                        else
                        {
                            var updateUserResponse = await _coreIdentityHttpClient.UpdateUser(user);
                            if (updateUserResponse.IsSuccess)
                            {
                                Executor.GetCommand<UpdateUserSettingCommand>().Process(c => c.Execute(testingUserDto));

                                var uploadedUser = new UserDto
                                {
                                    Id = user.Id,
                                    Inn = user.Inn,
                                    FirstName = user.FirstName,
                                    MiddleName = user.MiddleName,
                                    SecondName = user.SecondName
                                };
                                uploadedUsers.Add(uploadedUser);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        failureUsers.Add($"ИИН: {user.Inn}, ФИО: {user.FirstName} {user.MiddleName} {user.SecondName}. Ошибка: Ошибка сервера.");
                    }
                }

                #endregion
            }

            return new { failureUsers, uploadedUsers };

        }

        [HttpPost]
        [Route("uploadQuestions/{categoryId}")]
        public ActionResult UploadQuestions(Guid categoryId)
        {
            var files = GetFiles(Request);

            var parsedQuestions = new List<QuestionDto>();

            #region Парсинг вопросов 

            foreach (var formFile in files)
            {
                var filePath = Path.GetTempFileName();

                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
                {
                    formFile.CopyTo(stream);

                    var questionDtos = ExaminationAmbientContext.Current.ExcelDataHandlersService.QuestionDataHandlers.FormQuestion(stream);
                    foreach (var questionDto in questionDtos)
                    {
                        var question = new QuestionDto
                        {
                            Id = questionDto.Id,
                            CategoryId = categoryId,
                            QuestionText = questionDto.QuestionText,
                            IsMultipleAnswer = questionDto.IsMultipleAnswer,
                            ArticleNumber = questionDto.ArticleNumber,
                            Answers = new List<AnswerDto>()
                        };


                        foreach (var answerDto in questionDto.Answers)
                        {
                            question.Answers.Add(new AnswerDto
                            {
                                AnswerText = answerDto.AnswerText,
                                IsRight = answerDto.IsRight,
                                QuestionId = question.Id
                            });
                        }

                        parsedQuestions.Add(question);
                    }
                }

                System.IO.File.Delete(filePath);
            }

            #endregion

            var failureQuestions = new List<string>();

            #region Логика создания/обновления вопросов

            if (parsedQuestions.Count > 0)
            {
                foreach (var questionDto in parsedQuestions)
                {
                    try
                    {
                        if (Guid.Empty == questionDto.Id)
                        {
                            var question = questionDto.CreateQuestion;

                            Executor.CommandChain()
                                .AddCommand<CreateQuestionCommand>(c => c.Execute(question))
                                .AddCommand<CreateAnswersCommand>(c => c.Execute(questionDto, question.Id))
                                .ExecuteAllWithTransaction();
                        }
                        else
                        {
                            Executor.CommandChain()
                                .AddCommand<UpdateQuestionCommand>(c => c.Execute(questionDto))
                                .AddCommand<DeleteAnswerCommand>(c => c.Execute(questionDto))
                                .AddCommand<UpdateAnswerCommand>(c => c.Execute(questionDto))
                                .AddCommand<CreateAnswersCommand>(c => c.Execute(questionDto))
                                .ExecuteAllWithTransaction();
                        }
                    }
                    catch (Exception ex)
                    {
                        failureQuestions.Add($"Id: {questionDto.Id}, Текст вопроса: {questionDto.QuestionText}. Ошибка: Ошибка сервера.");
                    }
                }
            }

            #endregion

            return Ok(failureQuestions);

        }

        [HttpGet]
        [Route("getUserDtoTemplate")]
        public FileResult GetUserDtoTemplate()
        {
            var path = Path.GetFullPath("FileTemplates/UserDtoTemplate.xlsx");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                stream.CopyTo(memory);
            }
            memory.Position = 0;

            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        [HttpGet]
        [Route("getQuestionDtoTemplate")]
        public FileResult GetQuestionDtoTemplate()
        {
            var path = Path.GetFullPath("FileTemplates/QuestionDtoTemplate.xlsx");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                stream.CopyTo(memory);
            }
            memory.Position = 0;

            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        #region File Helpers

        private List<IFormFile> GetFiles(HttpRequest request)
        {
            //TODO: ADD file tpye validate
            var files = request.Form?.Files;
            var formFiles = new List<IFormFile>();

            foreach (var formFile in files)
            {
                if (formFile != null && formFile.Length > 0)
                {
                    formFiles.Add(formFile);
                }
            }

            return formFiles;
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        #endregion
    }
}