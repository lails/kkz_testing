param($buildNumber)

$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
echo $dir
cd $dir

dotnet publish -c Release -o C:/KKZTestingBuild/KKZTesting_$buildNumber/publish

Copy-Item -Path "../Dockerfile" -Destination C:/KKZTestingBuild/KKZTesting_$buildNumber/