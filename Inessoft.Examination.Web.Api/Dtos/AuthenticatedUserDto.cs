﻿namespace Inessoft.Examination.Web.Api.Dtos
{
    public class AuthenticatedUserDto
    {
        public string UserName { get; set; }
        public string Iin { get; set; }
        public string BearerToken { get; set; }
        public bool CanAccessAdministration { get; set; }
        public bool CanAccessExamination { get; set; }
    }
}