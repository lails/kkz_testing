﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.BusinessLogic.ExaminationChairmans.Dtos;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.BusinessLogic.Examinations.Dtos
{
    public class ExaminationDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [IgnoreDataMember]
        private DateTime _startDate;
        public DateTime StartDate
        {
            get => _startDate;
            set
            {
                var date = value;
                _startDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            }
        }

        [IgnoreDataMember]
        private DateTime _endDate;
        public DateTime EndDate
        {
            get => _endDate;
            set
            {
                var date = value;
                _endDate = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
            }
        }

        public int Duration { get; set; }
        public int QuestionCount { get; set; }
        public int RequiredCountOfCorrectAnswers { get; set; }

        public int PointFive { get; set; }
        public int PointFour { get; set; }
        public int PointThree { get; set; }

        public ExaminationChairmanDto Chairman { get; set; }
        public Guid ChairmanId { get; set; }

        public List<ExaminationCategoryDto> ExaminationCategories { get; set; }
        public List<ExaminationUserGroupDto> ExaminationUserGroups { get; set; }

        public static Func<Domain.Testing.Examination, ExaminationDto> Map = exam => new ExaminationDto
        {
            Id = exam.Id,
            Name = exam.GetSystemLanguageQuestionTranslate()?.Text,
            Chairman = ExaminationChairmanDto.Map(exam),
            Duration = exam.Duration,
            StartDate = exam.StartDate,
            EndDate = exam.EndDate,
            QuestionCount = exam.QuestionCount,
            RequiredCountOfCorrectAnswers = exam.RequiredEvaluationSummary,
            PointFive = exam.PointFive,
            PointFour = exam.PointFour,
            PointThree = exam.PointThree
        };

        [IgnoreDataMember]
        public Domain.Testing.Examination CreateExamination => new Domain.Testing.Examination
        {
            ChairmanId = ChairmanId,
            ChairmanName = Chairman.FIO,
            ChairmanInn = Chairman.INN,
            Duration = Duration,
            StartDate = StartDate,
            EndDate = EndDate,
            IsDeleted = false,
            QuestionCount = QuestionCount,
            RequiredEvaluationSummary = RequiredCountOfCorrectAnswers,
            PointFive = PointFive,
            PointFour = PointFour,
            PointThree = PointThree,
            ExamineTranslates = new List<ExamineTranslate>
            {
                new ExamineTranslate
                {
                    LanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode,
                    Text = Name
                }
            },
            ExaminationCategories = ExaminationCategories.Select(c => c.ToExaminationCategory).ToList(),
            ExaminationUserGroups = ExaminationUserGroups.Select(u => u.ToExaminationUserGroup).ToList()
        };

        public void UpdateExamination(Domain.Testing.Examination examination)
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            examination.ChairmanId = Chairman.Id;
            examination.ChairmanName = Chairman.FIO;
            examination.ChairmanInn = Chairman.INN;
            examination.Duration = Duration;
            examination.StartDate = StartDate;
            examination.EndDate = EndDate;
            examination.IsDeleted = false;
            examination.QuestionCount = QuestionCount;
            examination.RequiredEvaluationSummary = RequiredCountOfCorrectAnswers;
            examination.PointFive = PointFive;
            examination.PointFour = PointFour;
            examination.PointThree = PointThree;

            UpdateExaminationNameTranslation(examination, systemLanguageCode);

            //examination.ExaminationCategories = ExaminationCategories.Select(c => c.ToExaminationCategory).ToList();
            //examination.ExaminationUserGroups = ExaminationUserGroups.Select(u => u.ToExaminationUserGroup).ToList();
        }

        private void UpdateExaminationNameTranslation(Domain.Testing.Examination examination, string systemLanguageCode)
        {
            if (examination.HasSystemLanguageQuestionTranslate)
            {
                examination
                    .GetSystemLanguageQuestionTranslate()
                    .SetTranslate(Name);

                return;
            }

            if (examination.ExamineTranslates == null)
            {
                examination.ExamineTranslates = new List<ExamineTranslate>();
            }

            var examinationTranslateText = new ExamineTranslate
            {
                LanguageCode = systemLanguageCode,
                Text = Name
            };

            examination.ExamineTranslates.Add(examinationTranslateText);
        }
    }
}