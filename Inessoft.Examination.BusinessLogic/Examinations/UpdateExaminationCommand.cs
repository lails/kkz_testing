﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class UpdateExaminationCommand : BaseCommand
    {
        public Result<Guid> Execute(ExaminationDto examinationDto)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();
                var examination = examinationRepository
                    .AsQueryable()
                    .Include(c => c.ExamineTranslates)
                    //.Include(c => c.ExaminationCategories)
                    //.ThenInclude(ec => ec.Category)
                    //.Include(c => c.ExaminationUserGroups)
                    .FirstOrDefault(c => c.Id == examinationDto.Id && c.IsDeleted == false);

                if (examination == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Exam with id {examinationDto.Id} not found");
                }

                examinationDto.UpdateExamination(examination);
                examinationRepository.Update(examination);
                Uow.SaveChanges();

                return Result<Guid>.Ok(examination.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}