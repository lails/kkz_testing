﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDataAccess.BaseRequests;
using NetCoreDataAccess.BaseResponses;
using NetCoreDataAccess.Externsions;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class GetExaminationsQuery: BaseQuery
    {
        public Result<PagedListResponse<ExaminationDto>> Execute(PagedListRequest request)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();
                var response = new PagedListResponse<ExaminationDto>();

                var query = examinationRepository
                    .AsQueryable()
                    .Include(e => e.ExamineTranslates)
                    .Where(e => e.IsDeleted == false);

                response.Items = query
                    .ApplyPagedListRequest(request, response)
                    .AsEnumerable()
                    .Select(ExaminationDto.Map)
                    .ToArray();

                return Result<PagedListResponse<ExaminationDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<PagedListResponse<ExaminationDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}