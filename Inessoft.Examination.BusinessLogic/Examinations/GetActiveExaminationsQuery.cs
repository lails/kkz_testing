﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Inessoft.Examination.Di;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class GetActiveExaminationsQuery : BaseQuery
    {
        public Result<List<ExaminationDto>> Execute()
        {
            try
            {
                var currentDate = ExaminationAmbientContext.Current.DateTimeProvider.Now;

                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();

                var query = examinationRepository
                    .AsQueryable()
                    .Include(e => e.ExamineTranslates)
                    .Where(e => e.StartDate <= currentDate && currentDate <= e.EndDate && e.IsDeleted == false);

                var response = query
                    .AsEnumerable()
                    .Select(ExaminationDto.Map)
                    .ToList();

                return Result<List<ExaminationDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}