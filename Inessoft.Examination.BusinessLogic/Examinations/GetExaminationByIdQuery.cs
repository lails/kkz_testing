﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class GetExaminationByIdQuery : BaseQuery
    {
        public Result<ExaminationDto> Execute(Guid id)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();
                var examinationDto = examinationRepository
                    .AsQueryable()
                    .Include(e => e.ExamineTranslates)
                    .Where(e => e.Id == id && e.IsDeleted == false)
                    .AsEnumerable()
                    .Select(ExaminationDto.Map)
                    .FirstOrDefault();

                if (examinationDto == null)
                {
                    return Result<ExaminationDto>.Fail(null, $"Exam with id {id} not found");
                }

                return Result<ExaminationDto>.Ok(examinationDto);
            }
            catch (Exception exception)
            {
                return Result<ExaminationDto>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}