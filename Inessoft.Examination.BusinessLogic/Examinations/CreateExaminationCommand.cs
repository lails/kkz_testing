﻿using System;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class CreateExaminationCommand : BaseCommand
    {
        public Result<Guid> Execute(ExaminationDto categoryDto)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();
                var examination = categoryDto.CreateExamination;
                examinationRepository.Create(examination);
                Uow.SaveChanges();
                return Result<Guid>.Ok(examination.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}