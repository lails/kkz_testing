﻿using System;
using System.Collections.Generic;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.BusinessLogic.Answers.Dtos
{
    public class AnswerDto
    {
        public Guid Id { get; set; }
        public bool IsRight { get; set; }
        public Guid QuestionId { get; set; }
        public string AnswerText { get; set; }

        public static Func<Answer, AnswerDto> Map = answer => new AnswerDto
        {
            Id = answer.Id,
            QuestionId = answer.QuestionId,
            IsRight = answer.IsRight,
            AnswerText = answer.GetSystemLanguageAnswerTranslate()?.Text
        };


        public static Func<Answer, AnswerDto> MapForExamenPassing = answer => new AnswerDto
        {
            Id = answer.Id,
            QuestionId = answer.QuestionId,
            AnswerText = answer.GetSystemLanguageAnswerTranslate()?.Text
        };

        public Answer ToAnswer => new Answer
        {
            IsRight = IsRight,
            QuestionId = QuestionId,
            IsDeleted = false,
            AnswerTranslates = new List<AnswerTranslate>
            {
                new AnswerTranslate
                {
                    LanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode,
                    Text = AnswerText
                }
            }
        };

        public void UpdateAnswer(Answer answer)
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;

            answer.IsRight = IsRight;
            answer.QuestionId = QuestionId;

            if (answer.HasSystemLanguageAnswerTranslate)
            {
                answer
                    .GetSystemLanguageAnswerTranslate()
                    .SetTranslate(AnswerText);

                return;
            }

            if (answer.AnswerTranslates == null)
            {
                answer.AnswerTranslates = new List<AnswerTranslate>();
            }

            var answerTranslateText = new AnswerTranslate
            {
                LanguageCode = systemLanguageCode,
                Text = AnswerText
            };

            answer.AnswerTranslates.Add(answerTranslateText);
        }
    }
}