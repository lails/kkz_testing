﻿using System;
using System.Linq;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.Answers
{
    public class DeleteQuestionAnswersCommand : BaseCommand
    {
        public void Execute(Guid questionId)
        {
            var answersRespository = Uow.GetRepository<Answer>();
            var questionAnswers = answersRespository
                .AsQueryable()
                .Where(a => a.QuestionId == questionId && a.IsDeleted == false)
                .AsEnumerable();

            foreach (var questionAnswer in questionAnswers)
            {
                questionAnswer.MarkAsDeleted();
                answersRespository.Update(questionAnswer);
            }

            Uow.SaveChanges();
        }
    }
}