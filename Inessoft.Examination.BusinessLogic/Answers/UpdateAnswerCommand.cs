﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Answers
{
    public class UpdateAnswerCommand : BaseCommand
    {
        public Result<List<Guid>> Execute(QuestionDto questionDto)
        {
            try
            {
                var updatedAnswerDtos = questionDto
                    .Answers
                    .Where(a => a.Id != Guid.Empty)
                    .Select(a => a)
                    .ToList();

                if (updatedAnswerDtos.Any() == false)
                {
                    return Result<List<Guid>>.Ok(new List<Guid>());
                }

                var answerRepository = Uow.GetRepository<Answer>();

                foreach (var updatedAnswerDto in updatedAnswerDtos)
                {
                    var answer = answerRepository
                        .AsQueryable()
                        .Include(a => a.AnswerTranslates)
                        .FirstOrDefault(a => a.Id == updatedAnswerDto.Id);

                    if (answer == null)
                    {
                        return Result<List<Guid>>.Fail(null, $"Anser with id {updatedAnswerDto.Id} not found");
                    }

                    updatedAnswerDto.UpdateAnswer(answer);
                    answerRepository.Update(answer);
                }

                Uow.SaveChanges();

                return Result<List<Guid>>.Ok(updatedAnswerDtos.Select(a => a.Id).ToList());
            }
            catch (Exception exception)
            {
                return Result<List<Guid>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}