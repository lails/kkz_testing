﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Answers
{
    public class GetQuestionAnswersQuery : BaseQuery
    {
        public Result<List<AnswerDto>> Execute(Guid questionId)
        {
            try
            {
                var answersRepository = Uow.GetRepository<Answer>();
                var questionAnswers = answersRepository
                    .AsQueryable()
                    .Include(a => a.AnswerTranslates)
                    .Where(a => a.QuestionId == questionId && a.IsDeleted == false)
                    .AsEnumerable()
                    .Select(AnswerDto.Map)
                    .ToList();

                return Result<List<AnswerDto>>.Ok(questionAnswers);
            }
            catch (Exception exception)
            {
                return Result<List<AnswerDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}