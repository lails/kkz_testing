﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Answers
{
    public class CreateAnswersCommand : BaseCommand
    {
        public Result<List<Guid>> Execute(QuestionDto questionDto, Guid? questionId = null)
        {
            try
            {
                var addedAnswers = questionDto
                    .Answers
                    .Where(a => a.Id == Guid.Empty)
                    .Select(a => a.ToAnswer)
                    .ToList();

                var answersQuestionId = questionId.HasValue == false || questionId == Guid.Empty ? questionDto.Id : questionId;
                return SaveAnswers(answersQuestionId.Value, addedAnswers);
            }
            catch (Exception exception)
            {
                return Result<List<Guid>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }

        private Result<List<Guid>> SaveAnswers(Guid questionId, List<Answer> addedAnswers)
        {
            if (addedAnswers.Any() == false)
            {
                return Result<List<Guid>>.Ok(new List<Guid>());
            }

            var answerRepository = Uow.GetRepository<Answer>();

            foreach (var addedAnswer in addedAnswers)
            {
                addedAnswer.QuestionId = questionId;
                answerRepository.Create(addedAnswer);
            }

            Uow.SaveChanges();
            return Result<List<Guid>>.Ok(addedAnswers.Select(a => a.Id).ToList());
        }
    }
}