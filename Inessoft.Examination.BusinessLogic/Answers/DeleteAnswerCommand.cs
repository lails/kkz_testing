﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Answers
{
    public class DeleteAnswerCommand : BaseCommand
    {
        public Result<bool> Execute(QuestionDto questionDto)
        {
            try
            {
                var answersRespository = Uow.GetRepository<Answer>();

                var existedAnswerIds = questionDto
                    .Answers
                    .Where(a => a.Id != Guid.Empty)
                    .Select(a => a.Id)
                    .ToList();

                var deletedAnswers = answersRespository
                    .AsQueryable()
                    .Where(a => a.QuestionId == questionDto.Id && existedAnswerIds.Contains(a.Id) == false && a.IsDeleted == false)
                    .ToList();

                foreach (var answer in deletedAnswers)
                {
                    answer.MarkAsDeleted();
                    answersRespository.Update(answer);
                }

                Uow.SaveChanges();

                return Result<bool>.Ok(true);
            }
            catch (Exception exception)
            {
                return Result<bool>.Fail(false, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}