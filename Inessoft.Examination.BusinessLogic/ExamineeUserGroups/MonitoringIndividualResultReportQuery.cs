﻿using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Requests;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using Microsoft.EntityFrameworkCore;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class MonitoringIndividualResultReportQuery : BaseQuery
    {
        public Result<MonitoringIndividualResultReportDto> Execute(MonitoringIndividualResultReportRequest request)
        {
            var examUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();

            var result = examUserGroupRepository
                .AsQueryable()
                .Include(g => g.Examination)
                .ThenInclude(g => g.ExaminationCategories)
                .ThenInclude(g => g.Category)
                .ThenInclude(g => g.CategoryTranslates)
                .Include(g => g.ExaminationResultType)
                .ThenInclude(g => g.ExaminationResultTypeTranslates)
                .FirstOrDefault(g => g.Id == request.UserGroupId
                                     && g.ExaminationId == request.ExaminationId
                                     && g.IsFullTestPassed
                                     && g.IsDeleted == false);

            if (result != null)
            {
                var examUserGroupHistoryRepository = Uow.GetRepository<UserAnswerHistory>();

                var userHistory = examUserGroupHistoryRepository
                    .AsQueryable()
                    .Include(r => r.Question)
                    .Where(r => r.ExamineeUsergroupId == request.UserGroupId
                                && r.IsRightAnswer)
                    .ToList();

                var monitoringIndividualResultReportDto = new MonitoringIndividualResultReportDto
                {
                    UserName = $"{result.SecondName} {result.FirstName} {result.MiddleName}",
                    ChairmanName = result.Examination.ChairmanName,
                    ExaminationPassedDate = result.ExaminationDateComplite,
                    ExaminationPoints = result.TestResultEvaluation,
                    OralExaminationPoints = result.OralExamPoint,
                    ExaminationResult = result.ExaminationResultType.GetSystemLanguageQuestionTranslate() == null
                        ? ""
                        : result.ExaminationResultType.GetSystemLanguageQuestionTranslate().Text,
                    IsPassed = result.IsFullTestPassed,
                    Categories = result.Examination.ExaminationCategories.Where(c => c.IsDeleted == false).Select(c =>
                        new MonitoringIndividualResultReportCategories
                        {
                            CategotyName = c.Category.GetSystemLanguageCategoryTranslate().Text,
                            QuestionCount = c.CategotyQuestionCount,
                            RightQuestionCount = userHistory.Count(r => r.Question.CategoryId == c.CategoryId)
                        }).ToList()
                };

                return Result<MonitoringIndividualResultReportDto>.Ok(monitoringIndividualResultReportDto);
            }


            return Result<MonitoringIndividualResultReportDto>.Ok(null);
        }
    }
}