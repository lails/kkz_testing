﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class CreateExaminationUserGroupsCommand : BaseCommand
    {
        public void Execute(List<ExaminationUserGroupDto> usersGroupDtos, Guid examinationId)
        {
            var examinationUserGroupsRepository = Uow.GetRepository<ExaminationUserGroup>();

            foreach (var usersGroupDto in usersGroupDtos)
            {
                var userGroup = examinationUserGroupsRepository
                    .AsQueryable()
                    .FirstOrDefault(g => g.ExaminationId == examinationId && g.UserId == usersGroupDto.Id && g.IsDeleted == false);

                if (userGroup != null)
                {
                    continue;
                }

                userGroup = usersGroupDto.ToExaminationUserGroup;
                userGroup.ExaminationId = examinationId;
                examinationUserGroupsRepository.Create(userGroup);
            }

            Uow.SaveChanges();
        }
    }
}