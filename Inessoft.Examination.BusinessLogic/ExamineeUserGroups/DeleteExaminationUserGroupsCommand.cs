﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class DeleteExaminationUserGroupsCommand : BaseCommand
    {
        public void Execute(List<ExaminationUserGroupDto> usersGroupDtos, Guid examinationId)
        {
            var examinationUserGroupsRepository = Uow.GetRepository<ExaminationUserGroup>();

            var existedUserIds = usersGroupDtos.Select(g => g.Id);

            var deletedUserGroups = examinationUserGroupsRepository
                .AsQueryable()
                .Where(g => g.ExaminationId == examinationId && existedUserIds.Contains(g.UserId) == false && g.IsDeleted == false)
                .ToList();

            foreach (var userGroup in deletedUserGroups)
            {
                userGroup.MarkAsDeleted();
                examinationUserGroupsRepository.Update(userGroup);
            }

            Uow.SaveChanges();
        }
    }
}