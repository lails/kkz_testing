﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Requests;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDataAccess.BaseResponses;
using NetCoreDataAccess.Externsions;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class GetMonitoringExaminationUserGroupQuery : BaseQuery
    {
        public Result<PagedListResponse<MonitoringExaminationUserGroupDto>> Execute(MonitoringExaminationUserGroupPagedRequest request)
        {
            try
            {
                var userGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var response = new PagedListResponse<MonitoringExaminationUserGroupDto>();

                var query = userGroupRepository
                    .AsQueryable()
                    .Include(u => u.Examination)
                    .Include(u => u.ExaminationResultType)
                    .ThenInclude(rt => rt.ExaminationResultTypeTranslates)
                    .Where(u => u.IsDeleted == false);

                if (request.HasExaminationId)
                {
                    query = query.Where(u => u.ExaminationId == request.ExaminationId.Value);
                }

                response.Items = query
                    .ApplyPagedListRequest(request, response)
                    .AsEnumerable()
                    .Select(MonitoringExaminationUserGroupDto.Map)
                    .ToArray();

                return Result<PagedListResponse<MonitoringExaminationUserGroupDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<PagedListResponse<MonitoringExaminationUserGroupDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}