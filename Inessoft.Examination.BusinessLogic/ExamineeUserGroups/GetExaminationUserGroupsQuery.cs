﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class GetExaminationUserGroupsQuery : BaseQuery
    {
        public Result<List<ExaminationUserGroupDto>> Execute(Guid examinationId)
        {
            try
            {
                var questionsRespository = Uow.GetRepository<ExaminationUserGroup>();
                var response = questionsRespository
                    .AsQueryable()
                    .Where(g => g.ExaminationId == examinationId && g.IsDeleted == false)
                    .Select(ExaminationUserGroupDto.Map)
                    .ToList();

                return Result<List<ExaminationUserGroupDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationUserGroupDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}