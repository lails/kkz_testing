﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups
{
    public class UpdateExaminationUserGroupCommand : BaseCommand
    {
        public Result<bool> Execute(MonitoringExaminationUserGroupDto usersGroupDto)
        {
            try
            {
                var examinationUserGroupsRepository = Uow.GetRepository<ExaminationUserGroup>();

                var userGroup = examinationUserGroupsRepository
                    .AsQueryable()
                    .Include(r => r.Examination)
                    .FirstOrDefault(g => g.Id == usersGroupDto.Id && g.IsDeleted == false);

                if (userGroup == null)
                {
                    return Result<bool>.Fail(false, "Запись отсутствует в базе");
                }

                usersGroupDto.UpdateExaminationUserGroup(userGroup);
                examinationUserGroupsRepository.Update(userGroup);

                Uow.SaveChanges();

                return Result<bool>.Ok(true);
            }
            catch (Exception exception)
            {
                return Result<bool>.Fail(false, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}