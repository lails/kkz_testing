﻿using System;
using System.Collections.Generic;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos
{
    public class MonitoringIndividualResultReportDto
    {
        public string UserName { get; set; }
        public DateTime? ExaminationPassedDate { get; set; }
        public List<MonitoringIndividualResultReportCategories> Categories { get; set; }
        public decimal ExaminationPoints { get; set; }
        public decimal OralExaminationPoints { get; set; }
        public string ExaminationResult { get; set; }
        public string ChairmanName { get; set; }
        public bool IsPassed { get; set; }
    }

    public class MonitoringIndividualResultReportCategories
    {
        public string CategotyName { get; set; }
        public int QuestionCount { get; set; }
        public int RightQuestionCount { get; set; }
    }
}