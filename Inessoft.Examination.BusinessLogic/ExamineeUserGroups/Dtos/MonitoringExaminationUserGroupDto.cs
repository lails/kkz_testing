﻿using System;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos
{
    public class MonitoringExaminationUserGroupDto
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }
        public string FactStartDate { get; set; }
        public string FactEndDate { get; set; }

        public Guid? StateId { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }

        public decimal ExaminationPoints { get; set; }
        public short OralExaminationPoints { get; set; }

        public string Comment { get; set; }

        public bool IsSelected { get; set; }

        public bool IsPassed { get; set; }

        public bool IsStateEditAvailable { get; set; }
        public bool IsOralResultEditAvailable { get; set; }

        public static Func<ExaminationUserGroup, MonitoringExaminationUserGroupDto> Map = userGroup =>
            new MonitoringExaminationUserGroupDto
            {
                Id = userGroup.Id,
                UserName = $"{userGroup.SecondName} {userGroup.FirstName} {userGroup.MiddleName}",
                Comment = userGroup.Comment,
                FactStartDate = userGroup.ExaminationDateStart.HasValue ? userGroup.ExaminationDateStart.Value.ToString("dd.MM.yyyy") : string.Empty,
                FactEndDate = userGroup.ExaminationDateEnd.HasValue ? userGroup.ExaminationDateEnd.Value.ToString("dd.MM.yyyy") : string.Empty,
                ExaminationPoints = userGroup.TestResultEvaluation,
                OralExaminationPoints = userGroup.OralExamPoint,
                StateId = userGroup.ExaminationResultTypeId,
                IsPassed = userGroup.IsFullTestPassed,
                IsStateEditAvailable = userGroup.Examination.EndDate <= ExaminationAmbientContext.Current.DateTimeProvider.Now,
                IsOralResultEditAvailable = userGroup.TestResultEvaluation != 0,
                StateName = userGroup.ExaminationResultType == null ? string.Empty : userGroup.ExaminationResultType.GetSystemLanguageQuestionTranslate()?.Text,
                StateCode = userGroup.ExaminationResultType?.Code
            };

        public void UpdateExaminationUserGroup(ExaminationUserGroup userGroup)
        {
            userGroup.ExaminationResultTypeId = StateId;
            userGroup.OralExamPoint = OralExaminationPoints;
            userGroup.Comment = Comment;
            //Экзамен считается пройденным(не обязательно успешно), если прошел до конца электронный экзамен и прошел устный экзамен
            userGroup.IsFullTestPassed = userGroup.IsComputerTestFinished && OralExaminationPoints > 0;
        }
    }
}