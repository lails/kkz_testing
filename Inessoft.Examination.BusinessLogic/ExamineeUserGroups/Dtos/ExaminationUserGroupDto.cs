﻿using System;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using Inessoft.Examination.Domain.Testing;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Dtos
{
    public class ExaminationUserGroupDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SecondName { get; set; }
        public string Inn { get; set; }

        public static Expression<Func<ExaminationUserGroup, ExaminationUserGroupDto>> Map = user => new ExaminationUserGroupDto
        {
            Id = user.UserId,
            Inn = user.Inn,
            FirstName = user.FirstName,
            MiddleName = user.MiddleName,
            SecondName = user.SecondName
        };

        [IgnoreDataMember]
        public ExaminationUserGroup ToExaminationUserGroup => new ExaminationUserGroup
        {
            UserId = Id,
            FirstName = FirstName,
            MiddleName = MiddleName,
            SecondName = SecondName,
            Inn = Inn
        };

        public void UpdateExaminationUserGroup(ExaminationUserGroup examinationUserGroup)
        {
            examinationUserGroup.FirstName = FirstName;
            examinationUserGroup.MiddleName = MiddleName;
            examinationUserGroup.SecondName = SecondName;
            examinationUserGroup.Inn = Inn;
        }
    }
}