﻿using System;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Requests
{
    public class MonitoringIndividualResultReportRequest
    {
        public Guid ExaminationId { get; set; }
        public Guid UserGroupId { get; set; }
    }
}