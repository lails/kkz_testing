﻿using System;
using NetCoreDataAccess.BaseRequests;

namespace Inessoft.Examination.BusinessLogic.ExamineeUserGroups.Requests
{
    public class MonitoringExaminationUserGroupPagedRequest : PagedListRequest
    {
        public Guid? ExaminationId { get; set; }

        public bool HasExaminationId => ExaminationId.HasValue;
    }
}
