﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategorySettings
{
    public class GetExaminationSettingCategoriesQuery : BaseQuery
    {
        public Result<List<ExaminationCategoryDto>> Execute(Guid examinationSettingId)
        {
            try
            {
                var categoryRepository = Uow.GetRepository<ExaminationSettingCategory>();
                var response = categoryRepository
                    .AsQueryable()
                    .Include(e => e.Category)
                    .ThenInclude(ec => ec.CategoryTranslates)
                    .Include(c => c.Category)
                    .ThenInclude(ec => ec.Questions)
                    .Where(c => c.ExaminationSettingId == examinationSettingId && c.IsDeleted == false)
                    .AsEnumerable()
                    .Select(ExaminationCategoryDto.MapToExamenSettingCategory)
                    .ToList();

                return Result<List<ExaminationCategoryDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationCategoryDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}