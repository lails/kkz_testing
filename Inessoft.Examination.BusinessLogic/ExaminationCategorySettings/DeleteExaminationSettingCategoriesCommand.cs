﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategorySettings
{
    public class DeleteExaminationSettingCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos, Guid examinationSettingId)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationSettingCategory>();

            var existedExaminationSettingCategoryIds = examinationCategoryDtos.Select(e => e.CategoryId);

            var deletedCategories = examinationCategoriesRepository
                .AsQueryable()
                .Where(c => c.ExaminationSettingId== examinationSettingId && existedExaminationSettingCategoryIds.Contains(c.CategoryId) == false && c.IsDeleted == false)
                .ToList();

            foreach (var deletedCategory in deletedCategories)
            {
                deletedCategory.MarkAsDeleted();
                examinationCategoriesRepository.Update(deletedCategory);
            }

            Uow.SaveChanges();
        }
    }
}