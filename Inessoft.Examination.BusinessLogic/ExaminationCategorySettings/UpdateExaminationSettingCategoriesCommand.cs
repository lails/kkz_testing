﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategorySettings
{
    public class UpdateExaminationSettingCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationSettingCategory>();

            var updatedCategoriesDtos = examinationCategoryDtos
                .Where(e => e.Id != Guid.Empty)
                .ToList();

            foreach (var updatedCategoriesDto in updatedCategoriesDtos)
            {
                var originalCategory = examinationCategoriesRepository
                    .AsQueryable()
                    .FirstOrDefault(c => c.Id ==  updatedCategoriesDto.Id);

                if (originalCategory == null)
                {
                    continue;
                }

                updatedCategoriesDto.UpdateExaminationSettingCategory(originalCategory);
                examinationCategoriesRepository.Update(originalCategory);
            }

            Uow.SaveChanges();
        }
    }
}