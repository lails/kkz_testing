﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategorySettings
{
    public class CreateExaminationSettingCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos, Guid examinationId)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationSettingCategory>();

            var createdCategoriesDtos = examinationCategoryDtos
                .Where(e => e.Id == Guid.Empty)
                .ToList();

            foreach (var createdCategoryDto in createdCategoriesDtos)
            {
                var category = createdCategoryDto.ToExaminationSettingCategory;
                category.ExaminationSettingId = examinationId;
                examinationCategoriesRepository.Create(category);
            }

            Uow.SaveChanges();
        }
    }
}