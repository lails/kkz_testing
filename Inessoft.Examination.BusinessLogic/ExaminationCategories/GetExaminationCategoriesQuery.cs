﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategories
{
    public class GetExaminationCategoriesQuery : BaseQuery
    {
        public Result<List<ExaminationCategoryDto>> Execute(Guid examinationId)
        {
            try
            {
                var categoryRepository = Uow.GetRepository<ExaminationCategory>();
                var response = categoryRepository
                    .AsQueryable()
                    .Include(e => e.Category)
                    .ThenInclude(ec => ec.CategoryTranslates)
                    .Include(c => c.Category)
                    .ThenInclude(ec => ec.Questions)
                    .Where(c => c.ExaminationId == examinationId && c.IsDeleted == false)
                    .AsEnumerable()
                    .Select(ExaminationCategoryDto.MapToExamenCategory)
                    .ToList();

                return Result<List<ExaminationCategoryDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationCategoryDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}