﻿using System;
using Inessoft.Examination.Domain.Dictionaries;
using Inessoft.Examination.Domain.Testing;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos
{
    public class ExaminationCategoryDto
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryQuestionCount { get; set; }
        public int FullCategoryQuestionCount { get; set; }

        public static Func<ExaminationCategory, ExaminationCategoryDto> MapToExamenCategory = examCategory => new ExaminationCategoryDto
        {
            Id = examCategory.Id,
            CategoryId = examCategory.CategoryId,
            CategoryName = examCategory.Category?.GetSystemLanguageCategoryTranslate()?.Text,
            CategoryQuestionCount = examCategory.CategotyQuestionCount,
            FullCategoryQuestionCount = examCategory.Category?.Questions?.Count ?? 0
        };

        public static Func<ExaminationSettingCategory, ExaminationCategoryDto> MapToExamenSettingCategory = examinationSettingCategory => new ExaminationCategoryDto
        {
            Id = examinationSettingCategory.Id,
            CategoryId = examinationSettingCategory.CategoryId,
            CategoryName = examinationSettingCategory.Category?.GetSystemLanguageCategoryTranslate()?.Text,
            CategoryQuestionCount = examinationSettingCategory.CategotyQuestionCount,
            FullCategoryQuestionCount = examinationSettingCategory.Category?.Questions?.Count ?? 0
        };

        public static Func<Category, ExaminationCategoryDto> MapToExaminationSettingParentCategoryDto = category => new ExaminationCategoryDto
        {
            Id = category.Id,
            CategoryId = category.Id,
            CategoryName = category?.GetSystemLanguageCategoryTranslate()?.Text
        };

        public ExaminationCategory ToExaminationCategory => new ExaminationCategory
        {
            Id = Id,
            CategoryId = CategoryId,
            CategotyQuestionCount = CategoryQuestionCount
        };

        public void UpdateExaminationCategory(ExaminationCategory examinationCategory)
        {
            examinationCategory.CategotyQuestionCount = CategoryQuestionCount;
        }
        

        public ExaminationSettingCategory ToExaminationSettingCategory => new ExaminationSettingCategory
        {
            Id = Id,
            CategoryId = CategoryId,
            CategotyQuestionCount = CategoryQuestionCount
        };

        public void UpdateExaminationSettingCategory(ExaminationSettingCategory examinationSettingCategory)
        {
            examinationSettingCategory.CategotyQuestionCount = CategoryQuestionCount;
        }
    }
}