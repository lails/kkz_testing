﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategories
{
    public class CreateExaminationCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos, Guid examinationId)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationCategory>();

            var createdCategoriesDtos = examinationCategoryDtos
                .Where(e => e.Id == Guid.Empty)
                .ToList();

            foreach (var createdCategoryDto in createdCategoriesDtos)
            {
                var category = createdCategoryDto.ToExaminationCategory;
                category.ExaminationId = examinationId;
                examinationCategoriesRepository.Create(category);
            }

            Uow.SaveChanges();
        }
    }
}