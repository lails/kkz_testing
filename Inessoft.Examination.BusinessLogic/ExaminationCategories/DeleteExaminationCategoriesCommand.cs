﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategories
{
    public class DeleteExaminationCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos, Guid examinationId)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationCategory>();

            var existedCategoryIds = examinationCategoryDtos.Select(e => e.CategoryId);

            var deletedCategories = examinationCategoriesRepository
                .AsQueryable()
                .Where(c => c.ExaminationId == examinationId && existedCategoryIds.Contains(c.CategoryId) == false && c.IsDeleted == false)
                .ToList();

            foreach (var deletedCategory in deletedCategories)
            {
                deletedCategory.MarkAsDeleted();
                examinationCategoriesRepository.Update(deletedCategory);
            }

            Uow.SaveChanges();
        }
    }
}