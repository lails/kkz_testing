﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.ExaminationCategories
{
    public class UpdateExaminationCategoriesCommand : BaseCommand
    {
        public void Execute(List<ExaminationCategoryDto> examinationCategoryDtos)
        {
            var examinationCategoriesRepository = Uow.GetRepository<ExaminationCategory>();

            var updatedCategoriesDtos = examinationCategoryDtos
                .Where(e => e.Id != Guid.Empty)
                .ToList();

            foreach (var updatedCategoriesDto in updatedCategoriesDtos)
            {
                var originalCategory = examinationCategoriesRepository
                    .AsQueryable()
                    .FirstOrDefault(c => c.Id ==  updatedCategoriesDto.Id);

                if (originalCategory == null)
                {
                    continue;
                }

                updatedCategoriesDto.UpdateExaminationCategory(originalCategory);
                examinationCategoriesRepository.Update(originalCategory);
            }

            Uow.SaveChanges();
        }
    }
}