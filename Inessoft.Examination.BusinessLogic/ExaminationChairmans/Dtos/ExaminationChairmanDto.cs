﻿using System;
using System.Linq.Expressions;
using Inessoft.Examination.Domain.Dictionaries;
using Inessoft.Examination.Domain.Testing;

namespace Inessoft.Examination.BusinessLogic.ExaminationChairmans.Dtos
{
    public class ExaminationChairmanDto
    {
        public Guid Id { get; set; }
        public string FIO { get; set; }
        public string INN { get; set; }

        public static Func<Domain.Testing.Examination, ExaminationChairmanDto> Map = chairman => new ExaminationChairmanDto
        {
            Id = chairman.ChairmanId,
            FIO = chairman.ChairmanName,
            INN = chairman.ChairmanInn
        };

        public static Func<ExaminationSetting, ExaminationChairmanDto> MapFromSettings = chairman => new ExaminationChairmanDto
        {
            Id = chairman.ChairmanId,
            FIO = chairman.ChairmanName,
            INN = chairman.ChairmanInn
        };
    }
}