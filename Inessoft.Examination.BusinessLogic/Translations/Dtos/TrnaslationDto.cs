﻿namespace Inessoft.Examination.BusinessLogic.Translations.Dtos
{
    public class TrnaslationDto
    {
        public string FromLanguageCode { get; set; }
        public string ToLanguageCode { get; set; }
        public bool IsOnlyNotTranslated { get; set; }
        public int? TakeCount { get; set; }
    }
}
