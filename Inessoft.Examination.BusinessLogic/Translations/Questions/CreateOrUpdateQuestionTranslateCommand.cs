﻿using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Translations.Questions
{
    public class CreateOrUpdateQuestionTranslateCommand : BaseCommand
    {
        public Result<Guid> Execute(QuestionTranslate answerTranslate)
        {
            try
            {
                var questionRepository = Uow.GetRepository<QuestionTranslate>();

                var result = questionRepository.AsQueryable().FirstOrDefault(r => r.QuestionId  == answerTranslate.QuestionId && r.LanguageCode == answerTranslate.LanguageCode);
                if (result != null)
                {
                    result.Text = answerTranslate.Text;
                }
                else
                {
                    questionRepository.Create(answerTranslate);
                }

                Uow.SaveChanges();

                return Result<Guid>.Ok(answerTranslate.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
