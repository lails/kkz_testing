﻿using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Translations.Questions
{
    public class CreateOrUpdateAnswerTranslateCommand : BaseCommand
    {
        public Result<Guid> Execute(AnswerTranslate answerTranslate)
        {
            try
            {
                var answerRepository = Uow.GetRepository<AnswerTranslate>();

                var result = answerRepository.AsQueryable().FirstOrDefault(r => r.AnswerId == answerTranslate.AnswerId && r.LanguageCode == answerTranslate.LanguageCode);
                if(result != null)
                {
                    result.Text = answerTranslate.Text;
                }
                else
                {
                    answerRepository.Create(answerTranslate);
                }

                Uow.SaveChanges();

                return Result<Guid>.Ok(answerTranslate.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
