﻿using Inessoft.Examination.Domain.Testing.TestingTranslate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inessoft.Examination.BusinessLogic.Translations.Questions.Dtos
{
    public class QuestoinTranslateDto
    {
        public List<QuestionTranslate> QuestionTranslates { get; set; }
        public List<List<AnswerTranslate>> AnswerTranslates { get; set; }
    }
}
