﻿using Inessoft.Examination.BusinessLogic.Translations.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inessoft.Examination.BusinessLogic.Translations.Questions.Dtos
{
    public class RequestQuestionTrnaslationDto: TrnaslationDto
    {
        public Guid? CategoryId { get; set; }
        public string ArticleNumber { get; set; }
        public string QuestionText { get; set; }
    }
}
