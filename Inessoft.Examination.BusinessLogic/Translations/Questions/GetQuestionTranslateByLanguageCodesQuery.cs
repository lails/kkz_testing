﻿using Inessoft.Examination.BusinessLogic.Translations.Dtos;
using Inessoft.Examination.BusinessLogic.Translations.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Translations.Questions
{
    public class GetQuestionTranslateByLanguageCodesQuery : BaseQuery
    {
        public Result<List<QuestoinTranslateDto>> Execute(RequestQuestionTrnaslationDto requestQuestionTrnaslationDto)
        {
            if (!requestQuestionTrnaslationDto.TakeCount.HasValue || requestQuestionTrnaslationDto.TakeCount.Value == 0)
            {
                requestQuestionTrnaslationDto.TakeCount = 30;
            }
            var languageCodes = new List<string> { requestQuestionTrnaslationDto.FromLanguageCode, requestQuestionTrnaslationDto.ToLanguageCode };

            var questionRepository = Uow.GetRepository<QuestionTranslate>();

            var questionResult = questionRepository.AsQueryable()
                .Include(r => r.Question)
                .Where(r => r.Question.IsDeleted == false && languageCodes.Contains(r.LanguageCode));

            if(requestQuestionTrnaslationDto.CategoryId.HasValue == true)
            {
                questionResult = questionResult.Where(r => r.Question.CategoryId == requestQuestionTrnaslationDto.CategoryId);
            }

            if(string.IsNullOrEmpty(requestQuestionTrnaslationDto.ArticleNumber) == false)
            {
                questionResult = questionResult.Where(r => r.Question.ArticleNumber == requestQuestionTrnaslationDto.ArticleNumber);
            }

            if(string.IsNullOrEmpty(requestQuestionTrnaslationDto.QuestionText) == false)
            {
                questionResult = questionResult.Where(r => r.Text.ToLower().Contains(requestQuestionTrnaslationDto.QuestionText.ToLower()));
            }

            #region Question Filter

            var groupped = questionResult.GroupBy(r => new { r.QuestionId });
            var questionFilterResult = new List<QuestionTranslate>();

            if (requestQuestionTrnaslationDto.IsOnlyNotTranslated)
            {
                var noTranslatedCategoryIds = groupped.Where(r => r.Count() == 1).Select(r => r.Key.QuestionId);
                questionResult = questionResult.Where(r => noTranslatedCategoryIds.Contains(r.QuestionId));

                questionFilterResult = questionResult.Take(requestQuestionTrnaslationDto.TakeCount.Value).ToList();
            }
            else
            {
                var translatedCategoryIds = groupped.Where(g => g.Count() > 1).Select(g => g.Key.QuestionId).Take(requestQuestionTrnaslationDto.TakeCount.Value);

                var translatedCount = translatedCategoryIds.Count();

                var translatedCategory = questionResult.Where(r => translatedCategoryIds.Contains(r.QuestionId))
                                            .Take(requestQuestionTrnaslationDto.TakeCount.Value + translatedCount);

                var notTranslatedCategory = questionResult.Where(r => !translatedCategoryIds.Contains(r.QuestionId))
                                                .Take((requestQuestionTrnaslationDto.TakeCount.Value + translatedCount) - translatedCategory.Count());

                questionFilterResult = translatedCategory.ToList().Union(notTranslatedCategory.ToList()).ToList();
            }
            #endregion


            var answerRepository = Uow.GetRepository<Answer>();

            var answerQuery = answerRepository.AsQueryable()
                .Where(r => r.IsDeleted == false);            

            var answerResult = answerQuery.Where(r => questionFilterResult.Select(q => q.QuestionId).Contains(r.QuestionId))
                .Select(r => new
                {
                    r.QuestionId,
                    AnswerTranslates = r.AnswerTranslates.Where(a => languageCodes.Contains(a.LanguageCode))
                }).ToList();

            var questoinTranslateDtos = new List<QuestoinTranslateDto>();

            foreach (var questionId in questionFilterResult.Select(r => r.QuestionId).Distinct())
            {

                var questionTranslate = new QuestoinTranslateDto
                {
                    QuestionTranslates = questionFilterResult.Where(r => r.QuestionId == questionId).Select(r =>
                    new QuestionTranslate
                    {
                        Id = r.Id,
                        LanguageCode = r.LanguageCode,
                        QuestionId = r.QuestionId,
                        Text = r.Text
                    }).ToList(),

                    AnswerTranslates = new List<List<AnswerTranslate>>()
                };
                foreach (var answerTranslates in answerResult.Where(r => r.QuestionId == questionId).Select(r => r.AnswerTranslates))
                {
                    var res = answerTranslates.Select(r => new AnswerTranslate
                    {
                        Id = r.Id,
                        LanguageCode = r.LanguageCode,
                        AnswerId = r.AnswerId,
                        Text = r.Text,
                        QuestionId = questionId
                    }).ToList();

                    questionTranslate.AnswerTranslates.Add(res);
                }

                questoinTranslateDtos.Add(questionTranslate);
            }

            return Result<List<QuestoinTranslateDto>>.Ok(questoinTranslateDtos);
        }
    }
}
