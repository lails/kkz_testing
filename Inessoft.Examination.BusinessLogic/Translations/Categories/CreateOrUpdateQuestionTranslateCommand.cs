﻿using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Translations.Categories
{
    public class CreateOrUpdateCategoryTranslateCommand : BaseCommand
    {
        public Result<Guid> Execute(CategoryTranslate categoryTranslate)
        {
            try
            {
                var questionRepository = Uow.GetRepository<CategoryTranslate>();

                var result = questionRepository.AsQueryable().FirstOrDefault(r => r.CategoryId  == categoryTranslate.CategoryId && r.LanguageCode == categoryTranslate.LanguageCode);
                if (result != null)
                {
                    result.Text = categoryTranslate.Text;
                }
                else
                {
                    questionRepository.Create(categoryTranslate);
                }

                Uow.SaveChanges();

                return Result<Guid>.Ok(categoryTranslate.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
