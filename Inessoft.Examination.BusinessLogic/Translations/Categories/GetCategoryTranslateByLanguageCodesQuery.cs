﻿using Inessoft.Examination.BusinessLogic.Translations.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Translations.Categories
{
    public class GetCategoryTranslateByLanguageCodesQuery : BaseQuery
    {
        public Result<List<CategoryTranslate>> Execute(TrnaslationDto translationDto)
        {
            if(!translationDto.TakeCount.HasValue || translationDto.TakeCount.Value == 0)
            {
                translationDto.TakeCount = 30;
            }

            var languageCodes = new List<string> { translationDto.FromLanguageCode, translationDto.ToLanguageCode };

            var cateoryTranslateRepository = Uow.GetRepository<CategoryTranslate>();


            var query = cateoryTranslateRepository.AsQueryable()
                .Include(r => r.Category)
                .Where(r => r.Category != null && r.Category.IsDeleted == false)
                .Where(r => languageCodes.Contains(r.LanguageCode))
                .Select(r => new CategoryTranslate { CategoryId = r.CategoryId, LanguageCode = r.LanguageCode, Text = r.Text });

            #region filter

            var groupped = query.GroupBy(r => new { r.CategoryId });

            if (translationDto.IsOnlyNotTranslated)
            {
                var noTranslatedCategoryIds = groupped.Where(r => r.Count() == 1).Select(r => r.Key.CategoryId);
                query = query.Where(r => noTranslatedCategoryIds.Contains(r.CategoryId));
                
                query = query.Take(translationDto.TakeCount.Value);
            }
            else
            {
                var translatedCategoryIds = groupped.Where(g => g.Count() > 1).Select(g => g.Key.CategoryId).Take(translationDto.TakeCount.Value);

                var translatedCount = translatedCategoryIds.Count();

                var translatedCategory = query.Where(r => translatedCategoryIds.Contains(r.CategoryId))
                                            .Take(translationDto.TakeCount.Value + translatedCount);

                var notTranslatedCategory = query.Where(r => !translatedCategoryIds.Contains(r.CategoryId))
                                                .Take((translationDto.TakeCount.Value + translatedCount) - translatedCategory.Count());

                query = translatedCategory.Union(notTranslatedCategory);
            }

            #endregion

            var result = query.ToList();

            return Result<List<CategoryTranslate>>.Ok(result);
        }
    }
}
