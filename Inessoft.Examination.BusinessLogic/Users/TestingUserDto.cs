﻿using System;
using Inessoft.Examination.Domain.Testing;
using NetCoreIdentityHttpClient.Dtos;

namespace Inessoft.Examination.BusinessLogic.Users
{
    public class TestingUserDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SecondName { get; set; }
        public string Position { get; set; }
        public string Inn { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public RoleDto Role { get; set; }

        public Guid? CategoryId { get; set; }

        public UserDto ToUserDto => new UserDto
        {
            Id = Id,
            FirstName = FirstName,
            MiddleName = MiddleName,
            SecondName = SecondName,
            Position = Position,
            Inn = Inn,
            Birthdate = Birthdate,
            Address = Address,
            PhoneNumber = PhoneNumber,
            Email = Email,
            IsActive = IsActive,
            IsDeleted = IsDeleted,
            //Password = Inn,
            Role = Role
        };

        public UserSetting CreateUserSetting => new UserSetting
        {
            UserId = Id,
            CategoryId = CategoryId == Guid.Empty ? null : CategoryId
        };

        public void UpdateUserSetting(UserSetting userSetting)
        {
            userSetting.UserId = Id;
            userSetting.CategoryId = CategoryId == Guid.Empty ? null : CategoryId;
        }

        public static TestingUserDto FromUserDto(UserDto user)
        {
            return new TestingUserDto
            {
                Id = user.Id,
                Inn = user.Inn,
                MiddleName = user.MiddleName,
                FirstName = user.FirstName,
                SecondName = user.SecondName,
                IsActive = user.IsActive
            };
        }
    }
}