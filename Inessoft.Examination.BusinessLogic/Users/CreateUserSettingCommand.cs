﻿using System;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Users
{
    public class CreateUserSettingCommand : BaseCommand
    {
        public Result<Guid> Execute(TestingUserDto testingUserDto)
        {
            var userSettingsRepository = Uow.GetRepository<UserSetting>();
            var userSetting = testingUserDto.CreateUserSetting;
            userSettingsRepository.Create(userSetting);
            Uow.SaveChanges();
            return Result<Guid>.Ok(userSetting.Id);
        }
    }
}