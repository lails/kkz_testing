﻿using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Users
{
    public class GetUserSettingQueryByUserId : BaseQuery
    {
        public Result<UserSetting> Execute(Guid userId)
        {
            try
            {
                var questionsRespository = Uow.GetRepository<UserSetting>();

                var userSetting = questionsRespository
                    .AsQueryable()
                    .Include(r=>r.Category)
                    .FirstOrDefault(r => r.UserId == userId);

                return Result<UserSetting>.Ok(userSetting);
            }
            catch (Exception exception)
            {
                return Result<UserSetting>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
