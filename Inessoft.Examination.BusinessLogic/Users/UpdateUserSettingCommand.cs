﻿using System;
using System.Linq;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Users
{
    public class UpdateUserSettingCommand : BaseCommand
    {
        public Result<Guid> Execute(TestingUserDto testingUserDto)
        {
            var userSettingsRepository = Uow.GetRepository<UserSetting>();
            var userSetting = userSettingsRepository
                .AsQueryable()
                .FirstOrDefault(u => u.UserId == testingUserDto.Id);

            if (userSetting == null)
            {
                userSetting = testingUserDto.CreateUserSetting;
                userSettingsRepository.Create(userSetting);
            }
            else
            {
                testingUserDto.UpdateUserSetting(userSetting);
                userSettingsRepository.Update(userSetting);
            }

            Uow.SaveChanges();

            return Result<Guid>.Ok(userSetting.Id);
        }
    }
}