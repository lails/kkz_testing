﻿using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.Users
{
    public class GetUserSettingsQueryByCategoryId : BaseQuery
    {
        public Result<List<UserSetting>> Execute(Guid categoryId)
        {
            try
            {
                var questionsRespository = Uow.GetRepository<UserSetting>();

                var userSetting = questionsRespository
                    .AsQueryable()
                    .Include(r=>r.Category)
                    .Where(r => r.CategoryId == categoryId)
                    .ToList();

                return Result<List<UserSetting>>.Ok(userSetting);
            }
            catch (Exception exception)
            {
                return Result<List<UserSetting>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
