﻿using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class AddQuestionToUserAnswerHistoryCommand : BaseCommand
    {
        public void Execute(Guid questionId, Guid examId)
        {
            var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

            var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
            var examinationUserGroup = examinationUserGroupRepository
                                        .AsQueryable()
                                        .FirstOrDefault(r => r.ExaminationId == examId
                                                            && r.UserId == currentUserId);

            var userAnswerHistoryRepository = Uow.GetRepository<UserAnswerHistory>();

            var isExistsQuestion = userAnswerHistoryRepository.AsQueryable()
                                                    .Any(r => r.ExamineeUsergroupId == examinationUserGroup.Id
                                                            && r.ExaminationId == examId
                                                            && r.QuestionId == questionId);

            if (isExistsQuestion == true)
            {
                return;
            }

            var currentCuestoinCount = userAnswerHistoryRepository.AsQueryable()
                                                    .Count(r => r.ExamineeUsergroupId == examinationUserGroup.Id
                                                            && r.ExaminationId == examId);

            var newUserAnswerHistory = new UserAnswerHistory
            {
                ExaminationQuestionNumber = (currentCuestoinCount + 1),
                ExamineeUsergroupId = examinationUserGroup.Id,
                ExaminationId = examId,
                QuestionId = questionId
            };

            userAnswerHistoryRepository.Create(newUserAnswerHistory);

            Uow.SaveChanges();
        }
    }
}
