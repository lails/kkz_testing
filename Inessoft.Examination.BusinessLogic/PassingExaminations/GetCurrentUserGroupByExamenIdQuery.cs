﻿using Inessoft.Examination.BusinessLogic.PassingExaminations.Dtos;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class GetCurrentUserGroupByExamenIdQuery : BaseQuery
    {
        public Result<UserGroupDto> Execute(Guid examenId)
        {
            try
            {
                var currentDate = ExaminationAmbientContext.Current.DateTimeProvider.Now;
                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();

                var examen = examinationRepository
                                                .AsQueryable()
                                                .Include(e => e.ExamineTranslates)
                                                .Where(e => e.StartDate <= currentDate && currentDate <= e.EndDate && e.IsDeleted == false)
                                                .Where(e => e.Id == examenId);

                if (examen == null)
                {
                    return Result<UserGroupDto>.Fail(null, $"Can't get user group");
                }

                var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository
                                                .AsQueryable()
                                                .Include(r => r.Examination)
                                                .ThenInclude(r=>r.ExamineTranslates)
                                                .FirstOrDefault(r => r.Examination.Id == examenId
                                                           && r.IsComputerTestFinished == false
                                                           && r.IsDeleted == false
                                                           && r.IsFullTestPassed == false
                                                           && r.UserId == currentUserId
                                                           && r.ExaminationDateComplite >= currentDate);

                if (examinationUserGroup == null)
                {

                    return Result<UserGroupDto>.Fail(null, $"Can't get user group");
                }

                return Result<UserGroupDto>.Ok(new UserGroupDto
                {
                    ExaminationName = examinationUserGroup.Examination.GetSystemLanguageQuestionTranslate().Text,
                    ExaminationDateComplite = examinationUserGroup.ExaminationDateComplite.Value,
                    IsComputerTestFinished = examinationUserGroup.IsComputerTestFinished,
                    QuestionCount = examinationUserGroup.Examination.QuestionCount
                });
            }
            catch (Exception exception)
            {
                return Result<UserGroupDto>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
