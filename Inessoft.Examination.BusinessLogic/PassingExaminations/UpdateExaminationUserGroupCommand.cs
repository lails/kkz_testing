﻿using Inessoft.Examination.BusinessLogic.Common.Enums;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Dictionaries;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class FinishExamenCommand : BaseCommand
    {
        public Result<Guid> Execute(Guid examId, List<UserAnswerHistory> userAnswers)
        {
            try
            {
                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository
                                                .AsQueryable()
                                                .Include(r => r.Examination)
                                                .FirstOrDefault(r => r.ExaminationId == examId && r.UserId == currentUserId);

                examinationUserGroup.ExaminationDateEnd = DateTime.Now;

                var rightAnswersPointSummary = userAnswers.Where(r => r.IsRightAnswer && r.IsQuestionAnswered).Sum(r => r.AnswerEvaluation);

                examinationUserGroup.TestResultEvaluation = rightAnswersPointSummary;

                #region Пройден ли экзамен

                var isExamPassed = rightAnswersPointSummary >= examinationUserGroup.Examination.RequiredEvaluationSummary;
                var resultTypeRepository = Uow.GetRepository<ExaminationResultType>();
                var excludeTypes = new string[]
                {
                    ExaminationResultTypeCodes.Passed,
                    ExaminationResultTypeCodes.NotPassed
                };

                var resultTypes = resultTypeRepository
                                                .AsQueryable()
                                                .Where(r => excludeTypes.Contains(r.Code)).ToList();
                if (isExamPassed == true)
                {
                    examinationUserGroup.ExaminationResultTypeId = resultTypes.FirstOrDefault(r => r.Code == ExaminationResultTypeCodes.Passed).Id;
                }
                else if (isExamPassed == false)
                {
                    examinationUserGroup.ExaminationResultTypeId = resultTypes.FirstOrDefault(r => r.Code == ExaminationResultTypeCodes.NotPassed).Id;
                }

                #endregion

                examinationUserGroup.IsComputerTestFinished = true;

                Uow.SaveChanges();

                return Result<Guid>.Ok(examinationUserGroup.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
