﻿using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class DeleteUserAnswerHistoryAnswerCommand : BaseCommand
    {
        public void Execute(Guid userAnswerHistoryId)
        {
            var userAnswerHistoryAnswerRepository = Uow.GetRepository<UserAnswerHistoryAnswer>();

            var userAnswerHistoryAnswerForDelete = userAnswerHistoryAnswerRepository.AsQueryable()
                            .Where(r => r.UserAnswerHistoryId == userAnswerHistoryId);

            userAnswerHistoryAnswerRepository.DeleteRange(userAnswerHistoryAnswerForDelete);

            Uow.SaveChanges();
        }
    }
}
