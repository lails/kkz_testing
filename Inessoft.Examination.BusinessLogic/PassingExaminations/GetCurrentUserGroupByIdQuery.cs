﻿using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class GetCurrentUserGroupByIdQuery : BaseQuery
    {
        public Result<ExaminationUserGroup> Execute(Guid userGroupId)
        {
            try
            {
                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository
                                                .AsQueryable()
                                                .Include(r => r.Examination)
                                                .ThenInclude(r => r.ExamineTranslates)
                                                .Include(r=>r.ExaminationResultType)
                                                .ThenInclude(r=>r.ExaminationResultTypeTranslates)
                                                .FirstOrDefault(r => r.Id == userGroupId && r.UserId == currentUserId);

                if (examinationUserGroup == null)
                {

                    return Result<ExaminationUserGroup>.Fail(null, $"Can't get user group");
                }

                return Result<ExaminationUserGroup>.Ok(examinationUserGroup);
            }
            catch (Exception exception)
            {
                return Result<ExaminationUserGroup>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
