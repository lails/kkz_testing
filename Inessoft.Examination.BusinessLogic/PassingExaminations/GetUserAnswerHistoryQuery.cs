﻿using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class GetUserAnswerHistoryQuery : BaseQuery
    {
        public Result<List<UserAnswerHistory>> Execute(Guid examId)
        {
            var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

            var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
            var examinationUserGroup = examinationUserGroupRepository
                                            .AsQueryable()
                                            .FirstOrDefault(r => r.ExaminationId == examId && r.UserId == currentUserId);

            var userAnswerHistoryRepository = Uow.GetRepository<UserAnswerHistory>();

            var userAnswerHistory = userAnswerHistoryRepository
                                            .AsQueryable()
                                            .Where(r => r.ExamineeUsergroupId == examinationUserGroup.Id
                                                        && r.ExaminationId == examId)
                                            .ToList();

            return Result<List<UserAnswerHistory>>.Ok(userAnswerHistory);
        }
    }
}

