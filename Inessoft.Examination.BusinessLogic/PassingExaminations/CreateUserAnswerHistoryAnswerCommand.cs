﻿using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class CreateUserAnswerHistoryAnswerCommand : BaseCommand
    {
        public Result<List<Guid>> Execute(Guid userAnswerHistoryId, List<AnswerDto> answersDto)
        {
            if (answersDto.Any() == false)
            {
                return Result<List<Guid>>.Ok(new List<Guid>());
            }

            var userAnswerHistoryAnswerRepository = Uow.GetRepository<UserAnswerHistoryAnswer>();

            var addedUserAnswerHistoryAnswerIds = new List<Guid>();
            foreach (var answerDto in answersDto)
            {
                var userAnswerHistoryAnswer = new UserAnswerHistoryAnswer
                {
                    AnswerId = answerDto.Id,
                    UserAnswerHistoryId = userAnswerHistoryId
                };

                userAnswerHistoryAnswerRepository.Create(userAnswerHistoryAnswer);
                addedUserAnswerHistoryAnswerIds.Add(userAnswerHistoryAnswer.Id);
            }

            Uow.SaveChanges();


            return Result<List<Guid>>.Ok(addedUserAnswerHistoryAnswerIds.ToList());
        }
    }
}
