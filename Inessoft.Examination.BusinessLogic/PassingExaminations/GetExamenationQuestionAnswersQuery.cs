﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class GetExamenationQuestionAnswersQuery : BaseQuery
    {
        public Result<List<AnswerDto>> Execute(Guid examId, Guid questionId)
        {
            try
            {
                var answersRepository = Uow.GetRepository<Answer>();
                var questionAnswers = answersRepository
                    .AsQueryable()
                    .Include(a => a.AnswerTranslates)
                    .Where(a => a.QuestionId == questionId && a.IsDeleted == false)
                    .AsEnumerable()
                    .Select(AnswerDto.MapForExamenPassing)
                    .ToList();


                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository
                                                .AsQueryable()
                                                .FirstOrDefault(r => r.ExaminationId == examId && r.UserId == currentUserId);

                //ответы, котоыре уже отмечены пользователем
                var currentUserAnswerHistoryAnswers = Uow.GetRepository<UserAnswerHistory>()
                                                .AsQueryable()
                                                .Where(r => r.ExamineeUsergroupId == examinationUserGroup.Id
                                                            && r.ExaminationId == examId
                                                            && r.QuestionId == questionId)
                                                .Include(r => r.UserAnswerHistoryAnswers)
                                                .Select(r=> r.UserAnswerHistoryAnswers)                                                
                                                .FirstOrDefault();

                foreach (var questionAnswer in questionAnswers)
                {
                    var answeredQuestion = currentUserAnswerHistoryAnswers.FirstOrDefault(r => r.AnswerId == questionAnswer.Id);
                    if (answeredQuestion!=null)
                    {
                        questionAnswer.IsRight = true;
                    }
                }

                return Result<List<AnswerDto>>.Ok(questionAnswers);
            }
            catch (Exception exception)
            {
                return Result<List<AnswerDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}