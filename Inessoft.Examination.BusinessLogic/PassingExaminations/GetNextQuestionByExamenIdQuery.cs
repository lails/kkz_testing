﻿using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class GetNextQuestionByExamenIdQuery : BaseQuery
    {
        public Result<QuestionDto> Execute(Guid examId, int questionNumber)
        {
            var examRepository = Uow.GetRepository<Domain.Testing.Examination>();
            var examCategories = examRepository.AsQueryable()
                .Where(r => r.Id == examId)
                .Include(r => r.ExaminationCategories)
                .FirstOrDefault();

            var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

            var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
            var examinationUserGroup = examinationUserGroupRepository
                                            .AsQueryable()
                                            .FirstOrDefault(r => r.ExaminationId == examId 
                                                                && r.UserId == currentUserId
                                                                && r.IsComputerTestFinished == false);
            if (examinationUserGroup == null)
            {
                return Result<QuestionDto>.Fail(null, $"Can't get next question");
            }
            var userAnswerHistory = Uow.GetRepository<UserAnswerHistory>();

            var questionFromHistory = Uow.GetRepository<UserAnswerHistory>()
                                            .AsQueryable()
                                            .Where(r => r.ExamineeUsergroupId == examinationUserGroup.Id 
                                                        && r.ExaminationId == examId
                                                        && r.ExaminationQuestionNumber == questionNumber)
                                            .Select(r=>r.Question)
                                            .Include(r => r.QuestionTranslates);

            if(questionFromHistory.Any())
            {
                var nextExistsQuestion = questionFromHistory
                                            .AsEnumerable()
                                            .Select(QuestionDto.Map)
                                            .FirstOrDefault();
                
                return Result<QuestionDto>.Ok(nextExistsQuestion);
            }

            var answeredQuestions = userAnswerHistory.AsQueryable()
                                            .Where(r => r.ExamineeUsergroupId == examinationUserGroup.Id)
                                            .Include(r => r.Question)
                                            .Where(r => r.Question != null)
                                            .Select(r => new { r.Question.CategoryId, QuestionId = r.Question.Id }).ToList();

            var categoryQuestionCounts = answeredQuestions.GroupBy(r => r.CategoryId);

            foreach (var examinationCategory in examCategories.ExaminationCategories)
            {
                var categoryId = examinationCategory.CategoryId;

                var questoinsInCategory = categoryQuestionCounts.Where(r => r.Key == categoryId).Count();
                if (examinationCategory.CategotyQuestionCount > questoinsInCategory)
                {
                    var answeredQuestionIds = answeredQuestions.Where(r => r.CategoryId == categoryId).Select(r => r.QuestionId).ToList();

                    var questionRepository = Uow.GetRepository<Question>();
                    var nextQuestion = questionRepository.AsQueryable()
                        .Include(q => q.QuestionTranslates)
                        .Where(r => r.CategoryId == categoryId
                                            && r.IsDeleted == false
                                            && answeredQuestionIds.Contains(r.Id) == false)
                        .AsEnumerable()
                        .Select(QuestionDto.Map)
                        .OrderBy(r => Guid.NewGuid())
                        .FirstOrDefault();

                    if (nextQuestion == null)
                    {

                        return Result<QuestionDto>.Fail(null, $"Can't get next question");
                    }

                    return Result<QuestionDto>.Ok(nextQuestion);
                }
            }

            return Result<QuestionDto>.Fail(null, $"Can't get next question");
        }
    }
}
