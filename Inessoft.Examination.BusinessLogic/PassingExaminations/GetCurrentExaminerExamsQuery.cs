﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Examinations.Dtos;
using Inessoft.Examination.Di;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Examinations
{
    public class GetCurrentExaminerExamsQuery : BaseQuery
    {
        public Result<List<ExaminationDto>> Execute()
        {
            try
            {
                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;
                var currentDate = ExaminationAmbientContext.Current.DateTimeProvider.Now;

                var examsRepository = Uow.GetRepository<Domain.Testing.Examination>();

                var currentExaminerExams = examsRepository.AsQueryable()
                    .Where(r => r.EndDate > DateTime.Now && r.StartDate <= DateTime.Now && r.IsDeleted == false)
                    .Include(r => r.ExaminationUserGroups)
                    /*e => e.StartDate <= currentDate && currentDate <= e.EndDate && e.IsDeleted == false*/
                    .Where(r => r.ExaminationUserGroups != null
                                && r.ExaminationUserGroups.Any()
                                && r.ExaminationUserGroups.All(u => u.UserId == currentUserId
                                                                 && u.IsDeleted == false
                                                                 && u.IsComputerTestFinished == false
                                                                 && u.IsFullTestPassed == false
                                                                 && u.ExaminationDateComplite > currentDate))
                    .Include(r => r.ExamineTranslates)
                    .Select(ExaminationDto.Map)
                    .ToList();

                return Result<List<ExaminationDto>>.Ok(currentExaminerExams);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}