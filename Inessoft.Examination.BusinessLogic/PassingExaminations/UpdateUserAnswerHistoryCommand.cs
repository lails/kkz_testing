﻿using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class UpdateUserAnswerHistoryCommand : BaseCommand
    {
        public Result<Guid> Execute(QuestionDto questionDto, Guid examId)
        {
            try
            {
                var userAnswerHistoryRepository = Uow.GetRepository<UserAnswerHistory>();

                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationUserGroupRepository = Uow.GetRepository<ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository
                                                .AsQueryable()
                                                .FirstOrDefault(r => r.ExaminationId == examId && r.UserId == currentUserId);

                var currentUserAnswerHistory = Uow.GetRepository<UserAnswerHistory>()
                                                .AsQueryable()
                                                .Where(r => r.ExamineeUsergroupId == examinationUserGroup.Id
                                                            && r.ExaminationId == examId
                                                            && r.QuestionId == questionDto.Id)
                                                //.Include(r => r.Question)
                                                .FirstOrDefault();

                var answersRepository = Uow.GetRepository<Answer>();
                var questionAnswers = answersRepository
                    .AsQueryable()
                    .Include(a => a.AnswerTranslates)
                    .Where(a => a.QuestionId == questionDto.Id && a.IsDeleted == false)
                    .AsEnumerable()
                    .Select(AnswerDto.Map)
                    .ToList();

                currentUserAnswerHistory.QuestionText = questionDto.QuestionText;

                if (questionDto.Answers.Any() == false)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Ответы не указаны");
                }

                currentUserAnswerHistory.AnswerText = string.Join(";", questionDto.Answers.Where(r => r.IsRight == true).Select(r => r.AnswerText));
                currentUserAnswerHistory.RightAnswerText = string.Join(";", questionAnswers.Where(r => r.IsRight == true).Select(r => r.AnswerText));

                var isRightAnswer = false;

                #region IsRightAnswer Logic

                var userAnswerCheckedIds = questionDto.Answers.Where(r => r.IsRight == true).Select(r => r.Id);
                var rightAnswerIds = questionAnswers.Where(r => r.IsRight == true).Select(r => r.Id);

                if (questionDto.IsMultipleAnswer == true)
                {

                    var exceptRightAnswerIds = userAnswerCheckedIds.Except(rightAnswerIds).ToList();
                    var exceptUserAnswerIds = rightAnswerIds.Except(userAnswerCheckedIds).ToList();


                    isRightAnswer = exceptRightAnswerIds.Any() == false && exceptUserAnswerIds.Any() == false;
                }
                else if (questionDto.IsMultipleAnswer == false)
                {
                    isRightAnswer = userAnswerCheckedIds.FirstOrDefault() == rightAnswerIds.FirstOrDefault();
                }

                #endregion

                currentUserAnswerHistory.IsRightAnswer = isRightAnswer;

                if (currentUserAnswerHistory.IsRightAnswer)
                {
                    currentUserAnswerHistory.AnswerEvaluation = 1;
                }
                else
                {
                    currentUserAnswerHistory.AnswerEvaluation = 0;
                }

                currentUserAnswerHistory.IsQuestionAnswered = true;

                Uow.SaveChanges();

                return Result<Guid>.Ok(currentUserAnswerHistory.Id);
            }
            catch (Exception exception)
            {

                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}
