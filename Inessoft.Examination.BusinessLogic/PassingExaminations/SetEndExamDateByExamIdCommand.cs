﻿using Inessoft.Examination.Di;
using NetCoreCQRS.Commands;
using NetCoreDomain;
using System;
using System.Linq;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations
{
    public class SetEndExamDateByExamIdCommand : BaseCommand
    {
        public Result<Guid> Execute(Guid examId)
        {
            try
            {

                var examinationRepository = Uow.GetRepository<Domain.Testing.Examination>();
                var examination = examinationRepository.AsQueryable().FirstOrDefault(r => r.Id == examId);

                if (examination == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Exam with id {examId} not found");
                }

                var currentUserId = ExaminationAmbientContext.Current.AuthenticationService.Identity.UserId;

                var examinationUserGroupRepository = Uow.GetRepository<Domain.Testing.ExaminationUserGroup>();
                var examinationUserGroup = examinationUserGroupRepository.AsQueryable()
                                                                        .FirstOrDefault(r=>r.ExaminationId == examId
                                                                                        && r.UserId == currentUserId
                                                                                        && r.IsDeleted == false
                                                                                        && r.IsComputerTestFinished == false
                                                                                        && r.ExaminationDateStart.HasValue == false
                                                                                        && r.ExaminationDateEnd.HasValue == false);

                if (examinationUserGroup == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"ExaminationuserGroup with id {examId} not found");
                }

                examinationUserGroup.ExaminationDateEnd = DateTime.Now;
                Uow.SaveChanges();

                return Result<Guid>.Ok(examinationUserGroup.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}