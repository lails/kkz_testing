﻿using System;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations.Dtos
{
    public class UserGroupDto
    {
        public string ExaminationName { get; set; }
        public DateTime ExaminationDateComplite { get; set; }
        public bool IsComputerTestFinished { get; set; }
        public int QuestionCount { get; set; }
    }
}
