﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inessoft.Examination.BusinessLogic.PassingExaminations.Dtos
{
    public class UserGroupResultDto
    {
        public string ExaminerFio { get; set; }
        public string ExamenName { get; set; }
        public string UserGroupExamStatusName { get; set; }
        public int TestQuestionTotalCount { get; set; }
        public decimal TestResultEvaluationSummary { get; set; }
        public int TestResultPoint { get; set; }
    }
}
