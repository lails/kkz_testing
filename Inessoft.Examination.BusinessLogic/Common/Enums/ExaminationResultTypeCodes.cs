﻿namespace Inessoft.Examination.BusinessLogic.Common.Enums
{
    public static class ExaminationResultTypeCodes
    {
        public static string Passed = "01";
        public static string NotPassed = "02";
        public static string MissingForRespectfulReason = "03";
        public static string MissingForDisrespectfulReason = "04";
    }
}