﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationResultTypes.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationResultTypes
{
    public class GetExaminationResultTypesQuery : BaseQuery
    {
        public Result<List<ExaminationResultTypeDto>> Execute()
        {
            try
            {
                var resultTypeRepository = Uow.GetRepository<ExaminationResultType>();

                var response = resultTypeRepository
                    .AsQueryable()
                    .Include(rt => rt.ExaminationResultTypeTranslates)
                    .AsEnumerable()
                    .Select(ExaminationResultTypeDto.Map)
                    .ToList();

                return Result<List<ExaminationResultTypeDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<List<ExaminationResultTypeDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}