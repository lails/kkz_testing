﻿using System;
using Inessoft.Examination.Domain.Dictionaries;

namespace Inessoft.Examination.BusinessLogic.ExaminationResultTypes.Dtos
{
    public class ExaminationResultTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }


        public static Func<ExaminationResultType, ExaminationResultTypeDto> Map = type => new ExaminationResultTypeDto
        {
            Id = type.Id,
            Code = type.Code,
            Name = type.GetSystemLanguageQuestionTranslate()?.Text
        };
    }
}