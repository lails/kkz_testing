﻿using System;
using System.Linq;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class CheckCategoryHasChildrenQuery : BaseQuery
    {
        public Result<bool?> Execute(Guid parentCategoryId)
        {
            try
            {
                var categoriesRespository = Uow.GetRepository<Category>();
                var categoryHasChildren = categoriesRespository
                    .AsQueryable()
                    .Any(c => c.ParentCategoryId == parentCategoryId && c.IsDeleted == false);

                if (categoryHasChildren)
                {
                    return Result<bool?>.Ok(true);
                }

                return Result<bool?>.Ok(false);
            }
            catch (Exception exception)
            {
                return Result<bool?>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}