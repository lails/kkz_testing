﻿using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using System;
using System.Linq;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class DeleteCategoryByIdCommand : BaseCommand
    {
        public Result<Guid> Execute(Guid categoryId)
        {
            try
            {
                var categoriesRepository = Uow.GetRepository<Category>();
                var category = categoriesRepository
                    .AsQueryable()
                    .FirstOrDefault(c => c.Id == categoryId && c.IsDeleted == false);

                if (category == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Category with id {categoryId} not found");
                }

                category.MarkAsDelete();
                categoriesRepository.Update(category);
                Uow.SaveChanges();
                return Result<Guid>.Ok(categoryId);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}