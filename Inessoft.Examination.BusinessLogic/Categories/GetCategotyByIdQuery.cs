﻿using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Queries;
using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Microsoft.EntityFrameworkCore;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class GetCategotyByIdQuery : BaseQuery
    {
        public Result<CategoryDto> Execute(Guid id)
        {
            var categoryRepository = Uow.GetRepository<Category>();

            var categoryDto = categoryRepository
                .AsQueryable()
                .Include(c => c.CategoryTranslates)
                .Include(c => c.Questions)
                .Where(c => c.Id == id && c.IsDeleted == false)
                .AsEnumerable()
                .Select(CategoryDto.Map)
                .FirstOrDefault();

            if (categoryDto == null)
            {
                return Result<CategoryDto>.Fail(null, $"Category with id {id} not found");
            }

            return Result<CategoryDto>.Ok(categoryDto);
        }
    }
}
