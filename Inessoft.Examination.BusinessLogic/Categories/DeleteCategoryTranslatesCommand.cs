﻿using System;
using System.Linq;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class DeleteCategoryTranslatesCommand : BaseCommand
    {
        public Result Execute(Guid categoryId)
        {
            try
            {
                var categoryTranslatesRepository = Uow.GetRepository<CategoryTranslate>();
                var translates = categoryTranslatesRepository
                    .AsQueryable()
                    .Where(c => c.CategoryId == categoryId && c.IsDeleted == false)
                    .ToList();

                foreach (var translate in translates)
                {
                    translate.MarkAsDeleted();
                    categoryTranslatesRepository.Update(translate);
                }

                Uow.SaveChanges();
                return Result.Ok();
            }
            catch (Exception exception)
            {
                return Result.Fail($"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}