﻿using System;
using System.Linq;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class CheckCategoryHasQuestionsQuery: BaseQuery
    {
        public Result<bool?> Execute(Guid categoryId)
        {
            try
            {
                var questionsRespository = Uow.GetRepository<Question>();
                var questionsHasChildren = questionsRespository
                    .AsQueryable()
                    .Any(c => c.CategoryId == categoryId && c.IsDeleted == false);

                if (questionsHasChildren)
                {
                    return Result<bool?>.Ok(true);
                }

                return Result<bool?>.Ok(false);
            }
            catch (Exception exception)
            {
                return Result<bool?>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}