﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDataAccess.BaseResponses;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class GetCategoriesQuery : BaseQuery
    {
        public Result<PagedListResponse<CategoryDto>> Execute()
        {
            try
            {
                var response = new PagedListResponse<CategoryDto>();
                var categoryRepository = Uow.GetRepository<Category>();
                response.Items = categoryRepository
                    .AsQueryable()
                    .Include(c => c.CategoryTranslates)
                    .Include(c => c.Questions)
                    .Where(c => c.IsDeleted == false)
                    .AsEnumerable()
                    .Select(CategoryDto.Map)
                    .ToArray();
                response.TotalCount = response.Items.Length;
                return Result<PagedListResponse<CategoryDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<PagedListResponse<CategoryDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}