﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.Domain.Testing;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.BusinessLogic.Categories.Dtos
{
    public class CategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int QuestionsCount { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsCanAddQuestions { get; set; }

        public static Func<Category, CategoryDto> Map = category => new CategoryDto
        {
            Id = category.Id,
            ParentId = category.ParentCategoryId,
            Name = category.GetSystemLanguageCategoryTranslate().Text,
            QuestionsCount = category.Questions?.Count(r=>r.IsDeleted == false) ?? 0,
            IsCanAddQuestions = category.IsCanAddQuestions
        };

        public Category CreateCategory => new Category
        {
            CategoryTranslates = new List<CategoryTranslate>
                {
                    new CategoryTranslate
                    {
                        LanguageCode = "ru",
                        Text = Name
                    }
                },


            IsCanAddQuestions = IsCanAddQuestions,
            ParentCategoryId = ParentId,
            IsDeleted = false
        };

        public void UpdateCategory(Category category)
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            category.SetParent(ParentId);

            category.IsCanAddQuestions = IsCanAddQuestions;

            if (category.HasSystemLanguageCategoryTranslate)
            {
                category
                    .GetSystemLanguageCategoryTranslate()
                    .SetTranslate(Name);

                return;
            }

            if (category.CategoryTranslates == null)
            {
                category.CategoryTranslates = new List<CategoryTranslate>();
            }

            var categoryTranslateText = new CategoryTranslate
            {
                LanguageCode = systemLanguageCode,
                Text = Name
            };

            category.CategoryTranslates.Add(categoryTranslateText);
        }
    }
}