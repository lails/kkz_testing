﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class UpdateCategoryCommand : BaseCommand
    {
        public Result<Guid> Execute(CategoryDto categoryDto)
        {
            try
            {
                var categoriesRepository = Uow.GetRepository<Category>();
                var category = categoriesRepository
                    .AsQueryable()
                    .Include(c => c.CategoryTranslates)
                    .FirstOrDefault(c => c.Id == categoryDto.Id && c.IsDeleted == false);

                if (category == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Category with id {categoryDto.Id} not found");
                }

                categoryDto.UpdateCategory(category);
                categoriesRepository.Update(category);
                Uow.SaveChanges();

                return Result<Guid>.Ok(category.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}