﻿using System;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Categories
{
    public class CreateCategoryCommand : BaseCommand
    {
        public Result<Guid> Execute(CategoryDto categoryDto)
        {
            try
            {
                var categoriesRepository = Uow.GetRepository<Category>();
                var category = categoryDto.CreateCategory;
                categoriesRepository.Create(category);
                Uow.SaveChanges();
                return Result<Guid>.Ok(category.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}