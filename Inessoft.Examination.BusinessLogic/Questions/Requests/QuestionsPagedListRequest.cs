﻿using System;
using NetCoreDataAccess.BaseRequests;

namespace Inessoft.Examination.BusinessLogic.Questions.Requests
{
    public class QuestionsPagedListRequest : PagedListRequest
    {
        public Guid? CategoryId { get; set; }
        public string ArticleNumber { get; set; }
        public string CategoryName { get; set; }
        public string QuestionText { get; set; }

        public bool HasCategoryId => CategoryId.HasValue;
        public bool HasCategoryName => string.IsNullOrEmpty(CategoryName) == false;
        public bool HasArticleNumber => string.IsNullOrEmpty(ArticleNumber) == false;
        public bool HasQuestionText => string.IsNullOrEmpty(QuestionText) == false;
    }
}