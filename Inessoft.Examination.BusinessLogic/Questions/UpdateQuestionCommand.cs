﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Categories.Dtos;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Questions
{
    public class UpdateQuestionCommand : BaseCommand
    {
        public Result<Guid> Execute(QuestionDto questionDto)
        {
            try
            {
                var questionsRepository = Uow.GetRepository<Question>();
                var question = questionsRepository
                    .AsQueryable()
                    .Include(c => c.QuestionTranslates)
                    .FirstOrDefault(c => c.Id == questionDto.Id && c.IsDeleted == false);

                if (question == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Question with id {questionDto.Id} not found");
                }

                questionDto.UpdateQuestion(question);
                questionsRepository.Update(question);
                Uow.SaveChanges();

                return Result<Guid>.Ok(question.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}