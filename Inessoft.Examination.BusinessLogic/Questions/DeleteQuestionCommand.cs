﻿using System;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;

namespace Inessoft.Examination.BusinessLogic.Questions
{
    public class DeleteQuestionCommand : BaseCommand
    {
        public void Execute(Guid questionId)
        {
            var questionsRepository = Uow.GetRepository<Question>();
            var question = questionsRepository.GetById(questionId);

            if (question == null)
            {
                return;
            }

            question.MarkAsDeleted();
            questionsRepository.Update(question);

            Uow.SaveChanges();
        }
    }
}