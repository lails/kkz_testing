﻿using System;
using Inessoft.Examination.Domain.Testing;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Questions
{
    public class CreateQuestionCommand : BaseCommand
    {
        public Result<Guid> Execute(Question question)
        {
            try
            {
                var questionRepository = Uow.GetRepository<Question>();

                questionRepository.Create(question);
                Uow.SaveChanges();
                return Result<Guid>.Ok(question.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}