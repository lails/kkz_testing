﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.BusinessLogic.Questions.Requests;
using Inessoft.Examination.Di;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDataAccess.BaseResponses;
using NetCoreDataAccess.Externsions;
using NetCoreDI;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Questions
{
    public class GetQuestionsQuery : BaseQuery
    {
        public Result<PagedListResponse<QuestionDto>> Execute(QuestionsPagedListRequest request)
        {
            try
            {
                var response = new PagedListResponse<QuestionDto>();
                var questionsRespository = Uow.GetRepository<Question>();
                var query = questionsRespository
                    .AsQueryable()
                    .Include(q => q.QuestionTranslates)
                    .Include(q => q.Category)
                    .ThenInclude(c => c.CategoryTranslates)
                    .Where(q => q.IsDeleted == false);

                query = ProcessRequest(query, request);

                response.Items = query
                    .ApplyPagedListRequest(request, response)
                    .AsEnumerable()
                    .Select(QuestionDto.Map)
                    .ToArray();

                return Result<PagedListResponse<QuestionDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<PagedListResponse<QuestionDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }

        private static IQueryable<Question> ProcessRequest(IQueryable<Question> query, QuestionsPagedListRequest request)
        {
            if (request.HasCategoryId)
            {
                query = query.Where(r => r.CategoryId == request.CategoryId);
            }
            if (request.HasCategoryName)
            {
                query = query.Where(r => r.Category != null && r.Category.CategoryTranslates != null && r.Category.CategoryTranslates.Any(t => t.Text.Contains(request.CategoryName)));
            }

            if (request.HasArticleNumber)
            {
                query = query.Where(r => r.ArticleNumber == request.ArticleNumber);
            }

            if (request.HasQuestionText)
            {
                var lang = AmbientContext.Current.Localization.CurrentLanguageCode;
                query = query.Where(r => r.QuestionTranslates.Any(t=>t.LanguageCode == lang && t.Text.ToLower().Contains(request.QuestionText.ToLower())) );
            }

            return query;
        }
    }
}