﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Answers.Dtos;
using Inessoft.Examination.Domain.Testing;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.BusinessLogic.Questions.Dtos
{
    public class QuestionDto
    {
        public Guid Id { get; set; }
        public string QuestionText { get; set; }
        public bool IsMultipleAnswer { get; set; }
        public string ArticleNumber { get; set; }
        public string CategoryName { get; set; }
        public Guid CategoryId { get; set; }

        public List<AnswerDto> Answers { get; set; }

        public static Func<Question, QuestionDto> Map = question => new QuestionDto
        {
            Id = question.Id,
            QuestionText = question.GetSystemLanguageQuestionTranslate()?.Text,
            CategoryId = question.CategoryId,
            ArticleNumber = question.ArticleNumber,
            IsMultipleAnswer = question.IsMultipleAnswer,
            CategoryName = question.Category?.GetSystemLanguageCategoryTranslate()?.Text
        };

        public Question CreateQuestion => new Question
        {
            CategoryId = CategoryId,
            ArticleNumber = ArticleNumber,
            IsDeleted = false,
            IsMultipleAnswer = IsMultipleAnswer,
            QuestionTranslates = new List<QuestionTranslate>
            {
                new QuestionTranslate
                {
                    LanguageCode = "ru",
                    Text = QuestionText
                }
            }
        };

        public void UpdateQuestion(Question question)
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;

            question.CategoryId = CategoryId;
            question.ArticleNumber = ArticleNumber;
            question.IsMultipleAnswer = IsMultipleAnswer;

            if (question.HasSystemLanguageQuestionTranslate)
            {
                question
                    .GetSystemLanguageQuestionTranslate()
                    .SetTranslate(QuestionText);

                return;
            }

            if (question.QuestionTranslates == null)
            {
                question.QuestionTranslates = new List<QuestionTranslate>();
            }

            var questionTranslatedText = new QuestionTranslate
            {
                LanguageCode = systemLanguageCode,
                Text = QuestionText
            };

            question.QuestionTranslates.Add(questionTranslatedText);
        }
    }
}