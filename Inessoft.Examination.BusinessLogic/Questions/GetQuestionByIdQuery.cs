﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.Questions.Dtos;
using Inessoft.Examination.Domain.Testing;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.Questions
{
    public class GetQuestionByIdQuery : BaseQuery
    {
        public Result<QuestionDto> Execute(Guid questionId)
        {
            try
            {
                var questionsRespository = Uow.GetRepository<Question>();
                var questionDto = questionsRespository
                    .AsQueryable()
                    .Include(q => q.QuestionTranslates)
                    .Include(q => q.Category)
                    .ThenInclude(c => c.CategoryTranslates)
                    .Where(q => q.Id == questionId && q.IsDeleted == false)
                    .AsEnumerable()
                    .Select(QuestionDto.Map)
                    .FirstOrDefault();

                if (questionDto == null)
                {
                    return Result<QuestionDto>.Fail(null, $"Question with id {questionId} not found");
                }

                return Result<QuestionDto>.Ok(questionDto);
            }
            catch (Exception exception)
            {
                return Result<QuestionDto>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}