﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDataAccess.BaseRequests;
using NetCoreDataAccess.BaseResponses;
using NetCoreDataAccess.Externsions;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationSettings
{
    public class GetExaminationSettingsQuery : BaseQuery
    {
        public Result<PagedListResponse<ExaminationSettingDto>> Execute(PagedListRequest request)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<ExaminationSetting>();
                var response = new PagedListResponse<ExaminationSettingDto>();
                
                var query = examinationRepository
                    .AsQueryable()
                    .Where(e => e.IsDeleted == false);

                if (request == null)
                {
                    response.Items = query
                                .AsEnumerable()
                                .Select(ExaminationSettingDto.Map)
                                .ToArray();
                }
                else
                {
                    response.Items = query
                                .ApplyPagedListRequest(request, response)
                                .AsEnumerable()
                                .Select(ExaminationSettingDto.Map)
                                .ToArray();
                }

                return Result<PagedListResponse<ExaminationSettingDto>>.Ok(response);
            }
            catch (Exception exception)
            {
                return Result<PagedListResponse<ExaminationSettingDto>>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}