﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Inessoft.Examination.BusinessLogic.ExaminationCategories.Dtos;
using Inessoft.Examination.BusinessLogic.ExaminationChairmans.Dtos;
using Inessoft.Examination.Domain.Dictionaries;

namespace Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos
{
    public class ExaminationSettingDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public int Duration { get; set; }
        public int QuestionCount { get; set; }
        public int RequiredCountOfCorrectAnswers { get; set; }

        public int PointFive { get; set; }
        public int PointFour { get; set; }
        public int PointThree { get; set; }

        public Guid ChairmanId { get; set; }
        public Guid ExamenSettingParentCategoryId { get; set; }
        public ExaminationChairmanDto Chairman { get; set; }
        public ExaminationCategoryDto ExamenSettingParentCategory { get; set; }

        public List<ExaminationCategoryDto> ExaminationCategories { get; set; }

        public static Func<ExaminationSetting, ExaminationSettingDto> Map = examinationSetting => new ExaminationSettingDto
        {
            Id = examinationSetting.Id,
            Name = examinationSetting.Name,
            Chairman =  ExaminationChairmanDto.MapFromSettings(examinationSetting),
            Duration = examinationSetting.Duration,
            QuestionCount = examinationSetting.QuestionCount,
            RequiredCountOfCorrectAnswers = examinationSetting.RequiredCountOfCorrectAnswers,
            PointFive = examinationSetting.PointFive,
            PointFour = examinationSetting.PointFour,
            PointThree = examinationSetting.PointThree
        };

        [IgnoreDataMember]
        public ExaminationSetting CreateExaminationSetting => new ExaminationSetting
        {
            Name = Name,
            ChairmanId = ChairmanId,
            ChairmanName = Chairman.FIO,
            ChairmanInn = Chairman.INN,
            Duration = Duration,
            IsDeleted = false,
            QuestionCount = QuestionCount,
            RequiredCountOfCorrectAnswers = RequiredCountOfCorrectAnswers,
            PointFive = PointFive,
            PointFour = PointFour,
            PointThree = PointThree,
            ExaminationSettingCategories = ExaminationCategories.Select(c => c.ToExaminationSettingCategory).ToList()
        };

        public void UpdateCategorySetting(ExaminationSetting examinationSetting)
        {
            examinationSetting.Name = Name;
            examinationSetting.ChairmanId = Chairman.Id;
            examinationSetting.ChairmanName = Chairman.FIO;
            examinationSetting.ChairmanInn = Chairman.INN;
            examinationSetting.Duration = Duration;
            examinationSetting.IsDeleted = false;
            examinationSetting.QuestionCount = QuestionCount;
            examinationSetting.RequiredCountOfCorrectAnswers = RequiredCountOfCorrectAnswers;
            examinationSetting.PointFive = PointFive;
            examinationSetting.PointFour = PointFour;
            examinationSetting.PointThree = PointThree;           
        }        
    }
}