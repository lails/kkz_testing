﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationSettings
{
    public class UpdateExaminationSettingCommand : BaseCommand
    {
        public Result<Guid> Execute(ExaminationSettingDto examinationSettingDto)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<ExaminationSetting>();
                var examination = examinationRepository
                    .AsQueryable()
                    .FirstOrDefault(c => c.Id == examinationSettingDto.Id && c.IsDeleted == false);

                if (examination == null)
                {
                    return Result<Guid>.Fail(Guid.Empty, $"Exam with id {examinationSettingDto.Id} not found");
                }

                examinationSettingDto.UpdateCategorySetting(examination);
                examinationRepository.Update(examination);
                Uow.SaveChanges();

                return Result<Guid>.Ok(examination.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}