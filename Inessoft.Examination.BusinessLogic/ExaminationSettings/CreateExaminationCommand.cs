﻿using System;
using Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using NetCoreCQRS.Commands;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationSettings
{
    public class CreateExaminationSettingCommand : BaseCommand
    {
        public Result<Guid> Execute(ExaminationSettingDto categoryDto)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<ExaminationSetting>();
                var examination = categoryDto.CreateExaminationSetting;
                examinationRepository.Create(examination);
                Uow.SaveChanges();
                return Result<Guid>.Ok(examination.Id);
            }
            catch (Exception exception)
            {
                return Result<Guid>.Fail(Guid.Empty, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}