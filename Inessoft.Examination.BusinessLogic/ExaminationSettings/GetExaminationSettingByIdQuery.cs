﻿using System;
using System.Linq;
using Inessoft.Examination.BusinessLogic.ExaminationSettings.Dtos;
using Inessoft.Examination.Domain.Dictionaries;
using Microsoft.EntityFrameworkCore;
using NetCoreCQRS.Queries;
using NetCoreDomain;

namespace Inessoft.Examination.BusinessLogic.ExaminationSettings
{
    public class GetExaminationSettingByIdQuery : BaseQuery
    {
        public Result<ExaminationSettingDto> Execute(Guid id)
        {
            try
            {
                var examinationRepository = Uow.GetRepository<ExaminationSetting>();
                var examinationDto = examinationRepository
                    .AsQueryable()
                    .Where(e => e.Id == id && e.IsDeleted == false)
                    .AsEnumerable()
                    .Select(ExaminationSettingDto.Map)
                    .FirstOrDefault();

                if (examinationDto == null)
                {
                    return Result<ExaminationSettingDto>.Fail(null, $"Exam with id {id} not found");
                }

                return Result<ExaminationSettingDto>.Ok(examinationDto);
            }
            catch (Exception exception)
            {
                return Result<ExaminationSettingDto>.Fail(null, $"{exception.Message}, {exception.StackTrace}");
            }
        }
    }
}