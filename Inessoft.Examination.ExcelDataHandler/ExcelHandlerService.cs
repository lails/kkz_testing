﻿using Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler;
using Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.UserDataHandler;

namespace Inessoft.Examination.ExcelDataHandler
{
    public class ExcelHandlerService : IExcelDataHandlersService
    {
        public UserDataHandlers UserDataHandlers => new UserDataHandlers();
        public QuestionDataHandlers QuestionDataHandlers => new QuestionDataHandlers();
    }
}
