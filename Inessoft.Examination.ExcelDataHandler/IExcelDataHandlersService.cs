﻿using Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler;
using Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.UserDataHandler;

namespace Inessoft.Examination.ExcelDataHandler
{
    public interface IExcelDataHandlersService
    {
        UserDataHandlers UserDataHandlers { get; }
        QuestionDataHandlers QuestionDataHandlers { get; }
    }
}
