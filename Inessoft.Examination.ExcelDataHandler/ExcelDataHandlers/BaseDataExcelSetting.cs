﻿namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers
{
    public abstract class BaseDataExcelSetting
    {
        private int _indentCount { get; set; }

        public virtual int IndentCount
        {
            get
            {
                return _indentCount <= 0 ? 0 : _indentCount - 1;
            }
            set
            { _indentCount = value; }
        }

        /// <summary>
        /// отступ от левого края
        /// </summary>
        public abstract int LeftTabCount { get; }
        /// <summary>
        /// Всего строк для переходу к новому объекту
        /// </summary>
        public abstract int FullRowsCount { get; }
    }
}
