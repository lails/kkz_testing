﻿namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler
{
    public class QuestionDataExcelSetting : BaseDataExcelSetting
    {
        public override int IndentCount { get => base.IndentCount; set => base.IndentCount = 1; }
        public int IdRowNumber => IndentCount + 1;
        public int QuestionTextRowNumber => IndentCount + 2;
        public int ArticleNumberRowNumber => IndentCount + 3;


        public int RightAnswerColumnNumber => LeftTabCount + 1;
        public override int FullRowsCount => throw new System.NotImplementedException();

        public override int LeftTabCount => 1;
    }
}
