﻿using System;
using System.Collections.Generic;
namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler.Dtos
{
    public class QuestionDto
    {
        public Guid Id { get; set; }
        public string QuestionText { get; set; }
        public bool IsMultipleAnswer { get; set; }
        public string ArticleNumber { get; set; }
        public string CategoryName { get; set; }
        public List<AnswerDto> Answers { get; set; }
    }
}