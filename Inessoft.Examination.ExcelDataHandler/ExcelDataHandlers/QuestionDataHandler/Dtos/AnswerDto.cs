﻿using System;

namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler.Dtos
{
    public class AnswerDto
    {
        public Guid Id { get; set; }
        public bool IsRight { get; set; }
        public string AnswerText { get; set; }
    }
}