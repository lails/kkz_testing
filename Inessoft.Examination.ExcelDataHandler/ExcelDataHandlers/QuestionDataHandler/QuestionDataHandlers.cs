﻿using Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler.Dtos;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.QuestionDataHandler
{
    public class QuestionDataHandlers : BaseExcelDataHandler<QuestionDataExcelSetting>
    {
        private int _currentQuestionNumber = 0;
        private int _questionStartNumber = 0;

        public List<QuestionDto> FormQuestion(FileStream file)
        {
            file.Position = 0;
            XSSFWorkbook hssfwb = new XSSFWorkbook(file);

            ISheet sheet = hssfwb.GetSheetAt(0);
            var questionDtos = new List<QuestionDto>();

            QuestionDto questionDto = null;

            for (int i = DataExcelSetting.IndentCount; i <= sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);
                var rowValue = row.GetCell(0)?.StringCellValue;
                var isNextQuestion = string.IsNullOrEmpty(rowValue) == false && int.TryParse(rowValue, out int questionNumber);
                if (isNextQuestion)
                {
                    questionDto = new QuestionDto
                    {
                        Answers = new List<AnswerDto>()
                    };
                    questionDtos.Add(questionDto);
                    _currentQuestionNumber += 1;
                    _questionStartNumber = i;
                }

                if (row != null && questionDto != null)
                {
                    SetQuestionData(row, i, ref questionDto);
                    questionDto.IsMultipleAnswer = questionDto.Answers.Count(r => r.IsRight == true) > 1;
                }
            }

            return questionDtos;
        }

        #region Set User Data

        private void SetQuestionData(IRow row, int index, ref QuestionDto QuestionDto)
        {
            SetId(row, index, ref QuestionDto);
            SetQuestionText(row, index, ref QuestionDto);
            SetArticleNumber(row, index, ref QuestionDto);
            AddAnswer(row, index, ref QuestionDto);
        }

        private void SetId(IRow row, int index, ref QuestionDto QuestionDto)
        {
            if (index == GetCurrentPosition(DataExcelSetting.IdRowNumber))
            {
                if (Guid.TryParse(GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount), out Guid questionId) == true)
                {
                    QuestionDto.Id = questionId;
                }
            }
        }
        private void SetQuestionText(IRow row, int index, ref QuestionDto QuestionDto)
        {
            if (index == GetCurrentPosition(DataExcelSetting.QuestionTextRowNumber))
            {
                QuestionDto.QuestionText = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void SetArticleNumber(IRow row, int index, ref QuestionDto QuestionDto)
        {
            if (index == GetCurrentPosition(DataExcelSetting.ArticleNumberRowNumber))
            {
                QuestionDto.ArticleNumber = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void AddAnswer(IRow row, int index, ref QuestionDto QuestionDto)
        {
            if (index > GetCurrentPosition(DataExcelSetting.ArticleNumberRowNumber))
            {
                if(string.IsNullOrEmpty(GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount)) == false)
                {
                    var isRightAnswer = row.GetCell(DataExcelSetting.RightAnswerColumnNumber)?.StringCellValue == "1";
                    var answer = new AnswerDto
                    {
                        AnswerText = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount),
                        IsRight = isRightAnswer
                    };

                    QuestionDto.Answers.Add(answer);
                }
            }
        }

        #endregion

        private int GetCurrentPosition(int rowNumber)
        {
            return _questionStartNumber - 1 + DataExcelSetting.IndentCount + rowNumber;
        }

        private string GetCellValueCellIndex(IRow row, int cellIndex = 1)
        {
            return row.GetCell(cellIndex)?.StringCellValue;
        }
    }
}
