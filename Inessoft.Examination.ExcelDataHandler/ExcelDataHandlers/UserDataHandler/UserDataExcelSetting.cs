﻿namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.UserDataHandler
{
    public sealed class UserDataExcelSetting : BaseDataExcelSetting
    {
        public override int IndentCount { get => base.IndentCount; set => base.IndentCount = 1; }
        public int IdRowNumber => IndentCount + 1;
        public int IinRowNumber => IndentCount + 2;

        public int PasswordRowNumber => IinRowNumber;
        /// <summary>
        /// Логни для входа
        /// </summary>
        public int UserNameRowNumber => IinRowNumber;

        /// <summary>
        /// Фамилия
        /// </summary>
        public int MiddleNameRowNumber => IndentCount + 3;
        public int FirstNameRowNumber => IndentCount + 4;
        /// <summary>
        /// Отчество
        /// </summary>
        public int SecondNameRowNumber => IndentCount + 5;

        public override int FullRowsCount => SecondNameRowNumber + 1;

        public override int LeftTabCount => 1;
    }
}
