﻿using NetCoreIdentityHttpClient.Dtos;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers.UserDataHandler
{
    public class UserDataHandlers : BaseExcelDataHandler<UserDataExcelSetting>
    {
        private int _currentUserNumber = 0;

        public List<UserDto> FormUserDto(FileStream file)
        {
            file.Position = 0;
            XSSFWorkbook hssfwb = new XSSFWorkbook(file);

            ISheet sheet = hssfwb.GetSheetAt(0);
            var userDtos = new List<UserDto>();

            UserDto userDto = null;

            for (int i = DataExcelSetting.IndentCount; i <= sheet.LastRowNum; i++)
            {
                if ((DataExcelSetting.FullRowsCount * _currentUserNumber) == i)
                {
                    userDto = new UserDto
                    {
                        IsActive = true,
                        Position = ""
                    };
                    //TODO: надо добавить код для ролей либо все, кто не имеют роль - являются кандидатами
                    userDtos.Add(userDto);
                    _currentUserNumber += 1;
                }

                var row = sheet.GetRow(i);

                if (row != null)
                {
                    SetUserData(row, i, ref userDto);
                }
            }

            return userDtos;
        }

        #region Set User Data

        private void SetUserData(IRow row, int index, ref UserDto userDto)
        {
            SetId(row, index, ref userDto);
            SetIin(row, index, ref userDto);
            SetUserAccount(row, index, ref userDto);
            SetMiddleName(row, index, ref userDto);
            SetFirstName(row, index, ref userDto);
            SetSecondName(row, index, ref userDto);
        }

        private void SetId(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.IdRowNumber + GetCurrenUserStartPosition()))
            {
                if(string.IsNullOrEmpty(GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount)) == false)
                {
                    userDto.Id = Guid.Parse(GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount));
                }
            }
        }

        private void SetIin(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.IinRowNumber + GetCurrenUserStartPosition()))
            {
                userDto.Inn = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void SetMiddleName(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.MiddleNameRowNumber + GetCurrenUserStartPosition()))
            {
                userDto.MiddleName = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void SetUserAccount(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.UserNameRowNumber + GetCurrenUserStartPosition()))
            {
                userDto.Account = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void SetFirstName(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.FirstNameRowNumber + GetCurrenUserStartPosition()))
            {
                userDto.FirstName = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private void SetSecondName(IRow row, int index, ref UserDto userDto)
        {
            if (index == (DataExcelSetting.SecondNameRowNumber + GetCurrenUserStartPosition()))
            {
                userDto.SecondName = GetCellValueCellIndex(row, DataExcelSetting.LeftTabCount);
            }
        }
        private int GetCurrenUserStartPosition()
        {
            return (_currentUserNumber - 1) * DataExcelSetting.FullRowsCount;
        }

        private string GetCellValueCellIndex(IRow row, int cellIndex = 1)
        {
            return row.GetCell(cellIndex).StringCellValue;
        }

        #endregion
    }
}
