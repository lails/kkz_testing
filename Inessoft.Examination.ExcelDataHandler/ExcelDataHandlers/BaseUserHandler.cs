﻿using System;

namespace Inessoft.Examination.ExcelDataHandler.ExcelDataHandlers
{
    public class BaseExcelDataHandler<TDataExcelSetting> : IDisposable
        where TDataExcelSetting : BaseDataExcelSetting, new()
    {
        private TDataExcelSetting _dataExcelSetting { get; set; }
        protected TDataExcelSetting DataExcelSetting => _dataExcelSetting ?? (_dataExcelSetting = new TDataExcelSetting());

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
