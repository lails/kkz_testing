﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.Migrations
{
    public partial class init5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserAnswerHistoryAnswer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnswerId = table.Column<Guid>(nullable: false),
                    UserAnswerHistoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAnswerHistoryAnswer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserAnswerHistoryAnswer_Answers_AnswerId",
                        column: x => x.AnswerId,
                        principalTable: "Answers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserAnswerHistoryAnswer_UserAnswerHistories_UserAnswerHistoryId",
                        column: x => x.UserAnswerHistoryId,
                        principalTable: "UserAnswerHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserAnswerHistoryAnswer_AnswerId",
                table: "UserAnswerHistoryAnswer",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAnswerHistoryAnswer_UserAnswerHistoryId",
                table: "UserAnswerHistoryAnswer",
                column: "UserAnswerHistoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserAnswerHistoryAnswer");
        }
    }
}
