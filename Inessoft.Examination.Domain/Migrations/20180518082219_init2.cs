﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.Migrations
{
    public partial class init2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsComputerTestPassed",
                table: "ExamineeUserGroups");

            migrationBuilder.AddColumn<bool>(
                name: "IsComputerTestFinished",
                table: "ExamineeUserGroups",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsComputerTestFinished",
                table: "ExamineeUserGroups");

            migrationBuilder.AddColumn<bool>(
                name: "IsComputerTestPassed",
                table: "ExamineeUserGroups",
                nullable: true);
        }
    }
}
