﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsCanAddQuestions = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ParentCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExaminationChairmans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FIO = table.Column<string>(nullable: true),
                    IIN = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationChairmans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExaminationResultTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationResultTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryTranslates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryTranslates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryTranslates_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ArticleNumber = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsMultipleAnswer = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Questions_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSettings_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Examinations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ChairmanId = table.Column<Guid>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    PointFive = table.Column<int>(nullable: false),
                    PointFour = table.Column<int>(nullable: false),
                    PointThree = table.Column<int>(nullable: false),
                    QuestionCount = table.Column<int>(nullable: false),
                    RequiredCountOfCorrectAnswers = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Examinations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Examinations_ExaminationChairmans_ChairmanId",
                        column: x => x.ChairmanId,
                        principalTable: "ExaminationChairmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExaminationSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ChairmanId = table.Column<Guid>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PointFive = table.Column<int>(nullable: false),
                    PointFour = table.Column<int>(nullable: false),
                    PointThree = table.Column<int>(nullable: false),
                    QuestionCount = table.Column<int>(nullable: false),
                    RequiredCountOfCorrectAnswers = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExaminationSettings_ExaminationChairmans_ChairmanId",
                        column: x => x.ChairmanId,
                        principalTable: "ExaminationChairmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsRight = table.Column<bool>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionTranslates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: true),
                    QuestionId = table.Column<Guid>(nullable: false),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionTranslates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionTranslates_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExaminationCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    CategotyQuestionCount = table.Column<int>(nullable: false),
                    ExaminationId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExaminationCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExaminationCategories_Examinations_ExaminationId",
                        column: x => x.ExaminationId,
                        principalTable: "Examinations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExamineeUserGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExaminationDateComplite = table.Column<DateTime>(nullable: true),
                    ExaminationDateEnd = table.Column<DateTime>(nullable: true),
                    ExaminationDateStart = table.Column<DateTime>(nullable: true),
                    ExaminationId = table.Column<Guid>(nullable: false),
                    ExaminationResultTypeId = table.Column<Guid>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Inn = table.Column<string>(nullable: true),
                    IsComputerTestPassed = table.Column<bool>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsFullTestPassed = table.Column<bool>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    OralExamPoint = table.Column<short>(nullable: false),
                    SecondName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamineeUserGroups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamineeUserGroups_Examinations_ExaminationId",
                        column: x => x.ExaminationId,
                        principalTable: "Examinations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExamineeUserGroups_ExaminationResultTypes_ExaminationResultTypeId",
                        column: x => x.ExaminationResultTypeId,
                        principalTable: "ExaminationResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ExamineTranslates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExaminationId = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamineTranslates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamineTranslates_Examinations_ExaminationId",
                        column: x => x.ExaminationId,
                        principalTable: "Examinations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExaminationSettingCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    CategotyQuestionCount = table.Column<int>(nullable: false),
                    ExaminationSettingId = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationSettingCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExaminationSettingCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExaminationSettingCategories_ExaminationSettings_ExaminationSettingId",
                        column: x => x.ExaminationSettingId,
                        principalTable: "ExaminationSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AnswerTranslates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnswerId = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnswerTranslates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnswerTranslates_Answers_AnswerId",
                        column: x => x.AnswerId,
                        principalTable: "Answers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAnswerHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AnswerEvaluation = table.Column<decimal>(nullable: false),
                    AnswerText = table.Column<string>(nullable: true),
                    ExaminationId = table.Column<Guid>(nullable: false),
                    ExaminationUsergroupId = table.Column<Guid>(nullable: true),
                    ExamineeUsergroupId = table.Column<Guid>(nullable: false),
                    IsRightAnswer = table.Column<bool>(nullable: false),
                    QuestionId = table.Column<Guid>(nullable: false),
                    QuestionText = table.Column<string>(nullable: true),
                    RightAnswerText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAnswerHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserAnswerHistories_Examinations_ExaminationId",
                        column: x => x.ExaminationId,
                        principalTable: "Examinations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserAnswerHistories_ExamineeUserGroups_ExaminationUsergroupId",
                        column: x => x.ExaminationUsergroupId,
                        principalTable: "ExamineeUserGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAnswerHistories_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_AnswerTranslates_AnswerId",
                table: "AnswerTranslates",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryId",
                table: "Categories",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryTranslates_CategoryId",
                table: "CategoryTranslates",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationCategories_CategoryId",
                table: "ExaminationCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationCategories_ExaminationId",
                table: "ExaminationCategories",
                column: "ExaminationId");

            migrationBuilder.CreateIndex(
                name: "IX_Examinations_ChairmanId",
                table: "Examinations",
                column: "ChairmanId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationSettingCategories_CategoryId",
                table: "ExaminationSettingCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationSettingCategories_ExaminationSettingId",
                table: "ExaminationSettingCategories",
                column: "ExaminationSettingId");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationSettings_ChairmanId",
                table: "ExaminationSettings",
                column: "ChairmanId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamineeUserGroups_ExaminationId",
                table: "ExamineeUserGroups",
                column: "ExaminationId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamineeUserGroups_ExaminationResultTypeId",
                table: "ExamineeUserGroups",
                column: "ExaminationResultTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamineTranslates_ExaminationId",
                table: "ExamineTranslates",
                column: "ExaminationId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_CategoryId",
                table: "Questions",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionTranslates_QuestionId",
                table: "QuestionTranslates",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAnswerHistories_ExaminationId",
                table: "UserAnswerHistories",
                column: "ExaminationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAnswerHistories_ExaminationUsergroupId",
                table: "UserAnswerHistories",
                column: "ExaminationUsergroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAnswerHistories_QuestionId",
                table: "UserAnswerHistories",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_CategoryId",
                table: "UserSettings",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnswerTranslates");

            migrationBuilder.DropTable(
                name: "CategoryTranslates");

            migrationBuilder.DropTable(
                name: "ExaminationCategories");

            migrationBuilder.DropTable(
                name: "ExaminationSettingCategories");

            migrationBuilder.DropTable(
                name: "ExamineTranslates");

            migrationBuilder.DropTable(
                name: "QuestionTranslates");

            migrationBuilder.DropTable(
                name: "UserAnswerHistories");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "ExaminationSettings");

            migrationBuilder.DropTable(
                name: "ExamineeUserGroups");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "Examinations");

            migrationBuilder.DropTable(
                name: "ExaminationResultTypes");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "ExaminationChairmans");
        }
    }
}
