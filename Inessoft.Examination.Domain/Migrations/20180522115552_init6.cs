﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.Migrations
{
    public partial class init6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RequiredCountOfCorrectAnswers",
                table: "Examinations",
                newName: "RequiredEvaluationSummary");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RequiredEvaluationSummary",
                table: "Examinations",
                newName: "RequiredCountOfCorrectAnswers");
        }
    }
}
