﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.Migrations
{
    public partial class init4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExaminationQuestionNumber",
                table: "UserAnswerHistories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsQuestionAnswered",
                table: "UserAnswerHistories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExaminationQuestionNumber",
                table: "UserAnswerHistories");

            migrationBuilder.DropColumn(
                name: "IsQuestionAnswered",
                table: "UserAnswerHistories");
        }
    }
}
