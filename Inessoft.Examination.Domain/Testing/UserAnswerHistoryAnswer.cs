﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// Таблица используется при прохождении экзамена, что бы знать ответы, которые уже были отмечены.
    /// </summary>
    [Table("UserAnswerHistoryAnswer")]
    public class UserAnswerHistoryAnswer : BaseEntity
    {
        public Guid AnswerId { get; set; }
        public Answer Answer { get; set; }
        /*

        public Guid QuestionId { get; set; }
        public Question Question { get; set; }
        */
        public Guid UserAnswerHistoryId { get; set; }
        public UserAnswerHistory UserAnswerHistory { get; set; }
    }
}
