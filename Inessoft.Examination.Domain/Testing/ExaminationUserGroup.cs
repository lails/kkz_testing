﻿using Inessoft.Examination.Domain.Dictionaries;
using NetCoreDataAccess.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// Група экзаминуемых пользователей
    /// </summary>
    [Table("ExamineeUserGroups")]
    public class ExaminationUserGroup : BaseEntity, IDeletable
    {
        /// <summary>
        /// Дата-время начала экзамена фактически
        /// </summary>
        public DateTime? ExaminationDateStart { get; set; }
        /// <summary>
        /// Дата-время завершения экзамена  фактически
        /// </summary>
        public DateTime? ExaminationDateEnd { get; set; }
        /// <summary>
        /// Дата-время завершения экзамена
        /// </summary>
        public DateTime? ExaminationDateComplite { get; set; }
        public Guid? ExaminationResultTypeId { get; set; }
        /// <summary>
        /// Наименование типа результата экзамена (прошел или не прошел, не прИшел на экзамен и т.д.)
        /// </summary>
        public ExaminationResultType ExaminationResultType { get; set; }
        public Guid ExaminationId { get; set; }
        public Examination Examination { get; set; }
        public Guid UserId { get; set; }

        public string Comment { get; set; }

        public bool IsDeleted { get; set; }
        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }


        /// <summary>
        /// Фамилия
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string SecondName { get; set; }

        public string GetFio()
        {
            return $"{MiddleName} {FirstName} {SecondName}";
        }

        public string Inn { get; set; }




        /// <summary>
        /// Флаг показывает прошел ли тестируемый именно электронный экзамен (не важно как успешно или не успешно)
        /// </summary>
        public bool IsComputerTestFinished { get; set; }
        public decimal TestResultEvaluation { get; set; }
        /// <summary>
        /// Пройден ли экзамен полностью
        /// </summary>
        public bool IsFullTestPassed { get; set; }
        /// <summary>
        /// Оценка устного экзамена
        /// </summary>
        public short OralExamPoint { get; set; }
    }
}
