﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// Председатель экзамена
    /// </summary>
    [Table("ExaminationChairmans")]
    public class ExaminationChairman : BaseEntity
    {
        public string FIO { get; set; }
        public string IIN { get; set; }
        
        //TODO: нужно ли отношение к категориям первого уровня??
    }
}
