﻿using NetCoreDataAccess.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// Таблица-связка содержит данные о категорях для экзамена
    /// </summary>
    [Table("ExaminationCategories")]
    public class ExaminationCategory : BaseEntity, IDeletable
    {
        public int CategotyQuestionCount { get; set; }

        public Guid ExaminationId { get; set; }
        public Examination Examination { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public bool IsDeleted { get; set; }

        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }
    }
}
