﻿using NetCoreDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.Domain.Testing
{
    [Table("Answers")]
    public class Answer : BaseEntity, IDeletable
    {
        public bool IsRight { get; set; }
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }
        public bool IsDeleted { get; set; }

        public List<AnswerTranslate> AnswerTranslates { get; set; }

        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }

        public AnswerTranslate GetSystemLanguageAnswerTranslate()
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            var answerTranslate = AnswerTranslates.FirstOrDefault(c => c.LanguageCode == systemLanguageCode) ??
                                  AnswerTranslates.FirstOrDefault(c => c.LanguageCode == LanguageDto.DefaultLanguage);
            return answerTranslate;
        }

        public bool HasSystemLanguageAnswerTranslate => AnswerTranslates != null && AnswerTranslates.Any(c => c.LanguageCode == AmbientContext.Current.Localization.CurrentLanguageCode);
    }
}
