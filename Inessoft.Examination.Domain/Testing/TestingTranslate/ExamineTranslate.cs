﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing.TestingTranslate
{
    [Table("ExamineTranslates")]
    public class ExamineTranslate : BaseEntityTranslate
    {
        public string Text { get; set; }
        public Guid ExaminationId { get; set; }
        public Examination Examination { get; set; }
        public void SetTranslate(string text)
        {
            Text = text;
        }
    }
}
