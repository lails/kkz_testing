﻿using NetCoreDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing.TestingTranslate
{
    [Table("QuestionTranslates")]
    public class QuestionTranslate : BaseEntityTranslate
    {
        public string Text { get; set; }
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }
        public void SetTranslate(string text)
        {
            Text = text;
        }
    }
}
