﻿using NetCoreDataAccess.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing.TestingTranslate
{
    [Table("AnswerTranslates")]
    public class AnswerTranslate : BaseEntityTranslate
    {
        public string Text { get; set; }
        [NotMapped]
        public Guid QuestionId { get; set; }
        public Guid AnswerId { get; set; }
        public Answer Answer { get; set; }

        public void SetTranslate(string text)
        {
            Text = text;
        }
    }
}
