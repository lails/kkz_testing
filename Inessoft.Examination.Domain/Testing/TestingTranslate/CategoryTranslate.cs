﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using NetCoreDataAccess.Interfaces;

namespace Inessoft.Examination.Domain.Testing.TestingTranslate
{
    [Table("CategoryTranslates")]
    public class CategoryTranslate : BaseEntityTranslate, IDeletable
    {
        public string Text { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public void SetTranslate(string text)
        {
            Text = text;
        }

        public bool IsDeleted { get; set; }
        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }
    }
}
