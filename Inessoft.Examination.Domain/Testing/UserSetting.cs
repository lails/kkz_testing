﻿using System;

namespace Inessoft.Examination.Domain.Testing
{
    public class UserSetting: BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid? CategoryId { get; set; }
        public Category Category { get; set; }
    }
}