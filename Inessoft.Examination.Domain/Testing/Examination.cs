﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDataAccess.Interfaces;
using NetCoreDI;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// Экзамен
    /// </summary>
    [Table("Examinations")]
    public class Examination : BaseEntity, IDeletable
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int Duration { get; set; }
        public int QuestionCount { get; set; }
        public int RequiredEvaluationSummary { get; set; }

        public int PointFive { get; set; }
        public int PointFour { get; set; }
        public int PointThree { get; set; }

        public Guid ChairmanId { get; set; }
        public string ChairmanName { get; set; }
        public string ChairmanInn { get; set; }
        //public ExaminationChairman Chairman { get; set; }
        public bool IsDeleted { get; set; }

        public List<ExamineTranslate> ExamineTranslates { get; set; }
        public List<ExaminationCategory> ExaminationCategories { get; set; }
        public List<ExaminationUserGroup> ExaminationUserGroups { get; set; }

        public ExamineTranslate GetSystemLanguageQuestionTranslate()
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            var examineTranslate = ExamineTranslates.FirstOrDefault(c => c.LanguageCode == systemLanguageCode) ??
                                   ExamineTranslates.FirstOrDefault(c => c.LanguageCode == LanguageDto.DefaultLanguage);

            return examineTranslate;
        }

        public bool HasSystemLanguageQuestionTranslate => ExamineTranslates != null && ExamineTranslates.Any(c => c.LanguageCode == AmbientContext.Current.Localization.CurrentLanguageCode);
    }
}
