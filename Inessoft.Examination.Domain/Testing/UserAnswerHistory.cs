﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Testing
{
    /// <summary>
    /// История ответов на экзамен
    /// </summary>
    [Table("UserAnswerHistories")]
    public class UserAnswerHistory : BaseEntity
    {
        public string QuestionText { get; set; }
        public string AnswerText { get; set; }
        public string RightAnswerText { get; set; }
        public bool IsRightAnswer { get; set; }
        /// <summary>
        /// Оценка ответа(может быть меньше единицы и больше нуля, если считаются нексолько ответов отдельно)
        /// </summary>
        public decimal AnswerEvaluation { get; set; }
        /// <summary>
        /// Вопрос был отвечен
        /// </summary>
        public bool IsQuestionAnswered { get; set; }
        /// <summary>
        /// Номер вопроса в рамках экзамена (нужен будет для перехода по вопросу следующий, предыдщий, 10й..)
        /// </summary>
        public int ExaminationQuestionNumber { get; set; }

        public Guid ExamineeUsergroupId { get; set; }
        public ExaminationUserGroup ExaminationUsergroup { get; set; }

        public Guid ExaminationId { get; set; }
        public Examination Examination { get; set; }

        /// <summary>
        /// Ссылка на вопрос нужна, что бы знать сколько вопросов задано по текущему экзамену в конкретной категории
        /// </summary>
        public Guid QuestionId { get; set; }
        public Question Question { get; set; }

        public List<UserAnswerHistoryAnswer> UserAnswerHistoryAnswers { get; set; }
    }
}
