﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using NetCoreDataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.Domain.Testing
{
    [Table("Categories")]
    public class Category : BaseEntity, IDeletable
    {
        public Guid? ParentCategoryId { get; set; }
        public Category ParentCategory { get; set; }

        public List<CategoryTranslate> CategoryTranslates { get; set; }

        public List<Question> Questions { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsCanAddQuestions { get; set; }

        public void MarkAsDelete()
        {
            IsDeleted = true;
        }

        public void SetParent(Guid? parentId)
        {
            ParentCategoryId = parentId;
        }

        public CategoryTranslate GetSystemLanguageCategoryTranslate()
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            var categoryTranslate = CategoryTranslates.FirstOrDefault(c => c.LanguageCode == systemLanguageCode) ??
                                    CategoryTranslates.FirstOrDefault(c => c.LanguageCode == LanguageDto.DefaultLanguage);
            return categoryTranslate;
        }

        public bool HasSystemLanguageCategoryTranslate => CategoryTranslates != null && CategoryTranslates.Any(c => c.LanguageCode == AmbientContext.Current.Localization.CurrentLanguageCode);
    }
}
