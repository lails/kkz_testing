﻿using NetCoreDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using NetCoreDI;

namespace Inessoft.Examination.Domain.Testing
{
    [Table("Questions")]
    public class Question : BaseEntity, IDeletable
    {
        public bool IsMultipleAnswer { get; set; }
        public string ArticleNumber { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public bool IsDeleted { get; set; }

        public List<QuestionTranslate> QuestionTranslates { get; set; }

        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }

        public QuestionTranslate GetSystemLanguageQuestionTranslate()
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            var questionTranslate = QuestionTranslates.FirstOrDefault(c => c.LanguageCode == systemLanguageCode) ??
                                    QuestionTranslates.FirstOrDefault(c => c.LanguageCode == LanguageDto.DefaultLanguage);
            return questionTranslate;
        }

        public bool HasSystemLanguageQuestionTranslate => QuestionTranslates != null && QuestionTranslates.Any(c => c.LanguageCode == AmbientContext.Current.Localization.CurrentLanguageCode);
    }
}
