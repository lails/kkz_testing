﻿namespace Inessoft.Examination.Domain
{
    public class BaseEntityTranslate : BaseEntity
    {
        public string LanguageCode { get; set; }
    }
}
