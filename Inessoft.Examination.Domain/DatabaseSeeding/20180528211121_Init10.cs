﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.DatabaseSeeding
{
    public partial class Init10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Examinations_ExaminationChairmans_ChairmanId",
                table: "Examinations");

            migrationBuilder.DropForeignKey(
                name: "FK_ExaminationSettings_ExaminationChairmans_ChairmanId",
                table: "ExaminationSettings");

            migrationBuilder.DropIndex(
                name: "IX_ExaminationSettings_ChairmanId",
                table: "ExaminationSettings");

            migrationBuilder.DropIndex(
                name: "IX_Examinations_ChairmanId",
                table: "Examinations");

            migrationBuilder.AddColumn<string>(
                name: "ChairmanInn",
                table: "ExaminationSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChairmanName",
                table: "ExaminationSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChairmanInn",
                table: "Examinations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChairmanName",
                table: "Examinations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChairmanInn",
                table: "ExaminationSettings");

            migrationBuilder.DropColumn(
                name: "ChairmanName",
                table: "ExaminationSettings");

            migrationBuilder.DropColumn(
                name: "ChairmanInn",
                table: "Examinations");

            migrationBuilder.DropColumn(
                name: "ChairmanName",
                table: "Examinations");

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationSettings_ChairmanId",
                table: "ExaminationSettings",
                column: "ChairmanId");

            migrationBuilder.CreateIndex(
                name: "IX_Examinations_ChairmanId",
                table: "Examinations",
                column: "ChairmanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Examinations_ExaminationChairmans_ChairmanId",
                table: "Examinations",
                column: "ChairmanId",
                principalTable: "ExaminationChairmans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExaminationSettings_ExaminationChairmans_ChairmanId",
                table: "ExaminationSettings",
                column: "ChairmanId",
                principalTable: "ExaminationChairmans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
