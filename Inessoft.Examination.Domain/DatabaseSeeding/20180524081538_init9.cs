﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.DatabaseSeeding
{
    public partial class init9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsFullTestPassed",
                table: "ExamineeUserGroups",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsFullTestPassed",
                table: "ExamineeUserGroups",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
