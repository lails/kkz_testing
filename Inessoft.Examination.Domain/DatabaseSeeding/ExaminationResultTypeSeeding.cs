﻿using Inessoft.Examination.Domain.Dictionaries;
using Inessoft.Examination.Domain.Dictionaries.DictonaryTranslates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Inessoft.Examination.Domain.DatabaseSeeding
{
    [DbContext(typeof(ExaminationDbContext))]
    [Migration("ExaminationResultType_Seed")]
    public partial class ExaminationResultTypeSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var tableName = typeof(ExaminationResultType).Name + "s";
            var tableFields = new[] { "Id", "Code" };

            var translateTableName = typeof(ExaminationResultTypeTranslate).Name + "s";
            var translateTableFields = new[] { "Id", "LanguageCode", "Text", "ExaminationResultTypeId" };
            Guid parentCode;

            parentCode = Guid.NewGuid();
            migrationBuilder.InsertData(tableName, tableFields, new object[] { parentCode, "01" });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "ru", "Cдан", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "kz", "Cдан", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "en", "Passed", parentCode });

            parentCode = Guid.NewGuid();
            migrationBuilder.InsertData(tableName, tableFields, new object[] { parentCode, "02" });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "ru", "Не сдан", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "kz", "Не сдан", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "en", "Not passed", parentCode });

            parentCode = Guid.NewGuid();
            migrationBuilder.InsertData(tableName, tableFields, new object[] { parentCode, "03" });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "ru", "Отсуствовал по уважительной причине", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "kz", "Не сдан kz", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "en", "Отсуствовал по уважительной причине en", parentCode });

            parentCode = Guid.NewGuid();
            migrationBuilder.InsertData(tableName, tableFields, new object[] { parentCode, "04" });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "ru", "Отсуствовал по не уважительной причине", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "kz", "Отсуствовал по не уважительной причине kz", parentCode });
            migrationBuilder.InsertData(translateTableName, translateTableFields, new object[] { Guid.NewGuid(), "en", "Отсуствовал по не уважительной причине en", parentCode });
        }
    }
}
