﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Domain.DatabaseSeeding
{
    public partial class int7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "ExaminationResultTypes");

            migrationBuilder.CreateTable(
                name: "ExaminationResultTypeTranslates",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ExaminationResultTypeId = table.Column<Guid>(nullable: false),
                    LanguageCode = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExaminationResultTypeTranslates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExaminationResultTypeTranslates_ExaminationResultTypes_ExaminationResultTypeId",
                        column: x => x.ExaminationResultTypeId,
                        principalTable: "ExaminationResultTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExaminationResultTypeTranslates_ExaminationResultTypeId",
                table: "ExaminationResultTypeTranslates",
                column: "ExaminationResultTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExaminationResultTypeTranslates");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ExaminationResultTypes",
                nullable: true);
        }
    }
}
