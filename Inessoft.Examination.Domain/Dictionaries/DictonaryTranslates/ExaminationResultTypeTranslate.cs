﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Dictionaries.DictonaryTranslates
{
    [Table("ExaminationResultTypeTranslates")]
    public class ExaminationResultTypeTranslate : BaseEntityTranslate
    {
        public string Text { get; set; }

        public Guid ExaminationResultTypeId { get; set; }
        public ExaminationResultType ExaminationResultType { get; set; }

        public void SetTranslate(string text)
        {
            Text = text;
        }
    }
}
