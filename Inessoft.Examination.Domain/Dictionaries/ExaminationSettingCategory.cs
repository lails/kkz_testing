﻿using Inessoft.Examination.Domain.Testing;
using NetCoreDataAccess.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Dictionaries
{
    /// <summary>
    /// Таблица-связка содержит данные о категорях для экзамена
    /// </summary>
    [Table("ExaminationSettingCategories")]
    public class ExaminationSettingCategory : BaseEntity, IDeletable
    {
        public Guid ExaminationSettingId { get; set; }
        public ExaminationSetting ExaminationSetting { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public int CategotyQuestionCount { get; set; }
        public bool IsDeleted { get; set; }

        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }
    }
}
