﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Inessoft.Examination.Common.Dtos;
using Inessoft.Examination.Domain.Dictionaries.DictonaryTranslates;
using NetCoreDI;

namespace Inessoft.Examination.Domain.Dictionaries
{
    /// <summary>
    /// Тип результата экзамена
    /// </summary>
    [Table("ExaminationResultTypes")]
    public class ExaminationResultType : BaseEntity
    {
        public string Code { get; set; }

        public List<ExaminationResultTypeTranslate> ExaminationResultTypeTranslates { get; set; }

        public ExaminationResultTypeTranslate GetSystemLanguageQuestionTranslate()
        {
            var systemLanguageCode = AmbientContext.Current.Localization.CurrentLanguageCode;
            var resultTypeTranslate = ExaminationResultTypeTranslates?.FirstOrDefault(c => c.LanguageCode == systemLanguageCode) ??
                                      ExaminationResultTypeTranslates?.FirstOrDefault(c => c.LanguageCode == LanguageDto.DefaultLanguage);
            return resultTypeTranslate;
        }

        public bool HasSystemLanguageQuestionTranslate => ExaminationResultTypeTranslates != null && ExaminationResultTypeTranslates.Any(c => c.LanguageCode == AmbientContext.Current.Localization.CurrentLanguageCode);
    }
}
