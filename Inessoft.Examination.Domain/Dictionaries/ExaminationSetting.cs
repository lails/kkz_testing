﻿using Inessoft.Examination.Domain.Testing;
using NetCoreDataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Inessoft.Examination.Domain.Dictionaries
{
    /// <summary>
    /// Настройка для экзамена
    /// </summary>
    [Table("ExaminationSettings")]
    public class ExaminationSetting : BaseEntity, IDeletable
    {
        public string Name { get; set; }
        public int Duration { get; set; }
        public int QuestionCount { get; set; }
        public int RequiredCountOfCorrectAnswers { get; set; }
        public int PointFive { get; set; }
        public int PointFour { get; set; }
        public int PointThree { get; set; }

        public Guid ChairmanId { get; set; }
        public string ChairmanName { get; set; }
        public string ChairmanInn { get; set; }
        //public ExaminationChairman Chairman { get; set; }

        public List<ExaminationSettingCategory> ExaminationSettingCategories { get; set; }

        public bool IsDeleted { get; set; }
        public void MarkAsDeleted()
        {
            IsDeleted = true;
        }
    }
}
