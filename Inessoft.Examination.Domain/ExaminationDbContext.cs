﻿using Inessoft.Examination.Domain.Dictionaries;
using Inessoft.Examination.Domain.Dictionaries.DictonaryTranslates;
using Inessoft.Examination.Domain.Testing;
using Inessoft.Examination.Domain.Testing.TestingTranslate;
using Microsoft.EntityFrameworkCore;

namespace Inessoft.Examination.Domain
{
    public class ExaminationDbContext : DbContext
    {
        public ExaminationDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Answer> AnswerSet { get; set; }
        public DbSet<Category> CategorySet { get; set; }
        public DbSet<Testing.Examination> ExaminationSet { get; set; }
        public DbSet<ExaminationCategory> ExaminationCategorieSet { get; set; }
        public DbSet<ExaminationChairman> ExaminationChairmanSet { get; set; }
        public DbSet<ExaminationUserGroup> ExamineeUserGroupSet { get; set; }
        public DbSet<Question> QuestionSet { get; set; }
        public DbSet<UserAnswerHistory> UserAnswerHistorySet { get; set; }
        public DbSet<UserAnswerHistoryAnswer> UserAnswerHistoryAnswerSet { get; set; }

        public DbSet<UserSetting> UserSettings { get; set; }

        public DbSet<AnswerTranslate> AnswerTranslateSet { get; set; }
        public DbSet<CategoryTranslate> CategoryTranslateSet { get; set; }
        public DbSet<QuestionTranslate> QuestionTranslateSet { get; set; }
        public DbSet<ExamineTranslate> ExamineTranslateSet { get; set; }
        public DbSet<ExaminationResultTypeTranslate> ExaminationResultTypeTranslateSet { get; set; }

        public DbSet<ExaminationSetting> ExaminationSettings { get; set; }
        public DbSet<ExaminationSettingCategory> ExaminationSettingCategorySet { get; set; }
        public DbSet<ExaminationResultType> ExaminationResultTypeSet { get; set; }
    }
}
