﻿using System;

namespace Inessoft.Examination.Di
{
    public class DateTimeProvider: IDateTimeProvider
    {
        public DateTime Now => DateTime.Now;
    }
}