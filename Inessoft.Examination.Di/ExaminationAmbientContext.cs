﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetCoreCQRS;
using System;
using Inessoft.Examination.Authentication;
using Inessoft.Examination.ExcelDataHandler;

namespace Inessoft.Examination.Di
{
    public class ExaminationAmbientContext
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly IConfiguration _configuration;

        private static ExaminationAmbientContext _current;

        public static ExaminationAmbientContext Current
        {
            get
            {
                if (_current == null)
                {
                    throw new Exception($"{nameof(ExaminationAmbientContext)} current is null");
                }

                return _current;
            }
        }

        public ExaminationAmbientContext(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _configuration = configuration;

            _current = this;
        }

        public IConfiguration Configuration => _configuration;

        public IExecutor Executor => _serviceProvider.GetRequiredService<IExecutor>();

        public IExaminationUserAuthenticationService AuthenticationService => _serviceProvider.GetRequiredService<IExaminationUserAuthenticationService>();

        public IDateTimeProvider DateTimeProvider => _serviceProvider.GetRequiredService<IDateTimeProvider>();

        public IExcelDataHandlersService ExcelDataHandlersService => _serviceProvider.GetRequiredService<IExcelDataHandlersService>();
    }
}
