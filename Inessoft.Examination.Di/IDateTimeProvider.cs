﻿using System;

namespace Inessoft.Examination.Di
{
    public interface IDateTimeProvider
    {
        DateTime Now { get; }
    }
}