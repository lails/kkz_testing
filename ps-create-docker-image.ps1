param($buildNumber)

cd  C:/KKZTestingBuild/KKZTesting_$buildNumber

docker rmi kkzexamination 

docker build -t kkzexamination .

docker save -o C:/KKZTestingBuild/KKZTesting_$buildNumber/docker_images kkzexamination 