﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NetCoreHttpClient;
using NetCoreIdentityHttpClient.Configurations;
using NetCoreIdentityHttpClient.Contracts;
using Newtonsoft.Json;

namespace NetCoreIdentityHttpClient
{
    public class NetCoreIdentityHttpClient : INetCoreIdentityHttpClient
    {
        private readonly NetCoreBaseHttpClient _httpClient;

        public NetCoreIdentityHttpClient(IHttpContextAccessor httpContextAccessor)
        {
            var configuration = new NetCoreIdentityHttpClientConfiguration();

            _httpClient = new NetCoreBaseHttpClient(httpContextAccessor);
            _httpClient.ConfigureServiceHttpClient(new NetCoreHttpClientConfigurationOptions { HttpClientBaseAddress = configuration.Uri });
        }

        public async Task<List<NetCoreIdentityUser>> GetAllUsers()
        {
            var client = await _httpClient.GetHttpClient();
            var response = await client.GetAsync("api/getAllUsers").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var identityUsers = JsonConvert.DeserializeObject<List<NetCoreIdentityUser>>(jsonResponse);
                return identityUsers;
            }

            throw new Exception("Error");
        }
    }
}
