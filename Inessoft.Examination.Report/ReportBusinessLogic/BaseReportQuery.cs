﻿using Inessoft.Examination.Report;
using NetCoreCQRS.Queries;

namespace Inessoft.Examination.Report.ReportBusinessLogic
{
    internal abstract class BaseReportQuery : BaseQuery
    {
        internal abstract ReportData Execute(ReportConditionData reportFilterData);
    }
}
