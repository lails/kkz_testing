﻿using System;
using System.Collections.Generic;
using Inessoft.Examination.Report.ReportBusinessLogic;

namespace Inessoft.Examination.Report.Reports
{
    /// <summary>
    /// Отчет Сведения по поступившим в РГП «НИИС» заявкам на выдачу охранных документов на объекты промышленной собственности за год
    /// </summary>
    internal class ReceivedRequestReport : ReportBase<GetReportDataReceivedRequestReportQuery>
    {
        public ReceivedRequestReport(ReportConditionData rportConditionData) : base(rportConditionData)
        {
            rportConditionData.DateFrom = rportConditionData.DateFrom ?? new DateTimeOffset(new DateTime(DateTime.Now.Year, 1, 1));
            rportConditionData.DateTo = rportConditionData.DateTo ?? DateTimeOffset.Now;

            rportConditionData.DateFrom = rportConditionData.DateFrom.Value.ToLocalTime();
            rportConditionData.DateTo = rportConditionData.DateTo.Value.ToLocalTime();
        }

        protected override string ReportTemplate => "ReceivedRequestReportTemplate.xlsx";

        protected override int RowStart => 1;
        protected override int ColumnStart => 1;

        protected override List<Row> GetReportHeader()
        {
            var header = new List<Row>
            {
                new Row
                {
                    IsHeader = true,

                    Cells = new List<Cell>
                    {
                        new Cell{ Value = "№", RowSpan = 2 },
                        new Cell{ Value =
                            $"Сведения по заявкам с {ReportConditionData.DateFrom.Value.ToLocalTime().Date:dd.MM.yyyy} по {ReportConditionData.DateTo.Value.ToLocalTime().Date:dd.MM.yyyy}", RowSpan = 2 },

                        new Cell{ Value = "Количество поступивших заявок", CollSpan = 3 }
                    }
                },
                new Row
                {
                    IsHeader = true,

                    Cells = new List<Cell>
                    {
                        new Cell{ },
                        new Cell{ },
                        new Cell{ Value = "От национальных заявителей" },
                        new Cell{ Value = "От иностранных заявителей" },
                        new Cell{ Value = "Всего"  },
                    }
                }
            };

            header.Reverse();

            return header;
        }
    }
}
