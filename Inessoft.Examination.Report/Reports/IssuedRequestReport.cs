﻿using Inessoft.Examination.Report.ReportBusinessLogic;
using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Report.Reports
{
    /// <summary>
    /// Сведения по выданным охранным документам на объекты промышленной собственности РГП «НИИС» за указанный период
    /// </summary>
    internal class IssuedProtectionDocumentsReport : ReportBase<GetReportDataIssuedProtectionDocumentsReportQuery>
    {
        public IssuedProtectionDocumentsReport(ReportConditionData rportConditionData) : base(rportConditionData)
        {
            rportConditionData.DateFrom = rportConditionData.DateFrom ?? new DateTimeOffset(new DateTime(DateTime.Now.Year, 1, 1));
            rportConditionData.DateTo = rportConditionData.DateTo ?? DateTimeOffset.Now;

            rportConditionData.DateFrom = rportConditionData.DateFrom.Value.ToLocalTime();
            rportConditionData.DateTo = rportConditionData.DateTo.Value.ToLocalTime();
        }

        protected override string ReportTemplate => "IssuedProtectionDocumentsReportTemplate.xlsx";

        protected override int RowStart => 1;

        protected override int ColumnStart => 1;
       
        protected override List<Row> GetReportHeader()
        {
            var header = new List<Row>
            {
                new Row
                {
                    IsHeader = true,

                    Cells = new List<Cell>
                    {
                        new Cell{ Value = "№", RowSpan = 2 },
                        new Cell{ Value = "Объекты промышленной собственности", RowSpan = 2 },
                        new Cell{ Value = "Кол-во выданных охранных документов (патентов)", CollSpan = 2 },
                        new Cell{ Value = "Кол-во  выданных охранных документов (свидетельств, выписок)", CollSpan = 2 },
                        new Cell{ Value = "Всего", RowSpan = 2 }
                    }
                },
                new Row
                {
                    IsHeader = true,

                    Cells = new List<Cell>
                    {
                        new Cell{ },
                        new Cell{ },
                        new Cell{ Value = "Национальным заявителям" },
                        new Cell{ Value = "Иностранным заявителям" },
                        new Cell{ Value = "Национальным заявителям" },
                        new Cell{ Value = "Иностранным заявителям" },
                    }
                }
            };

            header.Reverse();

            return header;
        }
    }
}
