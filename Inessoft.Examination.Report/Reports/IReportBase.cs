﻿//using Inessoft.Examination.Report.Chart;

namespace Inessoft.Examination.Report.Reports
{
    public interface IReportBase
    {
        ReportData GetData();
        byte[] GetAsExcel();
        byte[] GetAsPDF();
       // ChartDataBase GetChartData();
    }
}
