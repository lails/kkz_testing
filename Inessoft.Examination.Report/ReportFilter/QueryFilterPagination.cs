﻿using Inessoft.Examination.Domain;
using System.Linq;

namespace Inessoft.Examination.Report.ReportFilter
{
    internal static class QueryFilterPagination
    {
        internal static IQueryable<T> AddPaging<T>(this IQueryable<T> query, int itemsPerCount, int pageNumber)
            where T : BaseEntity, new()
        {
            query = query.Skip(itemsPerCount * (pageNumber - 1)).Take(itemsPerCount);

            return query;
        }
    }
}
