﻿using Inessoft.Examination.Domain;
using System.Linq;

namespace Inessoft.Examination.Report.ReportFilter
{
    internal static class QueryFilter
    {
        internal static IQueryable<T> AddReportFilter<T>(this IQueryable<T> query, ReportFilterData reportFilterData)
            where T : BaseEntity, new()
        {

            //TODO: Not implemented
            query = query.AddSorting();

            query = query.AddPaging(reportFilterData.ItemsCountPerPage, reportFilterData.PageNumber);

            return query;
        }
    }
}
