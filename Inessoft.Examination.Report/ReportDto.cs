﻿namespace Inessoft.Examination.Report
{
    public class ReportDto
    {

        public string Code { get; set; }
        public string Name { get; set; }

    }
}
