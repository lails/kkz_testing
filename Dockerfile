FROM microsoft/aspnetcore:2.0
ARG source
WORKDIR /app
EXPOSE 44317
COPY publish .
ENTRYPOINT ["dotnet", "Inessoft.Examination.Web.Api.dll"]
