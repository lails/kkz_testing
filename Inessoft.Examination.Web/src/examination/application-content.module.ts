import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { ApplicationContentComponent } from 'examination/application-content.component';


import { CategotyComponent } from './category/category.component';
import { CategoryDetailComponent } from './category/category-detail.component';

import { CategoryService } from './category/category.service';
import { QuestionService } from './question/question.service';

import { MonitoringTableComponent, MonitoringService } from './monitoring';

import { DataService } from '../shared/http-extensions';

import { TreeView } from 'base-components/tree-view/tree-view.component';
import { InputFieldComponent } from '../base-components/input-field/input-field.component';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

import { RTModule, RTSelectionModule, RTSelectionService, RTListsModule, FiltersService, PagedPagerComponent, RTFiltersService, RTBufferedPager, RTPagedPager, RTSortingsService, PageNumberDirective, PagedPager, BufferedPager } from 'right-angled';
import { CoreFullConfirmModalComponent } from '../base-components/core-full-confirm-modal/core-full-confirm-modal.component';
import { CorePaginationComponent } from '../base-components/core-pagination-component/core-pagination.component';

import { Interceptor, NotificationService, PrintServce } from '../shared/services';
import { QuestionTableComponent } from './question/question-table.component';
import { RouterModule } from '@angular/router';
import { RoutingModule } from './routing.module';
import { QuestionDetailComponent } from './question/question-detail.component';
import { QuestionComponent } from './question/question.component';
import { SettingsService } from './shared/settings.service';

import { SafeHtmlPipe } from '../shared/pipes';

import { SecurityService, AdministratorRouteGuardService, EmployeeRouteGuardService } from '../shared/authentication';

import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { EditorModule } from 'primeng/editor';
import { CheckboxModule } from 'primeng/checkbox';
import { DataTableModule } from 'primeng/datatable';
import { AnswerTableComponent } from './answer/answer-table.component';
import { ExamenComponent } from './examen/examen.component';
import { ExamenTableComponent } from './examen/examen-table.component';
import { ExamenDetailComponent } from './examen/examen-detail.component';
import { ExamenService } from './examen/examen.service';
import { FieldsetModule } from 'primeng/fieldset';
import { ListboxModule } from 'primeng/listbox';
import { ExamenCategortyTableComponent } from './examen/examen-categorty-table.component';
import { PickListModule } from 'primeng/picklist';
import { UserService } from './user/user.service';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { GrowlModule } from 'primeng/growl';
import { CategoryTranslateComponent } from './translates/category_translate/category_translate.component';
import { CategoryTranslateService } from './translates/category_translate/category_translate.service';
import { QuestionTranslateService } from './translates/question-translate/question_translate.service';
import { QuestionTranslateComponent } from './translates/question-translate/question_translate.component';
import { UserComponent } from './user/user.component';
import { UserTableComponent } from './user/user-table.component';
import { UserDetailComponent } from './user/user-detail.component';
import { ExaminationStateService } from './ExaminationStateService';
import { ExamenSettingComponent } from './dictionaries/examen-setting/examen-setting.component';
import { ExamenSettingDetailComponent } from './dictionaries/examen-setting/examen-setting-detail.component';
import { ExamenSettingService } from './dictionaries/examen-setting/examen-setting.service';
import { ExamenSettingTableComponent } from './dictionaries/examen-setting/examen-setting-table.component';
import { ExaminationTestingService } from './examination-testing/examination-testing.service';
import { ExaminationTestingComponent } from './examination-testing/examination-testing.component';
import { CoreTimerComponent } from "base-components/core-timer/core-timer.component";
import { ExaminationTestingPreparingComponent } from './examination-testing/examination-testing-preparing.component';
import { RadioButtonModule } from 'primeng/radiobutton';

import { APP_TRANSLATE_PROVIDERS } from '../shared/translate'
import { TranslatePipe, TranslateService } from '../shared/translate'
import { ExaminationTestingResultComponent } from './examination-testing/examination-testing-result.component';
import { FileUploadModule } from 'primeng/fileupload';
import { FileService } from './shared/file.service';
import { LoadingModule } from 'ngx-loading';
import { CoreBreadcrumbComponent } from 'base-components/core-breadcrumb/core-breadcrumb.component';

@NgModule({
    declarations: [
        ApplicationContentComponent,

        CategotyComponent,
        CategoryDetailComponent,

        MonitoringTableComponent,

        QuestionTableComponent,
        QuestionDetailComponent,
        QuestionComponent,
        
        AnswerTableComponent,

        ExamenComponent,
        ExamenTableComponent,
        ExamenDetailComponent,
        ExamenCategortyTableComponent,
        ExamenSettingComponent,
        ExamenSettingTableComponent,
        ExamenSettingDetailComponent,
        ExaminationTestingPreparingComponent,
        ExaminationTestingComponent,
        ExaminationTestingResultComponent,

        UserComponent,
        UserTableComponent,
        UserDetailComponent,
        
        TreeView,
        InputFieldComponent,
        CoreFullConfirmModalComponent,
        CorePaginationComponent,
        CoreTimerComponent,
        CoreBreadcrumbComponent,

        CategoryTranslateComponent,
        QuestionTranslateComponent,

        TranslatePipe,
        SafeHtmlPipe
    ],

    exports: [
        TranslatePipe,
        SafeHtmlPipe
    ],

    imports:[
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RTModule,
        RTSelectionModule,
        RTListsModule,
        HttpClientModule,
        RoutingModule,
        ReactiveFormsModule,
        LoadingModule,
        //#region primeng modules

        CalendarModule,
        DropdownModule,
        KeyFilterModule,
        EditorModule,
        CheckboxModule,
        DataTableModule,
        FieldsetModule,
        ListboxModule,
        PickListModule,
        ToggleButtonModule,
        GrowlModule,
        RadioButtonModule,
        FileUploadModule

        //#endregion

    ],
    providers: [
        APP_TRANSLATE_PROVIDERS,
        {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true},
        DataService,
        ExaminationStateService,
        CategoryService, 
        QuestionService,
        MonitoringService,
        SettingsService,
        SecurityService,
        AdministratorRouteGuardService,
        EmployeeRouteGuardService,
        FileService,
        ExamenService,
        ExamenSettingService,
        ExaminationTestingService,
        UserService,
        CategoryTranslateService,
        QuestionTranslateService,        
        NotificationService,
        PrintServce,
        RTSelectionService,
        RTFiltersService,
        BufferedPager,
        PagedPager],
    bootstrap: [ApplicationContentComponent]
})

export class ApplicationContentModule{}