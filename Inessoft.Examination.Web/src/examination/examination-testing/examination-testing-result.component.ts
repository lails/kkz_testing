import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SecurityService } from "shared/authentication";
import { ExaminationTestingService } from "./examination-testing.service";
import { UserGroupResult } from "../models/UserGroupResult";

@Component({
    selector : 'examination-testing-result',
    templateUrl : './examination-testing-result.component.html'
})

export class ExaminationTestingResultComponent implements OnInit
{
    userGroupResult : UserGroupResult;

    constructor(private route : ActivatedRoute,
        private router : Router,
        private securityService : SecurityService,
        private examinationTestingService : ExaminationTestingService){}

    
    ngOnInit(): void {        
        const examUserGroupid = this.route.snapshot.paramMap.get('id');

        this.examinationTestingService.getUserExamenResult(examUserGroupid)
            .then(userGroupResult=>{
                this.userGroupResult = userGroupResult;
                console.log(userGroupResult);
            })
    }
}