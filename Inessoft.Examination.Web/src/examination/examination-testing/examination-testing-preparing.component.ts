import { Component, Output, EventEmitter, Input } from "@angular/core";
import { SecurityService } from "shared/authentication";
import { ExaminationTestingService } from "./examination-testing.service";
import { Examine } from "../models/Examine";
import { AuthenticatedUser } from "shared/models";
import { NotificationService } from "shared/services";

@Component({
    selector : 'examination-testing-preparing',
    templateUrl : './examination-testing-preparing.component.html'
})

export class ExaminationTestingPreparingComponent
{
    @Input() isExamenStarted : boolean;

    availableExams : Examine[] = [];
    userInfo : AuthenticatedUser;
    selectedExam : Examine;
    isConditionAprove : boolean = false;
    
    @Output() examenStartEmitter = new EventEmitter<Examine>();

    constructor(private securityService : SecurityService,
                private examinationTestingService : ExaminationTestingService,
                private notificationService : NotificationService){}

    ngOnInit(): void {
        
        if(this.isExamenStarted == false){
            this.getCurrentUserInfo();
            this.getAvailableExams();
        }        
    }

    getCurrentUserInfo(){
        this.userInfo = this.securityService.getCurrentUserData();
    }

    getAvailableExams(){
        this.examinationTestingService.getExaminerExams()
        .then(r=>{
            this.availableExams = r;
            var btn = document.getElementById("show_examination_testing_preparation");
            btn.click();

            if(r.length == 1)
            {
                this.selectedExam = r[0];
            }
        })
    }

    onExamSelect(exam : Examine){
        this.selectedExam = exam;
    }
    
    examenStart()
    {
        if(!this.selectedExam)
        {
            this.notificationService.warningWithTranslation('SpecMessagePleaceSelectedExam');
            return;
        }

        if(this.isConditionAprove == false)
        {
            this.notificationService.warningWithTranslation('SpecMessagePleaceAcceptAgreementBySelectedExam');
            return;
        }

        var btn = document.getElementById("hide_examination_testing_preparation");
        btn.click();
        
        this.examenStartEmitter.emit(this.selectedExam);
    }
}