import { Injectable } from "@angular/core";
import { DataService } from "shared/http-extensions";
import { Examine } from "../models/Examine";
import { Question } from "../models/Question";
import { Answer } from "../models/Answer";
import { UserGroupResult } from "../models/UserGroupResult";
import { ExaminationUserGroup } from "../models/ExaminationUserGroup";

@Injectable()

export class ExaminationTestingService{

    public constructor(private dataService: DataService) { }
      
    examinationTestingApi: string = "api/passingExaminations";

    getExaminerExams(): Promise<Examine[]> {
        return this.dataService.callService({
          method: 'GET',
          url: `${this.examinationTestingApi}/getExaminerExams`
      });
    }

    setExaminStartDate(examId: string) : Promise<string>  {        
        return this.dataService.callService({
            method: 'POST',
            url: `${this.examinationTestingApi}/${examId}/setExaminStartDate`
        });  
    }

    getCurrentUserGroupByExamIdQuery(examId: string) : Promise<ExaminationUserGroup>{
        
        return this.dataService.callService({
            method: 'GET',
            url: `${this.examinationTestingApi}/${examId}/getCurrentUserGroupByExamIdQuery`
        });  
    }

    getNextQuestionByExamenId(examId : string, questionNumber: number): Promise<Question>  {
        
        return this.dataService.callService({
            method: 'GET',
            url: `${this.examinationTestingApi}/${examId}/getNextQueston/${questionNumber}`
        });  
    }

    getAnswersByQuestoinId(examId:string, questionId : string): Promise<Answer[]>  {        
        return this.dataService.callService({
            method: 'GET',
            url: `${this.examinationTestingApi}/${examId}/getAnswers/${questionId}`
        });  
    }

    updateUserAnswer(examId:string, qustion: Question) : Promise<string>{
        return this.dataService.callService({
            data : qustion,
            method: 'POST',
            url: `${this.examinationTestingApi}/${examId}/updateUserAnswer`
        });  
    }

    checkExamenBeforeFinish(examId:string) : Promise<boolean>{
        return this.dataService.callService({
            method: 'POST',
            url: `${this.examinationTestingApi}/${examId}/checkExamenBeforeFinish`
        });  
    }

    finishExam(examId:string) : Promise<string>{
        return this.dataService.callService({
            method: 'POST',
            url: `${this.examinationTestingApi}/${examId}/finishExam`
        });  
    }

    getUserExamenResult(userGroupId: string): Promise<UserGroupResult>{
        return this.dataService.callService({
            method: 'GET',
            url: `${this.examinationTestingApi}/userResult/${userGroupId}`
        });
    }
}