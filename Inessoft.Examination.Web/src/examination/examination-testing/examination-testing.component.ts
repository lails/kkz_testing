import { Component, OnInit } from "@angular/core";
import { SecurityService } from "shared/authentication";
import { Examine } from "../models/Examine";
import { ActivatedRoute, Router } from "@angular/router";
import { ExamenService } from "../examen/examen.service";
import { ExaminationTestingService } from "./examination-testing.service";
import { Question } from "../models/Question";
import { Answer } from "../models/Answer";
import { ExaminationUserGroup } from "../models/ExaminationUserGroup";

@Component({
    selector : 'examination-testing',
    templateUrl : 'examination-testing.component.html'
})

//Прохождение тестирования
export class ExaminationTestingComponent implements OnInit{
    
    examinationUserGroup: ExaminationUserGroup;
    endDateTime: Date;
    
    isExamenStarted : boolean = true;
    currentQuestionNumber : number = 1;
    nextCuestionNumber : number;
    
    currentQuestion : Question;
    currentAnswers : Answer[];  
    
    confirmMessage : string;
    

    constructor(private route : ActivatedRoute,
                private router : Router,
                private securityService : SecurityService,
                private examinationTestingService : ExaminationTestingService){}

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        if(id != "0")
        {
            this.getExaminationUserGroup(id);
        }
        else {
            //TODO: проверить есть ли у него уже экзамены которые он начал и не закончил 
            //или вообще пересмотреть роутинг, что бы и не видел номер экзамена.
            this.isExamenStarted = false;
        }
    }

    getExaminationUserGroup(examId:string){
        this.examinationTestingService.getCurrentUserGroupByExamIdQuery(examId)
            .then(examinationUserGroup =>{   
                this.examinationUserGroup = examinationUserGroup;  
                this.endDateTime = new Date(examinationUserGroup.examinationDateComplite);
                this.getNextQuestion();                    
        })
    }

    examenStartEmitterInput(exam : Examine){
        
        this.examinationTestingService.setExaminStartDate(exam.id)
        .then(r=>{
            this.router.navigate(['/examinationtesting', exam.id])
            .then(r=>{
                this.getExaminationUserGroup(exam.id);
            });
        });        
    }


    //используется при выборе ответа на клиенте
    checkAnswer(answer : Answer)
    {
        if(!this.currentQuestion.isMultipleAnswer)
        {
            this.currentAnswers.forEach(currentAnswer => {
                if(currentAnswer.id  == answer.id)
                {
                    currentAnswer.isRight = true;
                }else{
                    currentAnswer.isRight = false;
                }
            });
        }        
    }

    private getNextQuestion(){
        const id = this.route.snapshot.paramMap.get('id');
        this.examinationTestingService.getNextQuestionByExamenId(id, this.currentQuestionNumber)
            .then(question => {
                this.currentQuestion = question;
                if(question)
                {
                    this.examinationTestingService.getAnswersByQuestoinId(id, question.id)
                    .then(answers=>{
                        this.currentAnswers = answers;
                    })
                }
            });
    }

    async toNextQuestion()
    {
        if(this.currentQuestionNumber + 1 <= this.examinationUserGroup.questionCount)
        {
            this.nextCuestionNumber = this.currentQuestionNumber + 1;

            if(await this.validateAnswersBeforeChangeQuestion())
            {
                this.currentQuestionNumber = this.nextCuestionNumber;
                this.getNextQuestion();
            }

        }
    }

    async toPrevQuestion(){
        if(this.currentQuestionNumber - 1 > 0)
        {            
            this.nextCuestionNumber = this.currentQuestionNumber - 1;

            if(await this.validateAnswersBeforeChangeQuestion())
            {
                this.currentQuestionNumber = this.nextCuestionNumber;
                this.getNextQuestion();
            }
        }        
    }

    async validateAnswersBeforeChangeQuestion()
    {
        var hasAnyAnswer = false;
        this.currentAnswers.forEach(a=>{
            if(a.isRight ==true)
            {
                hasAnyAnswer = true;
                return;
            }
        });

        if(hasAnyAnswer == false)
        {
            this.confirmMessage = "Вопрос не отвечен, желаете ответить позже?";
            var modal = document.getElementById("confirmMessageId")
            modal.click();

            return false;
        }

        return await this.updateAnswerHistory();
    }

    onConfirmedQuestionOut(isConfirmed){
        if(isConfirmed)
        {
            this.currentQuestionNumber = this.nextCuestionNumber;
            this.getNextQuestion();
        }
    }

    async updateAnswerHistory():Promise<boolean>{
        
        const id = this.route.snapshot.paramMap.get('id');
        var request = JSON.parse(JSON.stringify(this.currentQuestion));
        request.answers = this.currentAnswers.filter(r=>r.isRight == true);
        if(request.answers && request.answers.length > 0)
        {
            return await this.examinationTestingService
            .updateUserAnswer(id, request)
            .then(r=>{ return true; });
        }
        else{
            return true;
        }
        
    }

    async onTimeOverOut(){
        await this.toFinishExam();
    }

    async toFinishExam(){
        await this.updateAnswerHistory().then(r=>{            
            const id = this.route.snapshot.paramMap.get('id');

            this.examinationTestingService.checkExamenBeforeFinish(id)
            .then(r=>{
                this.examinationTestingService.finishExam(id)
                .then(examusergroupId=>{
                    this.router.navigate(['/examinationtestingresult', examusergroupId]);
                })
            })
        })
    }
}
