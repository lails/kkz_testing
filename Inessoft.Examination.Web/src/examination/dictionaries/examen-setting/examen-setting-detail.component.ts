import { Component, OnInit } from "@angular/core";
import { ExamineSetting } from "../../models/ExamineSetting";
import { SelectItem } from "primeng/api";
import { ExaminationCategory } from "../../models/ExaminationCategory";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { CategoryService } from "../../category/category.service";
import { ExamenSettingService } from "./examen-setting.service";
import { NotificationService } from "shared/services";
import { Chairman } from "../../models/\u0421hairman";
import { Category } from "../../models/Category";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    selector: 'examen-setting-detail',
    templateUrl: './examen-setting-detail.component.html'
})

export class ExamenSettingDetailComponent implements OnInit {
    
    examine : ExamineSetting = new ExamineSetting();

    chairmans : SelectItem[];
    categoriesAll : Category[];
    categories : SelectItem[];
    examinationCategories : ExaminationCategory[];

    exameSettingForm : FormGroup;

    categoryIsValid : boolean
    usersIsValid : boolean;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];


    public get questionCount() : number {
        if(this.exameSettingForm.controls && this.exameSettingForm.controls.questionCount.value)
        {
            return this.exameSettingForm.controls.questionCount.value;
        }
        return 0;
    }

    constructor (private route: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationService,
                private examenSettingService : ExamenSettingService,
                private categoryService : CategoryService,
                //private userService : UserService,
                private fb: FormBuilder
            ){
                this.createForm();
            }

    ngOnInit(): void {
        this.setFormValues();

        this.getExaminationChairmans();
        this.getCategories();
        
        const id = this.route.snapshot.paramMap.get('id');
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/examensettings", "ExamExamSettings", false));
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem(`/examensetting/${id}`, "", true));                
    }
    
    //#region Работа с формой

    createForm() 
    {
        this.exameSettingForm = this.fb.group({
            id: this.examine.id,
            name: [null, Validators.required ],
            duration: [null, Validators.required ],
            questionCount: [null, Validators.required ],
            requiredCountOfCorrectAnswers: [null, Validators.required ],
            pointfive: [null, Validators.required ],
            pointfour: [null, Validators.required ],
            pointthree: [null, Validators.required ],
            chairmanId: [null, Validators.required ],
        //    examenSettingParentCategoryId : [null, Validators.required]
        });
    }

    setFormValues(){
        const id = this.route.snapshot.paramMap.get('id');
        
        if(id !== "0")
        {
            this.examenSettingService.getExamenById(id)
                .then((examen:ExamineSetting)=>
                {                
                    this.examine = examen;

                    this.exameSettingForm.setValue({
                        id: this.examine.id,
                        name:  this.examine.name,
                        duration:  this.examine.duration,
                        questionCount:  this.examine.questionCount,
                        requiredCountOfCorrectAnswers:  this.examine.requiredCountOfCorrectAnswers,
                        pointfive:  this.examine.pointFive,
                        pointfour:  this.examine.pointFour,
                        pointthree:  this.examine.pointThree,
                        chairmanId:  this.examine.chairman.id,
                    })
                });
        }
    }

    //----------------------------------------Валидация

    public validatErrors(controlName:any) : boolean {
        
        return this.exameSettingForm.controls[controlName].errors == null;
    }

    //#endregion


    private getExaminationChairmans() : void {
        this.examenSettingService.getExaminationChairmans()
        .then(chairmans =>{
            this.chairmans = chairmans.map(chairman => {
                return  {
                    label : `${chairman.secondName} ${chairman.firstName} ${chairman.middleName}`,
                    value : chairman.id
                };
            });

            this.chairmans.splice(0,0,{label:"",value:""});
        });
    }

    private getCategories() : void {
        this.categoryService.getCategories()
        .then(categories=>{
            if(categories.items){
                this.categoriesAll = categories.items;
                this.categories = categories.items.filter(r=>r.parentId !== null).map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
            }
        });
    }

    onSelectCategoryOut(category: Category){
      /* 
        if(this.examinationCategories && this.examinationCategories.length > 0)
        {
            this.exameSettingForm.controls.examenSettingParentCategoryId.setValue(category.id);    
        }*/
    }

    onexaminationCategoriesChange(examinationCategories:ExaminationCategory[])
    {
        this.examinationCategories = examinationCategories;
    }

    validateCategory()
    {
        var questionSum = 0;
        var parentCateoryId = "";
        this.categoryIsValid = true;

        this.examinationCategories
        .forEach(r => {
            questionSum += r.categoryQuestionCount;
            if(r.categoryQuestionCount > r.fullCategoryQuestionCount)
            {
                this.notificationService.warningWithTranslation('SpecMessageCategoriesTableDataNotValid');
                this.categoryIsValid = false;
                return;
            }

            this.categoriesAll.filter(c=>{
                if(parentCateoryId=="")
                {
                    if(c.id == r.categoryId)
                    {
                        parentCateoryId = c.parentId
                    }                    
                }
                if(c.id == r.categoryId && parentCateoryId  !== c.parentId)
                {
                    this.notificationService.warningWithTranslation('SpecMessageExaminationCategoriesNotValid');
                    this.categoryIsValid = false;
                    return;
                }  
            })

            if(!this.categoryIsValid)
            {
                return;
            }
        });

        if(!this.categoryIsValid)
        {
            return;
        }

        if(this.exameSettingForm.controls.questionCount.value !== questionSum)
        {
            this.notificationService.warning(`Несоотвествует количество вопросов, использовано ${questionSum} из назначеных ${this.exameSettingForm.controls.questionCount.value}`);
            this.categoryIsValid = false;
        }
    }

    onSubmit(): void {
        var request = this.exameSettingForm.value;
            request.chairman = new Chairman();
            request.chairman.id = this.exameSettingForm.value.chairmanId;
            var chairmanSelected = this.chairmans.filter(r=>r.value == this.exameSettingForm.value.chairmanId);
            if(chairmanSelected && chairmanSelected.length == 1)
            {
                request.chairman.fio = chairmanSelected[0].label;
            }

            request.examinationCategories = this.examinationCategories;

        if(this.exameSettingForm.valid)
        {
            this.validateCategory();
            if(this.categoryIsValid == false)
            {
                return;
            }

            if(this.examine.isNew && this.examine.isNew() == true)
            {
                this.examenSettingService.createExamenSetting(request).then(result=> {
                    this.router.navigate(['/examensetting', result]).then(r=>{
                        this.setFormValues();
                        this.notificationService.saved();
                    });
                });
            } 
            else {
                this.examenSettingService.updateExamenSetting(request).then(result=> {
                    this.notificationService.updated();
                });
            }
        }else{
            this.notificationService.formNotValid();
        }
    }
}