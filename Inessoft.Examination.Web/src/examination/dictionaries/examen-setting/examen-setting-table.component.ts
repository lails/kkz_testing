import { Component, ViewChild, OnInit } from "@angular/core";
import { filter, FilterConfig, RTSelectionService, PagedPager, ListResponse, SelectionAreaDirective } from "right-angled";
import { ExamenSettingService } from "./examen-setting.service";
import { ExamineSetting } from "../../models/ExamineSetting";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    selector: 'examen-setting-table',
    templateUrl: './examen-setting-table.component.html'
})

export class ExamenSettingTableComponent implements OnInit{

    pageSize:number  = 10;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    //#region   
    
        @filter(<FilterConfig>{ parameterName: 'categoryId' })
        public categoryId: string = null;
        @filter(<FilterConfig>{ categoryName: 'categoryName' })
        public categoryName: string = null;
        @filter(<FilterConfig>{ categoryName: 'articleNumber' })
        public articleNumber: string = null;
        
    //#endregion

    constructor(private examenSettingService : ExamenSettingService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager){}

    ngOnInit(): void {
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/examensettings", "ExamExamSettings",true));
    }

    getExamens = (request): Promise<ListResponse<ExamineSetting>> => {
        return this.examenSettingService.getExamens(request);
    }
    
    @ViewChild(SelectionAreaDirective) public selectionArea: SelectionAreaDirective;
    selectedQuestionNames :string;

    public getQuestionGridSelection(): void {
        this.selectedQuestionNames = this.selectionArea.selectionService
                .getSelectedElements()
                .map((c:ExamineSetting)=>c.id)
                .join(";");     
        document.getElementById("deletQuestionConfirm").click();
    }

    onDeleteQuestionConfirmed(confirmed:boolean)
    {
        if(confirmed == true)
        {
            //Удалим Вопросы
            //Сделаем резет вопросов
        }
    }
 }