import { Injectable } from '@angular/core';
import { DataService } from 'shared/http-extensions';
import { ListResponse } from 'right-angled';
import { ExamineSetting } from '../../models/ExamineSetting';
import { User } from '../../models/User';
import { ExaminationCategory } from '../../models/ExaminationCategory';

@Injectable()

export class ExamenSettingService {
  
  public constructor(private dataService: DataService) { }

  examenSettingApi: string = "api/examenSettings";

  getExamens(request:any): Promise<ListResponse<ExamineSetting>> {
      return this.dataService.callService({
        data: request,
        method: 'POST',
        url: `${this.examenSettingApi}`
    });  
  }

  getExamenById(id:string): Promise<ExamineSetting> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.examenSettingApi}/${id}`
    });
  }

  getExaminationChairmans(): Promise<User[]> {
      return this.dataService.callService({        
        method: 'GET',
        url: `api/examinations/chairmans`
    });
  }
  
  getExamenCategoriesByExamenId(examenId:string): Promise<ExaminationCategory[]> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.examenSettingApi}/${examenId}/categories`
    });  
  }

  createExamenSetting(examentSetting: any): Promise<string> {
    return this.dataService.callService({
      data: examentSetting,
      method: 'POST',
      url: `${this.examenSettingApi}/create`
    });
  }

  updateExamenSetting(examentSetting: any): Promise<string> {
    return this.dataService.callService({
      data: examentSetting,
      method: 'POST',
      url: `${this.examenSettingApi}/update`
    });
  }
  
  deleteExamenById(id: string): void {
    this.dataService.callService({
      method: 'DELETE',
      url: `${this.examenSettingApi}/${id}`
    });
  }
}