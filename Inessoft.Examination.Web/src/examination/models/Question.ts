import { BaseEntity } from "./BaseEntity";

export class Question extends BaseEntity{

    questionText : string;
    isMultipleAnswer : boolean;
    articleNumber : number;
    categoryName : string;
    categoryId : string;
    languageId : string;

    isNew = (): boolean => {
        return !!this.id || this.id == '';
    }
}