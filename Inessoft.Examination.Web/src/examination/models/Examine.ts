import { ExaminationCategory } from "./ExaminationCategory";
import { Chairman } from "./Сhairman";
import { BaseEntity } from "./BaseEntity";

export class Examine extends BaseEntity{

    constructor(){
        super();
        this.chairman = new Chairman();
    }

    name : string;
    startDate : Date;
    endDate : Date;

    duration : number;
    questionCount : number;
    requiredCountOfCorrectAnswers : number;
    
    pointFive : number;
    pointFour : number;
    pointThree : number;

    chairman : Chairman;
}