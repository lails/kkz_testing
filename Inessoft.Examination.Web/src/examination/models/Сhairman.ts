import { BaseEntity } from "./BaseEntity";

export class Chairman extends BaseEntity{
    fio: string;
    iin: string;
}