import { QuestionTranslate } from "./QuestionTranslate";
import { AnswerTranslate } from "./AnswerTranslate";

export class QuestoinTranslateDto
{
    questionTranslates : QuestionTranslate[];
    answerTranslates : AnswerTranslate[][];
}