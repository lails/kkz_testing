import { BaseEntity } from "./BaseEntity";

export class CategoryTranslate extends BaseEntity
{
    categoryId : string;
    languageCode : string;
    text : string;
    translateText: string;
}