import { BaseEntity } from "./BaseEntity";

export class Category extends BaseEntity{
    parentId:string;
    name:string;
    questionsCount:number;
    isCanAddQuestions : boolean;
}