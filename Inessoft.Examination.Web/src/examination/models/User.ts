import { Role } from "./Role";
import { BaseEntity } from "./BaseEntity";
import { Category } from "./Category";

export class User extends BaseEntity
{
    constructor(){
        super();
        this.role = new Role();
        this.category = new Category();
    }
    firstName : string;
    middleName : string;
    secondName : string;
    position : string;
    inn  : string;
    birthdate  : Date;
    address  : string;
    phoneNumber : string;
    email : string;
    isActive  : boolean;
    isDeleted  : boolean;
    role  : Role;
    category : Category;
}