import { BaseEntity } from "./BaseEntity";

export class ExaminationCategory extends BaseEntity
{
    categoryId: string;
    categoryName: string;
    fullCategoryQuestionCount: number;
    categoryQuestionCount: number;
}