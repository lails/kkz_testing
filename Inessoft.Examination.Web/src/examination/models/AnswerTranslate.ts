import { BaseEntity } from "./BaseEntity";
import { Answer } from "./Answer";

export class AnswerTranslate extends BaseEntity
{   
    questionId : string;
    answer : Answer;
    answerId : string;
    languageCode : string;
    text : string;
    translateText: string;
}