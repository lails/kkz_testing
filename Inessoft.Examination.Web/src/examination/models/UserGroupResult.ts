export class UserGroupResult
{
     examinerFio : string;
     examenName : string;
     userGroupExamStatusName : string;
     testResultEvaluationSummary : number;
     testResultPoint : number;
     testQuestionTotalCount : number;
}