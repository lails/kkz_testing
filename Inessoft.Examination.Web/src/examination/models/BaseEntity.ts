export class BaseEntity
{    
    id : string = "00000000-0000-0000-0000-000000000000";

    isNew = (): boolean => {
        return !!this.id || this.id == '' || this.id == '00000000-0000-0000-0000-000000000000';
    }
}