import { ExaminationCategory } from "./ExaminationCategory";
import { Chairman } from "./Сhairman";
import { BaseEntity } from "./BaseEntity";
import { Category } from "./Category";

export class ExamineSetting extends BaseEntity{

    constructor(){
        super();
        this.chairman = new Chairman();
        this.examenSettingParentCategory = new Category();
    }

    name : string;
    startDate : Date;
    endDate : Date;

    duration : number;
    questionCount : number;
    requiredCountOfCorrectAnswers : number;
    
    pointFive : number;
    pointFour : number;
    pointThree : number;

    chairman : Chairman;
    examenSettingParentCategory : Category;
}