export class Answer{
    id : string;
    isRight : boolean;
    questionId : string;
    answerText : string;
}