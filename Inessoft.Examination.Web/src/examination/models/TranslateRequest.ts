export class TranslateRequest
{    
    fromLanguageCode : string;
    toLanguageCode : string;
    isOnlyNotTranslated : boolean;
    takeCount : number;

    categoryId : string;
    articleNumber : number;
    questionText : string;
}