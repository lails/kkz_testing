import { BaseEntity } from "./BaseEntity";

export class QuestionTranslate extends BaseEntity
{
    questionId : string;
    languageCode : string;
    text : string;
    translateText: string;
}