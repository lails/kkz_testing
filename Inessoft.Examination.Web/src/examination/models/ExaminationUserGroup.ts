import { Examine } from "./Examine";

export class ExaminationUserGroup
{
    examinationName : string;
    examinationDateComplite : Date;
    isComputerTestFinished : boolean;
    questionCount : number
}