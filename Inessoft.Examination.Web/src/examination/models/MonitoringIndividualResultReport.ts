import { MonitoringIndividualResultReportCategories } from './MonitoringIndividualResultReportCategories';

export class MonitoringIndividualResultReport {
    userName: string;
    examinationPassedDate: Date
    categories: Array<MonitoringIndividualResultReportCategories>;
    examinationPoints: number;
    oralExaminationPoints: number;
    examinationResult: string;
    chairmanName: string

    constructor() {
        this.categories = new Array<MonitoringIndividualResultReportCategories>();
    }
}