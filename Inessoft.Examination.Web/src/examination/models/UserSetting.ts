import { BaseEntity } from "./BaseEntity";
import { Category } from "./Category";

export class UserSetting extends BaseEntity
{
    constructor(){
        super();
        this.category = new Category();
    }
    userId : string;    
    category : Category;
}