import { Injectable } from '@angular/core';
import { DataService } from '../../shared/http-extensions/data-service';
import { ListResponse } from 'right-angled';
import { Examine } from '../models/Examine';

@Injectable()
export class MonitoringService {
    
    public constructor(private dataService: DataService) { }

    monitoringApi: string = "api/monitoring";

    getActiveExaminations(): Promise<Array<Examine>> {
        return this.dataService.callService({
            method: 'GET',
            url: `${this.monitoringApi}/examinations`
      });  
    }

    getExaminationObservedUsers(request: any): Promise<ListResponse<any>> {
        return this.dataService.callService({
            data: request,
            method: 'POST',
            url: `${this.monitoringApi}/observed-users`
        });
    }

    getExaminationResultType(): Promise<Array<any>> {
        return this.dataService.callService({
            method: 'GET',
            url: `${this.monitoringApi}/result-types`
        });
    }

    updateExaminationObservedUser(request: any): Promise<any> {
        return this.dataService.callService({
            data: request,
            method: 'PUT',
            url: `${this.monitoringApi}/update`
        });
    }

    getExaminationIndividualResultReport(request: any): Promise<any> {
        return this.dataService.callService({
            data: request,
            method: 'POST',
            url: `${this.monitoringApi}/individual-result-report`
        });
    }
}