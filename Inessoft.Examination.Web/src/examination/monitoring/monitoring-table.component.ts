import { Component, OnInit, Output, EventEmitter, ViewChild, Input, SimpleChanges, forwardRef} from '@angular/core';
import { RTSelectionEvent, RTSelectionService, SelectionAreaDirective, ListResponse, RTFilterTarget, filter, FilterConfig } from 'right-angled';
import { PagedPager, List } from 'right-angled';
import { SelectItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { Examine } from '../models/Examine';
import { NotificationService, PrintServce } from 'shared/services';
import { MonitoringService } from './monitoring.service';
import { MonitoringIndividualResultReport, MonitoringIndividualResultReportCategories } from '../models';
import { SecurityService } from 'shared/authentication';

List.settings.keepRecordsOnLoad = true;

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => MonitoringTableComponent) }],
    selector : 'monitoring-table',
    templateUrl : './monitoring-table.component.html'
})
export class MonitoringTableComponent implements OnInit {

    @filter(<FilterConfig>{ parameterName: 'examinationId' })
    public examinationId: string = null;

    pageSize:number  = 10;
    activeExaminations: SelectItem[] = [];
    resultTypes: SelectItem[] = [];

    individualReportData: MonitoringIndividualResultReport = new MonitoringIndividualResultReport();

    constructor(public monitoringService: MonitoringService,       
                private securityService: SecurityService, 
                public notificationService: NotificationService, public printServce: PrintServce) {
        setInterval(() => {
            var el = document.getElementById("aplyMonitoringFilter");
            if(el)
            {
                el.click();
            }            
        }, 60000);
     }


    ngOnInit(): void {
        this.getActiveExaminations();
        this.getExaminationResultType();
    }
    
    getActiveExaminations(): void {
        this.monitoringService.getActiveExaminations()
        .then(exams =>{
            if(exams)
            {
                this.activeExaminations = exams
                .map(exam => {
                    return  {
                        label : exam.name,
                        value : exam.id
                    };
                });
            }
            this.activeExaminations.splice(0,0,{label:"",value:""});
        });
    }

    getExaminationResultType(): void {
        this.monitoringService.getExaminationResultType()
        .then(types => {
            if(types){
                this.resultTypes = types
                .map(type => {
                    return  {
                        label : type.name,
                        value : type.id
                    };
                });
            }

            this.resultTypes.splice(0,0,{label:"",value:""});
        });
    }

    getExaminationObservedUsers = (request: any): Promise<ListResponse<any>> => {4
        return this.monitoringService.getExaminationObservedUsers(request);
    }

    saveUserGroup(observedUser: any, observedUsersList: any): void {
        this.monitoringService.updateExaminationObservedUser(observedUser)
            .then(result => {
                this.notificationService.updated();
                observedUsersList.reloadData();
            });
    }

    toggleEditRow(observedUser: any, observedUsersList: Array<any>): void {
        for (let user of observedUsersList) {
            if(user.id !== observedUser.id) {
                user.isSelected = false;
            }
        }

        if(observedUser.isSelected) {
            observedUser.isSelected = false;
            localStorage.setItem("monitoring_opened_row", "");
        } else {
            observedUser.isSelected = true;
            localStorage.setItem("monitoring_opened_row", observedUser.id);
        }
    }

    getMonitoringOpenedRowState(): string {
        return localStorage.getItem("monitoring_opened_row");
    }

    printIndividualReport(userId: string): void {
        var reportRequest = { examinationId: this.examinationId, userGroupId: userId };
        this.monitoringService.getExaminationIndividualResultReport(reportRequest)
            .then(reportData => {
                if(reportData) {
                    this.individualReportData = reportData;
                } else {
                    this.individualReportData = new MonitoringIndividualResultReport();
                }
            })
            .then(() => {
                setTimeout  (() => {
                    this.printServce.print("indevidual-result-report");
                  }, 2000);
            });
    }
    
    hasAdministratorAccess(): boolean {
        return this.securityService.hasAdministratorAccess();
    }
}