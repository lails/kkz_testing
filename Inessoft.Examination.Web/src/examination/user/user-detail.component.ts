import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { User } from "../models/User";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "shared/services";
import { UserService } from "./user.service";
import { Role } from "../models/Role";
import { SelectItem } from "primeng/api";
import { Category } from "../models/Category";
import { CategoryService } from "../category/category.service";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    selector : 'user-detail',
    templateUrl : './user-detail.component.html'
})

export class UserDetailComponent implements OnInit{
    
    user : User = new User();
    userForm : FormGroup;

    userRoleItems : SelectItem[];
    categoryItems : SelectItem[];
    maxDateTime : Date = new Date();
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private fb: FormBuilder,
        private userService: UserService,
        private categoryService : CategoryService){
        this.createForm();
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        
        if(id !== "0")
        {
            this.setFormValues();
        }

        this.getuserRoles();
        this.getCategories();

        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/users", "Users", false));
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem(`/user/${id}`,"",true));        
    }

    createForm(){
        this.userForm = this.fb.group({
            id: this.user.id,
            firstName: [null, Validators.required ],
            middleName: [null, Validators.required ],
            secondName: [null, Validators.required ],
            position: [null, Validators.required ],
            inn: [null, Validators.required ],
            birthdate: null,
            address: '',
            phoneNumber: '',
            email: '',
            isActive: true,
            isDeleted : false,
            roleId : [null, Validators.required ],
            categoryId : null
        });
    }

    setFormValues(){        
        const id = this.route.snapshot.paramMap.get('id');
        
        this.userService.getUserById(id)
        .then((user:User)=>
        {                
            this.user = user;
            this.user.birthdate  = new Date(user.birthdate);
            
            if(!this.user.role)
            {
                this.user.role = new Role();
            }

            this.userService.getUserSettingQueryByUserId(id)
            .then(userSetting=>{
                if(userSetting && userSetting.category)
                {
                    this.user.category = userSetting.category;
                }else{
                    this.user.category = new Category();
                }
    
                this.userForm.setValue({
                    id: this.user.id,
                    firstName: this.user.firstName,
                    middleName: this.user.middleName,
                    secondName: this.user.secondName,
                    position: this.user.position,
                    inn: this.user.inn,
                    birthdate: this.user.birthdate,
                    address: this.user.address,
                    phoneNumber: this.user.phoneNumber,
                    email: this.user.email,
                    isActive: this.user.isActive,
                    isDeleted : this.user.isDeleted,
                    roleId : this.user.role.id,
                    categoryId : this.user.category.id || null
                });
            })
        });        
    }

    public validatErrors(controlName:any) : boolean {
        
        return this.userForm.controls[controlName].errors == null;
    }

    getuserRoles(): any {
        this.userService.getUserRoles().then(roles=>{
            this.userRoleItems = roles.map(role=> {
                return  {
                    label : role.name,
                    value : role.id
                };
            });            

            this.userRoleItems.splice(0,0,{label:"",value:""});
        })
    }

    getCategories(): any {
        this.categoryService.getCategories().then(categories=>{
            this.categoryItems = categories.items
            .filter(r=>r.parentId == null)
            .map(category=> {
                return  {
                    label : category.name,
                    value : category.id
                };
            });            

            this.categoryItems.splice(0,0,{label:"",value:""});
        })
    }

    onSubmit() {
        var request = this.userForm.value;
            request.role = new Role();
            request.role.id = this.userForm.value.roleId;
            request.category = new Category();
            request.category.id = this.userForm.value.categoryId;

        if(this.userForm.valid)
        {

            if(this.user.isNew && this.user.isNew() == true)
            {
                this.userService.createUser(request).then(result=> {
                    debugger;
                    this.router.navigate(['/user', result])
                    .then(r=>{
                        this.setFormValues();
                        this.notificationService.saved();
                    });
                    
                    //window.location.reload();
                });
            } 
            else {
                this.userService.updateUser(request).then(result=> {
                    this.setFormValues();
                    this.notificationService.updated();
                    //window.location.reload();
                });
            }
        }else{
            this.notificationService.formNotValid();
        }
    }
}