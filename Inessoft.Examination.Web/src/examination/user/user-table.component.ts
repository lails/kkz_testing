import { Component, forwardRef, OnInit } from "@angular/core";
import { User } from "../models/User";
import { ListResponse, filter, FilterConfig, PagedPager, RTFilterTarget } from "right-angled";
import { ExamenService } from "../examen/examen.service";
import { UserService } from "./user.service";
import { FileService } from "../shared/file.service";
import { NotificationService } from "shared/services";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => UserTableComponent) }],
    selector : 'user-table',
    templateUrl : './user-table.component.html'
})

export class UserTableComponent implements OnInit{

    pageSize : number = 10;    
    canUploadUsers : boolean = true;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    //#region ФИЛЬТР
        @filter(<FilterConfig>{ parameterName: 'middleName' })
        public middleName: string = null;
        @filter(<FilterConfig>{ parameterName: 'inn' })
        public inn: string = null;
    //#endregion

    ngOnInit(): void {
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/users", "Users", true));
    }

    constructor(private userService: UserService,
                private fileService: FileService,
                private notificationService: NotificationService,){}
    
    getUsersPagedList = (request): Promise<ListResponse<User>> => {
        return this.userService.getUsersPagedList(request);
    }

    downloadUserDtoTemplate(){
        window.open("api/files/getUserDtoTemplate");
    }
    
    onBeforeUploadUsers(e){
        this.canUploadUsers = false;
    }

    onUpload(response){
        this.notificationService.info(`Загрузка выполнена`);
        this.canUploadUsers = true;
    }

    onUserUploadError(error){
        this.canUploadUsers = true;
    }
}