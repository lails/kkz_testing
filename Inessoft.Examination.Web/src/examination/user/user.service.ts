import { Injectable } from '@angular/core';
import { DataService } from '../../shared/http-extensions/data-service';
import { User } from '../models/User';
import { ListResponse } from 'right-angled';
import { Role } from '../models/Role';
import { UserSetting } from '../models/UserSetting';

@Injectable()
export class UserService {
  
  public constructor(private dataService: DataService) { }

  usersApi: string = "api/users";

  getUsersPagedList(request:any): Promise<ListResponse<User>> {
    return this.dataService.callService({
      data: request,
      method: 'POST',
      url: `${this.usersApi}`
    });
  }

  //Используется для отображения в экзаменах
  getUsers(request:any): Promise<ListResponse<User>> {
      return this.dataService.callService({
        data: request,
        method: 'POST',
        url: `${this.usersApi}/examination-users`
    });
  }

  getUserById(id:string): Promise<User>{
    return this.dataService.callService({      
      method: 'GET',
      url: `${this.usersApi}/${id}`
    })
  }

  getUserSettingQueryByUserId(id:string): Promise<UserSetting>{
    return this.dataService.callService({      
      method: 'GET',
      url: `${this.usersApi}/${id}/userSetting`
    })
  }

  createUser(user: User): Promise<string> {
    return this.dataService.callService({
      data: user,
      method: 'POST',
      url: `${this.usersApi}/create`
    });
  }

  updateUser(user: User): Promise<string> {
    return this.dataService.callService({
      data: user,
      method: 'PUT',
      url: `${this.usersApi}/update`
    });
  }
  
  getUserRoles(): Promise<Role[]>{
    return this.dataService.callService({      
      method: 'GET',
      url: `${this.usersApi}/roles`
    })
  }
}