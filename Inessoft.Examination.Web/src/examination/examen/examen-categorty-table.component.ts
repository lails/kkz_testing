import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, forwardRef} from '@angular/core';
import { Category } from '../models/Category';
import { Observable } from 'rxjs/Observable';
import { RTSelectionEvent, RTSelectionService, SelectionAreaDirective, ListResponse, RTFilterTarget, filter, FilterConfig } from 'right-angled';
import { promise } from 'selenium-webdriver';
import { PagedPager } from 'right-angled';
import { Question } from '../models/Question';
import { Answer } from '../models/Answer';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../category/category.service';
import { SelectItem } from 'primeng/api';
import { ExaminationCategory } from '../models/ExaminationCategory';
import { ExamenService } from './examen.service';
import { ExamenSettingService } from '../dictionaries/examen-setting/examen-setting.service';

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => ExamenCategortyTableComponent) }],
    selector : 'examen-categorty-table',
    templateUrl : './examen-categorty-table.component.html'
})

export class ExamenCategortyTableComponent implements OnInit{

    @Input() questionCount : number = 0;
    @Input() isCategorySetting : boolean = false;    

    
    categorySelectItems : SelectItem[];
    categories : Category[];
        
    selectedCategory : Category = new Category();
    topcategories : Category[];
    treeCategories : Category[];
    headCategorySelected: boolean;

    @Output() onExaminationCategoriesChange = new EventEmitter<ExaminationCategory[]>();  
    @Output() onSelectCategoryOut = new EventEmitter<Category>();

    @filter(<FilterConfig>{ parameterName: 'examenSettingId' })
    @Input() examenSettingId : string;

    constructor(private route : ActivatedRoute,
                private categoryService : CategoryService,
                private examenService : ExamenService,
                private examenSettingService : ExamenSettingService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager){}
    
    ngOnInit(){
        this.getTreeCategories();
    }

    private getCategories() : void {
        this.categoryService.getCategories()
            .then(categories=> {
                if(categories.items)
                {
                    this.categorySelectItems = categories.items
                    .filter(r=>r.parentId == this.selectedCategory.id && r.isCanAddQuestions == true)
                    .map(category => {  
                        return  {
                            label : category.name,
                            value : category.id
                            };
                    });

                    this.categories = categories.items;
                    
                    this.categorySelectItems.splice(0,0,{label:"",value:""});
                }
            });
    }

    getTreeCategories() : void {
        this.categoryService.getCategories().then(categories=>{
        
            if(categories.items)
            {
                this.treeCategories = categories.items;
                this.topcategories = this.treeCategories.filter(category => !category.parentId);
            }
        });
    }

    onSelect(category : Category, isHead) : void {
        this.selectedCategory = category;
        this.getCategories();

        if(isHead)
        {
            this.headCategorySelected = !this.headCategorySelected;
        }
        this.onSelectCategoryOut.emit(category);
    }

    appFilter(){
        alert("Not Implemented")
        
    }

    getCategorySettings = (request): Promise<ExaminationCategory[]> => {
        var id = this.route.snapshot.paramMap.get('id');

        if(request.examenSettingId)
        {
            this.isCategorySetting = true;
            if(request.examenSettingId)
            {
                id = request.examenSettingId;
            }
        }
        
        if(this.isCategorySetting){
            if(id !== "0")
            {
                return this.examenSettingService.getExamenCategoriesByExamenId(id)
                .then(r=>{
                    //если запрошены настройки из экзамена
                    if(request.examenSettingId)
                    {

                        var items =  r;
                        items = items.map(ec => {
                            var newEc = new ExaminationCategory();
                            newEc.categoryId = ec.categoryId;
                            newEc.categoryName = ec.categoryName;
                            newEc.categoryQuestionCount = ec.categoryQuestionCount;
                            newEc.fullCategoryQuestionCount = ec.fullCategoryQuestionCount;                            
                            return newEc;
                        })
                       
                        this.onExaminationCategoriesChange.emit(items || []);
                     
                        return r;
                    }
                    this.onExaminationCategoriesChange.emit(r || [])
                    return r;
                });
            }
            
            return this.examenSettingService.getExamenCategoriesByExamenId(null)
            .then(r=>{
                this.onExaminationCategoriesChange.emit(r || [])
                return r;
            });
        }else{

            if(id !== "0")
            {                
                return this.examenService.getExamenCategoriesByExamenId(id)
                .then(r=>{
                    this.onExaminationCategoriesChange.emit(r || [])
                    return r;
                });
            }
            
            return this.examenService.getExamenCategoriesByExamenId(null)
            .then(r=>{
                this.onExaminationCategoriesChange.emit(r || [])
                return r;
            });
        }
    }
    
    onPercentchange(percent:number, examinationCategory:ExaminationCategory)
    {
        examinationCategory.categoryQuestionCount = parseInt((percent * this.questionCount / 100).toString());
    }

    percentSum(list:any) : number{
        var percentSum :number = 0;
        if(list && list.items.length > 0)
        {
            list.items.forEach( (item :ExaminationCategory)=> 
            {
                if(item.categoryQuestionCount)
                {
                    var res = 100*item.categoryQuestionCount/this.questionCount;
                    percentSum += res;
                }
            });
        }
        this.onExaminationCategory(list);

        return percentSum
    }

    questiontSum(list:any) : number{
        var questiontSum :number = 0;
        if(list && list.items.length > 0)
        {
            list.items.forEach( (item :ExaminationCategory)=> 
            {
                if(item.categoryQuestionCount)
                {
                    questiontSum += item.categoryQuestionCount;
                }
            });
        }
        this.onExaminationCategory(list);
        
        return questiontSum
    }

    changeCategoty(selectedOption:any, examinationCategory:ExaminationCategory) : void
    {
        examinationCategory.categoryId = selectedOption.value;
        examinationCategory.categoryName = selectedOption.label;
        examinationCategory.categoryQuestionCount = 0;
        
        var category = this.categories.filter(r=>r.id == selectedOption.value)[0]

        examinationCategory.fullCategoryQuestionCount = category.questionsCount;
    }

    deleteItem(list : any, item : Category)
    {
        var index = list.items.indexOf(item);
        if (index > -1) {
            list.items.splice(index, 1);
        }
        
        this.onExaminationCategoriesChange.emit(list.items);
    }

    addItem(list: any){
        list.items.splice(0,0,new Answer());
        
        this.onExaminationCategoriesChange.emit(list.items);
    }
    
    onExaminationCategory(list:any){
        this.onExaminationCategoriesChange.emit(list.items);
    }
 }