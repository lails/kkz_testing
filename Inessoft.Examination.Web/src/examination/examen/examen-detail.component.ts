import { Component, OnInit } from "@angular/core";
import { Examine } from "../models/Examine";
import { ActivatedRoute, Router } from "@angular/router";
import { ExamenService } from "./examen.service";
import { Chairman } from "../models/\u0421hairman";
import { SelectItem } from "primeng/api";
import { CategoryService } from "../category/category.service";
import { ListResponse } from "right-angled";
import { ExaminationCategory } from "../models/ExaminationCategory";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { User } from "../models/User";
import { UserService } from "../user/user.service";
import { NotificationService } from "shared/services";
import { Category } from "../models/Category";
import { ExamineSetting } from "../models/ExamineSetting";
import { ExamenSettingService } from "../dictionaries/examen-setting/examen-setting.service";
import { BaseEntity } from "../models/BaseEntity";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    selector: "examen-detail",
    templateUrl: "./examen-detail.component.html"
})
export class ExamenDetailComponent implements OnInit {
    
    examine : Examine = new Examine();

    minDateTime : Date = new Date();
    chairmans : SelectItem[];
    categories : SelectItem[];
    examinationCategories : ExaminationCategory[];
    examinationSettings : ExamineSetting[];
    examinationSettingItems : SelectItem[];
    examinationSettingId : string;

    noSelectedUsers : User[] = [];
    selectedUsers : User[] = [];
    failureUserInfos : string [] = [];
    canUploadUsers : boolean = true;

    examenForm : FormGroup;

    categoryIsValid : boolean
    usersIsValid : boolean;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];


    public get questionCount() : number {
        if(this.examenForm.controls && this.examenForm.controls.questionCount.value)
        {
            return this.examenForm.controls.questionCount.value;
        }
        return 0;
    }

    constructor (private route: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationService,
                private examenService : ExamenService,
                private examenSettingService : ExamenSettingService,
                private categoryService : CategoryService,
                private userService : UserService,
                private fb: FormBuilder
            ){
                this.createForm();
            }

    
    //#region Работа с формой

    createForm() 
    {
        this.examenForm = this.fb.group({
            id: this.examine.id,
            name: [null, Validators.required ],
            startDate: [null, Validators.required ],
            endDate: [null, Validators.required ],
            duration: [null, Validators.required ],
            questionCount: [null, Validators.required ],
            requiredCountOfCorrectAnswers: [null, Validators.required ],
            pointfive: [null, Validators.required ],
            pointfour: [null, Validators.required ],
            pointthree: [null, Validators.required ],
            chairmanId: [null, Validators.required ],
            //chairman: [new Chairman(), Validators.required ],
            
            //используется для фильтра юзеров на форме
            filterUserMiddleName : '',
            filterUserIin : '',
            //используется для установки настройки экзамена
            examinationSetting: '',
        });
    }

    setFormValues(){

        const id = this.route.snapshot.paramMap.get('id');
        
        if(id !== "0")
        {
            this.examenService.getExamenById(id)
            .then((examen:Examine)=>
            {
                var timezoneOffset = new Date().getTimezoneOffset() / 60 * -1;
                
                this.examine = examen;
                var startdate = new Date(examen.startDate);
                startdate.setHours(timezoneOffset);
                this.examine.startDate = startdate;

                var endDate = new Date(examen.endDate);
                endDate.setHours(timezoneOffset);
                this.examine.endDate =endDate;

                this.examenForm.setValue({
                    id: this.examine.id,
                    name:  this.examine.name,
                    startDate:  this.examine.startDate,
                    endDate:  this.examine.endDate,
                    duration:  this.examine.duration,
                    questionCount:  this.examine.questionCount,
                    requiredCountOfCorrectAnswers:  this.examine.requiredCountOfCorrectAnswers,
                    pointfive:  this.examine.pointFive,
                    pointfour:  this.examine.pointFour,
                    pointthree:  this.examine.pointThree,
                    chairmanId:  this.examine.chairman.id,
                    
                    //используется для фильтра юзеров на форме
                    filterUserMiddleName : '',
                    filterUserIin : '',
                    //используется для установки настройки экзамена
                    examinationSetting: '',
                })
            });

            this.getUserExaminationUserGroup(id);
        }
    }

    //----------------------------------------Валидация

    public validatErrors(controlName:any) : boolean {
        
        return this.examenForm.controls[controlName].errors == null;
    }

    //#endregion



    ngOnInit(): void {
        this.setFormValues();        
        this.getExaminationChairmans();
        this.getCategories();
        this.getExamenSettings();
        
        const id = this.route.snapshot.paramMap.get('id');
        if(id == "0") {
            this.getUsers();
        }

        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/examentantions", "ExamExams",false));
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem(`/examen/${id}`,"",true));
    }

    private getExaminationChairmans() : void {
        this.examenService.getExaminationChairmans()
        .then(chairmans =>{
            this.chairmans = chairmans.map(chairman => {
                return  {
                    label : `${chairman.secondName} ${chairman.firstName} ${chairman.middleName}`,
                    value : chairman.id
                };
            });

            this.chairmans.splice(0,0,{label:"",value:""});
        });
    }

    private getCategories() : void {
        this.categoryService.getCategories()
        .then(categories=>{
            if(categories.items){
                this.categories = categories.items.filter(r=>r.parentId !== null).map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
            }
        });
    }

    getUsers () : void
    {
        //TODO+: Исключить selectedUsers
        var request = {
            iin : this.examenForm.controls.filterUserIin.value,
            middleName : this.examenForm.controls.filterUserMiddleName.value,
            excludeUserIds : this.selectedUsers.map(r=>r.id)
        };

        this.userService.getUsers(request)
        .then(users=>{
            this.noSelectedUsers = users.items;
        });
        
    }

    getUserExaminationUserGroup(examinationId) {
        this.examenService.getExaminationUserGroup(examinationId)
        .then(users => {
            this.selectedUsers = users;
            this.getUsers();
        });
    }

    getExamenSettings(){
        this.examenSettingService.getExamens(null)
        .then(r=>{
            this.examinationSettings = r.items;
            this.examinationSettingItems = r.items.map(s=>{
                return  {
                    label : s.name,
                    value : s.id
                };
            });

            this.examinationSettingItems.splice(0,0,{label:"",value:""});
        });
    }
    
    onexaminationCategoriesChange(examinationCategories:ExaminationCategory[])
    {
        if(this.examine.isNew && this.examine.isNew())
        {
            var emprtyGuid = new BaseEntity().id
            this.examinationCategories = examinationCategories.map(r=>{
                r.id = emprtyGuid;
                return r;
            });
        }
        else{
            this.examinationCategories = examinationCategories;
        }
        
    }

    aplyUserFilter()
    {
        this.getUsers();
    }

    aplyExamenSetting()
    {        
        this.examinationSettingId = ""
        if(this.examenForm.controls.examinationSetting.value)
        {
            this.examenSettingService.getExamenById(this.examenForm.controls.examinationSetting.value)
                .then(setting => {
                    this.examenForm.controls.chairmanId.setValue(setting.chairman.id);
                    this.examenForm.controls.duration.setValue(setting.duration);
                    this.examenForm.controls.questionCount.setValue(setting.questionCount);
                    this.examenForm.controls.requiredCountOfCorrectAnswers.setValue(setting.requiredCountOfCorrectAnswers);
                    this.examenForm.controls.pointfive.setValue(setting.pointFive);
                    this.examenForm.controls.pointfour.setValue(setting.pointFour);
                    this.examenForm.controls.pointthree.setValue(setting.pointThree);
                    this.examinationSettingId = this.examenForm.controls.examinationSetting.value;
                })
        }
    }
    
    validateCategory()
    {
        var questionSum = 0;        
        this.categoryIsValid = true;

        this.examinationCategories
        .forEach(r => {
            questionSum += r.categoryQuestionCount;
            if(r.categoryQuestionCount > r.fullCategoryQuestionCount)
            {
                this.notificationService.warningWithTranslation('SpecMessageSetRightDataForCategoriesTable');
                this.categoryIsValid = false;
                return;
            }
            //examinationCategory.categoryQuestionCount > examinationCategory.fullCategoryQuestionCount ? 'invalid-value'
        });

        if(!this.categoryIsValid)
        {
            return;
        }

        if(this.examenForm.controls.questionCount.value !== questionSum)
        {
            this.notificationService.warning(`Несоотвествие количество вопросов, использовано ${questionSum} из возможных ${this.examenForm.controls.questionCount.value}`);
            this.categoryIsValid = false;
        }
    }

    validateUsers()
    {
        if(!this.selectedUsers || this.selectedUsers.length < 1)
        {
            //TODO валидация юзеров на данный момент не определена.
        }

        this.usersIsValid = true;
    }

    downloadUserDtoTemplate(){
        window.open("api/files/getUserDtoTemplate");
    }

    onBeforeUploadUsers(e){
        debugger;
        this.canUploadUsers = false;
    }

    onUpload(response){        
        var responseJson = JSON.parse(response.xhr.response);
        var failureUserInfos = responseJson.failureUsers;
        var uploadedUsers = responseJson.uploadedUsers;

        this.failureUserInfos = failureUserInfos;

        uploadedUsers.forEach(uploadedUser => {
            let user = new User();
            user.id = uploadedUser.id;
            user.inn = uploadedUser.inn;
            user.firstName = uploadedUser.firstName;
            user.middleName = uploadedUser.middleName;
            user.secondName = uploadedUser.secondName;
            
            this.selectedUsers.push(user);

            var key = this.noSelectedUsers.find(r=>r.id == uploadedUser.id);
            var index = this.noSelectedUsers.indexOf(key, 0);
            if (index > -1) {
                this.noSelectedUsers.splice(index, 1);
            }
        });

        this.notificationService.info(`Загрузка выполнена`);
        
        this.canUploadUsers = true;
    }

    onUserUploadError(error){
        this.notificationService.error(`Ошибка при загрузке пользователей`);
        this.canUploadUsers = true;
    }

    onSubmit() {
        var request = this.examenForm.value;
            request.chairman = new Chairman(); 
            request.chairman.id = this.examenForm.value.chairmanId;
            var chairmanSelected = this.chairmans.filter(r=>r.value == this.examenForm.value.chairmanId);
            if(chairmanSelected && chairmanSelected.length == 1)
            {
                request.chairman.fio = chairmanSelected[0].label;
            }
            
            request.examinationCategories = this.examinationCategories;
            request.examinationUserGroups = this.selectedUsers;

        if(this.examenForm.valid)
        {
            this.validateCategory();
            if(this.categoryIsValid == false)
            {
                return;
            }
            this.validateUsers();

            if(this.examine.isNew && this.examine.isNew() == true)
            {
                this.examenService.saveExamen(request).then(result=> {
                    this.router.navigate(['/examen', result]).then(r=>{
                        this.setFormValues();
                        this.notificationService.saved();
                    });
                });
            } 
            else {
                this.examenService.updateExamen(request).then(result=> {                    
                    this.notificationService.updated();
                });
            }
        }else{
            this.notificationService.formNotValid();
        }
    }


}