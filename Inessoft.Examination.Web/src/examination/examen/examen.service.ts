import { Injectable } from '@angular/core';
import { DataService } from '../../shared/http-extensions/data-service';
import { ListResponse } from 'right-angled';
import { Examine } from '../models/Examine';
import { Chairman } from '../models/\u0421hairman';
import { ExaminationCategory } from '../models/ExaminationCategory';
import { User } from '../models/User';

@Injectable()

export class ExamenService {
  
  public constructor(private dataService: DataService) { }

  examinationApi: string = "api/examinations";

  getExamens(request:any): Promise<ListResponse<Examine>> {
      return this.dataService.callService({
        data: request,
        method: 'POST',
        url: `${this.examinationApi}`
    });  
  }

  getExamenById(id:string): Promise<Examine> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.examinationApi}/${id}`
    });
  }

  getExaminationChairmans(): Promise<User[]> {
      return this.dataService.callService({        
        method: 'GET',
        url: `${this.examinationApi}/chairmans`
    });  
  }
  
  getExamenCategoriesByExamenId(examenId:string): Promise<ExaminationCategory[]> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.examinationApi}/${examenId}/categories`
    });  
  }

  getExaminationUserGroup(examenId:string): Promise<Array<User>> {
    return this.dataService.callService({
      method: 'GET',
      url: `${this.examinationApi}/${examenId}/users`
    });  
  }

  saveExamen(question: any): Promise<string> {
    return this.dataService.callService({
      data: question,
      method: 'POST',
      url: `${this.examinationApi}/create`
    });
  }

  updateExamen(question: any): Promise<string> {
    return this.dataService.callService({
      data: question,
      method: 'POST',
      url: `${this.examinationApi}/update`
    });
  }
  
  deleteExamenById(id: string): void {
    this.dataService.callService({
      method: 'DELETE',
      url: `${this.examinationApi}/${id}`
    });
  }
}