import { Component, SimpleChanges, ViewChild, Input, forwardRef, OnInit } from "@angular/core";
import { filter, FilterConfig, RTFilterTarget, RTSelectionService, PagedPager, ListResponse, SelectionAreaDirective } from "right-angled";
import { ExamenService } from "./examen.service";
import { Examine } from "../models/Examine";
import { Answer } from "../models/Answer";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => ExamenTableComponent) }],
    selector : "examen-table",
    templateUrl : "./examen-table.component.html"
})

export class ExamenTableComponent implements OnInit{

    pageSize:number  = 10;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    //#region   
    
        @filter(<FilterConfig>{ parameterName: 'categoryId' })
        public categoryId: string = null;
        @filter(<FilterConfig>{ categoryName: 'categoryName' })
        public categoryName: string = null;
        @filter(<FilterConfig>{ categoryName: 'articleNumber' })
        public articleNumber: string = null;
        
    //#endregion

    constructor(private examenService : ExamenService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager){}
                
    ngOnInit(): void {
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/examentantions", "ExamExams", true));
    }

    getExamens = (request): Promise<ListResponse<Examine>> => {
        return this.examenService.getExamens(request);
    }
    
    @ViewChild(SelectionAreaDirective) public selectionArea: SelectionAreaDirective;
    selectedQuestionNames :string;

    public getQuestionGridSelection(): void {
        this.selectedQuestionNames = this.selectionArea.selectionService
                .getSelectedElements()
                .map((c:Examine)=>c.id)
                .join(";");         
        document.getElementById("deletQuestionConfirm").click();
    }

    onDeleteQuestionConfirmed(confirmed:boolean)
    {
        if(confirmed == true)
        {
            //Удалим Вопросы
            //Сделаем резет вопросов
        }
    }
 }
