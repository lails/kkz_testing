import { Component, OnInit} from '@angular/core';
import { NotificationService } from 'shared/services';
import { SettingsService } from './shared/settings.service';
import { SecurityService } from '../shared/authentication';
import { Language } from './models/Language';
import { Message } from 'primeng/api';
import { TranslateService } from '../shared/translate'
import { Router } from '@angular/router';
 
@Component({
    selector: 'application-content',
    templateUrl : './application-content.component.html'
})
export class ApplicationContentComponent implements OnInit{ 

    constructor(private settingSerivce: SettingsService, 
                private securityService: SecurityService, 
                private translateService: TranslateService,            
                private router: Router) {
        this.securityService.getAuthenticatedUser()
        .then(authenticatedUser => {
            
            this.securityService.setCurrentUserData(authenticatedUser);
            
            this.securityService.setCanAccessAdministration(authenticatedUser.canAccessAdministration);
            this.securityService.setCanAccessExamination(authenticatedUser.canAccessExamination);

            this.userName = authenticatedUser.userName;

            if(this.hasEmployeeAccess()){
                var isExamenResult = this.router.url.indexOf("examinationtestingresult") > -1;
                
                if(isExamenResult == false)
                {
                    this.router.navigate(['/examinationtesting/',0]);
                }
            }
        });
    }    
    
    languages : Language[] = [];
    curentLanguage : Language = new Language();

    userName: string = '';

    informationMessage : Message[] = [];
    
    ngOnInit(): void {
        this.settingSerivce.getLanguages().then(languages=>{
            this.languages = languages;
            languages.forEach(lang=>{
                if(lang.isCurentLanguage == true)
                {
                    this.curentLanguage = lang;
                }
            })
        });
    }
    
    setSysemLanguage(languageCode: string){
        this.translateService.use(languageCode);
        window.location.reload();
    }

    logout(): void {
        localStorage.clear();
        window.location.href = "home/logout";
    }

    hasAdministratorAccess(): boolean {
        return this.securityService.hasAdministratorAccess();
    }

    hasEmployeeAccess(): boolean {
        return this.securityService.hasEmployeeAccess();
    }
}