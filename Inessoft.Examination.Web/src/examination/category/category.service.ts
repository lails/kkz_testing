import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Category } from '../models/Category';
import { DataService } from '../../shared/http-extensions/data-service';
import { ListResponse } from 'right-angled';
import { CategoryTranslate } from '../models/CategoryTranslate';

@Injectable()
export class CategoryService {

  categoryApi: string = "api/categories";

  public constructor(private dataService: DataService) { }

  getCategories(): Promise<ListResponse<Category>> {
      return this.dataService.callService({
        method: 'POST',
        url: `${this.categoryApi}`
    });  
  }

  createCategory(category: Category): Promise<string> {
    return this.dataService.callService({
      data: category,
      method: 'POST',
      url: `${this.categoryApi}/create`
    });
  }

  updateCategory(category: Category): Promise<string> {
    return this.dataService.callService({
      data: category,
      method: 'POST',
      url: `${this.categoryApi}/update`
    });
  }
  
  getCategoryById(id:string): Promise<Category> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.categoryApi}/${id}`
      });
  }

  deleteCategoryById(id: string): Promise<string> {
    return this.dataService.callService({
      method: 'DELETE',
      url: `${this.categoryApi}/${id}`
    });
  }
}