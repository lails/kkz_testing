import { Component, OnInit, Input, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { CategoryService } from './category.service';
import { Category } from '../models/Category';
import { SelectItem, Message } from 'primeng/api';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplicationContentComponent } from '../application-content.component';
import { NotificationService } from 'shared/services';

@Component({
    selector: 'category-detail',
    templateUrl : './category-detail.component.html'
})

export class CategoryDetailComponent implements OnInit{
    
    @Output() onSubmited = new EventEmitter<boolean>();
    @Input() set categoryId(categoryId: string) {
        if(categoryId)
        {
            this.getCategoryById(categoryId);
        }
        else{
            this.category = new Category();
            this.setFormValues();
        }
    }
       
    categoryform: FormGroup;
    category : Category = new Category();
    categories : SelectItem[];
    
    constructor(private categoryService : CategoryService,
                private fb: FormBuilder,
                private notificationService: NotificationService){
                    this.createForm();
                }

    //#region Работа с формой

    createForm() 
    {
        this.categoryform = this.fb.group({
            id: this.category.id,
            name: ['', Validators.required ],
            parentId: '',
            isCanAddQuestions: false
        });
    }

    setFormValues(){
        this.categoryform.setValue({
            id: this.category.id,
            name: this.category.name || '',
            parentId: this.category.parentId || '',
            isCanAddQuestions: this.category.isCanAddQuestions || false
        })
    }

    //----------------------------------------Валидация

    public validateErrors(controlName : any) : boolean {        
        return this.categoryform.controls[controlName].errors == null;
    }

    //#endregion

    ngOnInit(): void {
        this.getCategoryValues();
    }

    private getCategoryValues() : void {
        this.categories = [];
        this.categoryService.getCategories()
        .then(categories =>{
            if(categories.items){
                this.categories = categories.items.map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
    
                this.categories.splice(0,0,{label:"",value:""});
            }
        });
    }

    private getCategoryById(id: string) : void {
        if(id == "00000000-0000-0000-0000-000000000000") return;
        
        this.categoryService.getCategoryById(id)
        .then(category =>{
            this.category = category;
            this.setFormValues();
        });
    }
    
    onSubmit() {
        if(this.categoryform.valid)
        {
            if(this.category.isNew)
            {
                this.categoryService.createCategory(this.categoryform.value).then(result=> {
                    this.notificationService.saved();
                    this.categoryId = result;
                    this.onSubmited.emit(true);                    
                    this.getCategoryValues();
                });
            } 
            else {
                this.categoryService.updateCategory(this.categoryform.value).then(result=> {
                    this.notificationService.updated();
                    this.onSubmited.emit(true);
                    this.getCategoryValues();
                });
            }
        }
        else{          
            this.notificationService.notAllFieldsValid();
        }
    }

    onDelete()
    {        
        if(!this.category.isNew || this.category.isNew() == false)
        {
            this.categoryService.deleteCategoryById(this.categoryform.controls.id.value)
            .then(r=>{
                this.notificationService.deleted();
                this.clearForm();
                this.onSubmited.emit(true);
            });        
        }else{
            this.notificationService.deletingIsAnavailable();
        }
    }

    clearForm()
    {
        this.categoryId = null;
    }
}