import { Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import { Category } from '../models/Category';
import { CategoryService } from './category.service';
import { Observable } from 'rxjs/Observable';
import { RTSelectionEvent, RTSelectionService, SelectionAreaDirective, ListResponse } from 'right-angled';
import { promise } from 'selenium-webdriver';
import { PagedPager } from 'right-angled';

@Component({
    selector : 'category',
    templateUrl : './category.component.html'
})
export class CategotyComponent implements OnInit {

    selectedCategory : Category = new Category();
    topcategories : Category[];
    categories : Category[];
    headCategorySelected: boolean;

    constructor(private categoryService : CategoryService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager){}

         
    public getCategoryList = (request): Promise<ListResponse<Category>> => {        
        return this.categoryService.getCategories();
    }

    ngOnInit() : void {
        this.getCategories(null);
    }
    
    getCategories(request:any) : void {
            this.categoryService.getCategories().then(categories=>{
            
            if(categories.items)
            {
                this.categories = categories.items;
                this.topcategories = this.categories.filter(category=>!category.parentId);
            }
        });
    }

    onSelect(category : Category, isHead) : void {
        this.selectedCategory = category;
        if(isHead)
            this.headCategorySelected = !this.headCategorySelected;
    }    
 }