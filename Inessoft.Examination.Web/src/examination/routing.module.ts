import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CategotyComponent } from './category/category.component';
import { QuestionDetailComponent } from './question/question-detail.component';
import { QuestionComponent } from './question/question.component';
import { ExamenComponent } from './examen/examen.component';
import { ExamenDetailComponent } from './examen/examen-detail.component';
import { CategoryTranslateComponent } from './translates/category_translate/category_translate.component';
import { QuestionTranslateComponent } from './translates/question-translate/question_translate.component';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user/user-detail.component';
import { ExamenSettingComponent } from './dictionaries/examen-setting/examen-setting.component';
import { ExamenSettingDetailComponent } from './dictionaries/examen-setting/examen-setting-detail.component';

import { ExaminationTestingComponent } from './examination-testing/examination-testing.component';
import { ExaminationTestingPreparingComponent } from './examination-testing/examination-testing-preparing.component';

import { MonitoringTableComponent } from './monitoring';
import { ExaminationTestingResultComponent } from './examination-testing/examination-testing-result.component';

import { AdministratorRouteGuardService, EmployeeRouteGuardService } from '../shared/authentication';

const routes: Routes = [
    
    { path: '', component: MonitoringTableComponent },

    { path: 'categories', component: CategotyComponent, canActivate: [AdministratorRouteGuardService] },

    { path: 'questions', component: QuestionComponent, canActivate: [AdministratorRouteGuardService] },
    { path: 'question/:id', component: QuestionDetailComponent, canActivate: [AdministratorRouteGuardService] },
    
    { path: 'examentantions', component: ExamenComponent, canActivate: [AdministratorRouteGuardService] },
    { path: 'examen/:id', component: ExamenDetailComponent, canActivate: [AdministratorRouteGuardService] },

    { path: 'categories_translate', component: CategoryTranslateComponent, canActivate: [AdministratorRouteGuardService]},
    { path: 'questions_translate', component: QuestionTranslateComponent, canActivate: [AdministratorRouteGuardService]},

    { path: 'users', component: UserComponent, canActivate: [AdministratorRouteGuardService] },    
    { path: 'user/:id', component: UserDetailComponent, canActivate: [AdministratorRouteGuardService] },

    { path: 'examensettings', component: ExamenSettingComponent, canActivate: [AdministratorRouteGuardService] },
    { path: 'examensetting/:id', component: ExamenSettingDetailComponent, canActivate: [AdministratorRouteGuardService] },
    
    { path: 'examinationtesting/:id', component: ExaminationTestingComponent, canActivate: [EmployeeRouteGuardService] },
    { path: 'examinationtestingresult/:id', component: ExaminationTestingResultComponent, canActivate: [EmployeeRouteGuardService] },

    { path: '**', component: MonitoringTableComponent },
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes, { useHash: true }) 
    ],
    exports: [ RouterModule ]

})

export class RoutingModule {}