import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { DataService } from 'shared/http-extensions';
import { QuestionTranslate } from '../../models/QuestionTranslate';
import { QuestoinTranslateDto } from '../../models/QuestoinTranslateDto';
import { AnswerTranslate } from '../../models/AnswerTranslate';
import { TranslateRequest } from '../../models/TranslateRequest';

@Injectable()
export class QuestionTranslateService {

  questionApi: string = "api/questions";

  public constructor(private dataService: DataService) { }

  getQuestionsWithTwoLanguages(translateRequest: TranslateRequest)
                      : Promise<QuestoinTranslateDto[]> {
      return this.dataService.callService({
        data: translateRequest,
        method: 'POST',
        url: `${this.questionApi}/translation`
    });  
  }

  createOrUpdateQuestoinTranslate(questionTranslate: QuestionTranslate): Promise<string> {
    return this.dataService.callService({
      data: questionTranslate,
      method: 'POST',
      url: `${this.questionApi}/createOrUpdateQuestoinTranslate`
    });
  }
  
  createOrUpdateAnswerTranslate(answersTranslate: AnswerTranslate): Promise<string> {
    return this.dataService.callService({
      data: answersTranslate,
      method: 'POST',
      url: `${this.questionApi}/createOrUpdateAnswerTranslate`
    });
  }
}