import { Component, OnInit } from "@angular/core";
import { CategoryService } from "../../category/category.service";
import { SelectItem } from "primeng/api";
import { SettingsService } from "../../shared/settings.service";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { QuestionTranslateService } from "./question_translate.service";
import { QuestionTranslate } from "../../models/QuestionTranslate";
import { AnswerTranslate } from "../../models/AnswerTranslate";
import { Answer } from "../../models/Answer";
import { DEFAULT_INTERPOLATION_CONFIG } from "@angular/compiler";
import { TranslateRequest } from "../../models/TranslateRequest";
import { NotificationService } from "shared/services";

@Component({
    templateUrl: './question_translate.component.html'
})

export class QuestionTranslateComponent implements OnInit{
    
    questionTranslateItems : QuestionTranslate [];
    answerTranslateItems : AnswerTranslate [];

    languageItems : SelectItem[];
    languageOnlyRuItem :SelectItem[] = [];
    categories : SelectItem[] = [];

    translateForm : FormGroup;
    
    isOnlyNotTranslated: boolean = true;
    takeCount : number = 10;
    categoryId : string;
    articleNumber : number;
    questionText : string;

    constructor(private questionTranslateService : QuestionTranslateService,
                private settingsService : SettingsService,
                private fb: FormBuilder,
                private categoryService : CategoryService,
                private notificationService: NotificationService){
                    this.createForm();
                }
    
    ngOnInit(): void {
        this.loadLanguages();
        this.getCategoryValues();
    }

    createForm() 
    {
        this.translateForm = this.fb.group({
            fromLanguage : 'ru',
            toLanguage : ''
        });
    }


    private getCategoryValues() : void {        
        this.categoryService.getCategories()
        .then(categories =>{

            if(categories.items)
            {
                this.categories = categories.items
                .filter(r=>r.isCanAddQuestions == true)
                .map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
            }

            this.categories.splice(0,0,{label:"",value:""});
        });
    }

    loadLanguages(){
        this.settingsService.getLanguages()
        .then(languages => {
            languages.forEach(lang=>{
                if(lang.code == "ru"){
                    this.languageOnlyRuItem.push({label : lang.name,value : lang.code});
    
                    this.translateForm.setValue({
                        fromLanguage : 'ru',
                        toLanguage : ''
                    })
                }
            })
            
            this.languageItems = languages.filter(r=>r.code != "ru").map(lang => {                
                return  {
                    label : lang.name,
                    value : lang.code
                };
            });

            this.languageItems.splice(0, 0, {label : "Не выбрано", value : ""});            
        });
    }
    
    aplyFilter()
    {
        var fromLang = this.translateForm.controls.fromLanguage.value;
        var toLang = this.translateForm.controls.toLanguage.value;
        
        if(!toLang)
        {
            this.notificationService.warning("Укажите язык перевода")
        }

        if(fromLang && toLang)
        {                    
            this.questionTranslateItems = [];
            this.answerTranslateItems = [];

            if(fromLang == toLang) return;
            
            var translateRequest = new TranslateRequest();
            translateRequest.fromLanguageCode = fromLang;
            translateRequest.toLanguageCode = toLang;
            translateRequest.takeCount = this.takeCount;
            translateRequest.isOnlyNotTranslated = this.isOnlyNotTranslated;
            translateRequest.categoryId = this.categoryId;
            translateRequest.articleNumber = this.articleNumber;
            translateRequest.questionText = this.questionText;


            this.questionTranslateService.getQuestionsWithTwoLanguages(translateRequest)
            .then(r=>{
                if(r && r.length > 0) {
                    r.forEach(questoinTranslateDto=>{
                        
                            //пройдемся по вопросам
                            questoinTranslateDto.questionTranslates.forEach(questionTranslate =>{

                                var questionFrom = questoinTranslateDto.questionTranslates.filter(r=>r.questionId == questionTranslate.questionId
                                && r.languageCode == fromLang);
                                
                                var questionTo = questoinTranslateDto.questionTranslates.filter(r=>r.questionId == questionTranslate.questionId
                                && r.languageCode == toLang);

                                if(questionFrom.length == 1 && questionTo.length == 1)
                                {                      
                                    questionFrom[0].translateText  = questionTo[0].text
                                }  
                                
                                if(!this.questionTranslateItems.find(r=>r.questionId == questionFrom[0].questionId))
                                {
                                    this.questionTranslateItems.push(questionFrom[0]);
                                }
                                
                            });

                            //пройдемся по ответам
                            questoinTranslateDto.answerTranslates.forEach(answersTranslate=>{                         
                                //переводы ответа
                                answersTranslate.forEach(answerTranslate=>{
                              
                                    if(answerTranslate.languageCode == toLang){
                                        var answer = this.answerTranslateItems.filter(r=>r.answerId == answerTranslate.answerId);

                                        if(answer.length == 1){
                                            answer[0].translateText = answerTranslate.text;
                                        }
                                        else{
                                            this.answerTranslateItems.push(answerTranslate);
                                        }
                                    }
                                    else{                                        
                                        this.answerTranslateItems.push(answerTranslate);
                                    }
                                });
                            });                    
                    })
                }
            });
        }
    }

    onSubmit(questionTranslate : QuestionTranslate){
        
        var updateQuestionTranslate = new QuestionTranslate();
         var res = this.questionTranslateItems.filter(r=>
            r.languageCode == this.translateForm.controls.toLanguage.value
            && r.id == questionTranslate.id
        )
        if(res && res.length == 1)
        {
            updateQuestionTranslate =res[0];
            updateQuestionTranslate.text = questionTranslate.translateText;
        }else{

            updateQuestionTranslate.questionId = questionTranslate.questionId;        
            updateQuestionTranslate.languageCode = this.translateForm.controls.toLanguage.value;
            updateQuestionTranslate.text = questionTranslate.translateText;
        }

        //сохраним вопрос
        this.questionTranslateService.createOrUpdateQuestoinTranslate(updateQuestionTranslate)
        .then(r=>{

            var index = 0;
            this.answerTranslateItems
                .filter(r=>r.questionId == questionTranslate.questionId)
                .forEach(answerTranslate=>{
    
                    var updateAnswerTranslate = new AnswerTranslate();
                    var res = this.answerTranslateItems.filter(r=>
                       r.languageCode == this.translateForm.controls.toLanguage.value
                       && r.id == answerTranslate.id
                   )
                   if(res && res.length == 1)
                    {
                        updateAnswerTranslate = res[0];
                        updateAnswerTranslate.text = answerTranslate.translateText;
                    }
                    else{
                        
                        updateAnswerTranslate.answerId = answerTranslate.answerId;        
                        updateAnswerTranslate.languageCode = this.translateForm.controls.toLanguage.value;
                        updateAnswerTranslate.text = answerTranslate.translateText;
                        this.answerTranslateItems.push(updateAnswerTranslate);
                    }
                    this.questionTranslateService.createOrUpdateAnswerTranslate(updateAnswerTranslate)
                    .then(r=>{
                        index+=1;
                        if(index==this.answerTranslateItems.filter(r=>r.questionId == questionTranslate.questionId).length)
                        {
                            this.aplyFilter();  
                        }
                    });
            })

        });
              
    }
}