import { Component, OnInit } from "@angular/core";
import { CategoryService } from "../../category/category.service";
import { SelectItem } from "primeng/api";
import { SettingsService } from "../../shared/settings.service";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { CategoryTranslate } from "../../models/CategoryTranslate";
import { CategoryTranslateService } from "./category_translate.service";
import { TranslateRequest } from "../../models/TranslateRequest";
import { NotificationService } from "shared/services";

@Component({
    templateUrl: './category_translate.component.html'
})

export class CategoryTranslateComponent implements OnInit{
    
    categoryTranslateItems : CategoryTranslate [];

    languageItems : SelectItem[];
    languageOnlyRuItem :SelectItem[] = [];
    translateForm : FormGroup;

    isOnlyNotTranslated: boolean = true;
    takeCount : number = 10;
    //categoryTitle : number;

    constructor(private categoryTranslateService : CategoryTranslateService,
                private settingsService : SettingsService,
                private fb: FormBuilder,
                private notificationService: NotificationService){
                    this.createForm();
                }
    
    ngOnInit(): void {
        this.loadLanguages();
        
    }

    createForm() 
    {
        this.translateForm = this.fb.group({
            fromLanguage : '',
            toLanguage : ''
        });
    }

    loadLanguages(){
        this.settingsService.getLanguages()
        .then(languages => {
            languages.forEach(lang=>{
                if(lang.code == "ru"){
                    this.languageOnlyRuItem.push({label : lang.name,value : lang.code});
    
                    this.translateForm.setValue({
                        fromLanguage : 'ru',
                        toLanguage : ''
                    })
                }
            })
            
            this.languageItems = languages.filter(r=>r.code != "ru").map(lang => {                
                return  {
                    label : lang.name,
                    value : lang.code
                };
            });

            this.languageItems.splice(0, 0, {label : "Не выбрано", value : ""});            
        });
    }

    aplyFilter()
    {
        var fromLang = this.translateForm.controls.fromLanguage.value;
        var toLang = this.translateForm.controls.toLanguage.value;
        
        if(!toLang)
        {
            this.notificationService.warning("Укажите язык перевода")
        }

        if(fromLang && toLang)
        {                    
            this.categoryTranslateItems = [];

            if(fromLang == toLang) return;
            
            var translateRequest = new TranslateRequest();
            translateRequest.fromLanguageCode = fromLang;
            translateRequest.toLanguageCode = toLang;
            translateRequest.takeCount = this.takeCount;
            translateRequest.isOnlyNotTranslated = this.isOnlyNotTranslated;


            this.categoryTranslateService.getCategoriesWithTwoLanguages(translateRequest)
            .then(r=>{
                if(r && r.length > 0) {
                    r.forEach(translateCategory=>{
                        {
                            var category = this.categoryTranslateItems.filter(r=>r.categoryId== translateCategory.categoryId);
                            var isCategoryExists = category.length == 1;
                            
                            if(isCategoryExists == false)
                            {
                                var categoryies = r.filter(r=>r.categoryId== translateCategory.categoryId);
                                var categoryTranslateItem = categoryies.filter(r=>r.languageCode == fromLang);
                                if(categoryTranslateItem.length == 1)
                                {
                                    var translate = categoryies.filter(r=>r.languageCode == toLang);
                                    if(translate.length == 1)
                                    {
                                        categoryTranslateItem[0].translateText = translate[0].text;
                                    }
                                    this.categoryTranslateItems.push(categoryTranslateItem[0]);
                                }
                            }
                        }                        
                    })
                }
            });
        }
    }

    onSubmit(categoryTranslate : CategoryTranslate){
        
        if(!categoryTranslate.translateText || categoryTranslate.translateText.trim() == ""){
            this.notificationService.warning("Нельзя сохранить пустой перевод");
            return;
        }

        var updateCategoryTranslate = new CategoryTranslate();
         var res = this.categoryTranslateItems.filter(r=>
            r.languageCode == this.translateForm.controls.toLanguage.value
            && r.id == categoryTranslate.id
        )
        if(res && res.length == 1)
        {
            updateCategoryTranslate =res[0];
            updateCategoryTranslate.text = categoryTranslate.translateText;
        }else{

            updateCategoryTranslate.categoryId = categoryTranslate.categoryId;        
            updateCategoryTranslate.languageCode = this.translateForm.controls.toLanguage.value;
            updateCategoryTranslate.text = categoryTranslate.translateText;
        }

        this.categoryTranslateService.createOrUpdateCategoryTranslate(updateCategoryTranslate).then(result=> {
            this.aplyFilter();
        });
    }
}