import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { CategoryTranslate } from '../../models/CategoryTranslate';
import { DataService } from 'shared/http-extensions';
import { TranslateRequest } from '../../models/TranslateRequest';

@Injectable()
export class CategoryTranslateService {

  categoryApi: string = "api/categories";

  public constructor(private dataService: DataService) { }

  getCategoriesWithTwoLanguages(translateRequest: TranslateRequest)
                      : Promise<CategoryTranslate[]> {
      return this.dataService.callService({
        data: translateRequest,
        method: 'POST',
        url: `${this.categoryApi}/translation`
    });  
  }

  createOrUpdateCategoryTranslate(category: CategoryTranslate): Promise<string> {
    return this.dataService.callService({
      data: category,
      method: 'POST',
      url: `${this.categoryApi}/createOrUpdateCategoryTranslate`
    });
  }  
}