$(document).ready(function(){

    $(document).on("click","#toggle-btn", function() {
        $("#sidebarFull").toggleClass("visible");
        $("#sidebarlabaled").toggleClass("visible");

        $(".pusher").toggleClass('push');
      });

    $(document).on("click",".tree-list-item", function() {
        $(".tree-list-item").removeClass("selected");        
        $(this).addClass("selected"); 
    });

    $(".dropdown").dropdown();
    $('.ui.accordion').accordion();
    $('.ui.checkbox').checkbox();
    
    $(document).on("click",".ui.accordion", function() {
        $('.ui.accordion').accordion();
    });
    
    window.openModal = function(id)
    {
        $("#" + id).parent().modal("show");
    }

    $('.message-info').appendTo("body");
    
    $(document).on("click","#aplyExamenSetting", function() {
        setTimeout(function(){
            if($("#examenSettingId").val())
            {
                $("#applyExamentCategoryFilter").trigger("click");
            }
        },1000)
        
    });
    
    $(document).on("click","#show_examination_testing_preparation", function() {
        $('#examination_testing_preparation').modal({            
            closable: false,
            onApprove : function() {
              return false;
            }
          }).modal('show');
    });

    $(document).on("click","#hide_examination_testing_preparation", function() {
        $('#examination_testing_preparation').modal('hide');
    });
    
})
