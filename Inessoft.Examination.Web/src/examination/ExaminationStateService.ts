import { Injectable } from "@angular/core";

@Injectable()

export class ExaminationStateService {
    
    setCurrentQuestionCategoryId(v : string) {localStorage["currentQuestionCategoryId"] = v;}
    getCurrentQuestionCategoryId() { return localStorage.currentQuestionCategoryId; }

    localStorageClear() { localStorage.clear(); }
}
