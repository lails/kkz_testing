import { Component, OnInit, Output, EventEmitter, ViewChild, Input, SimpleChanges, forwardRef} from '@angular/core';
import { Category } from '../models/Category';
import { Observable } from 'rxjs/Observable';
import { RTSelectionEvent, RTSelectionService, SelectionAreaDirective, ListResponse, RTFilterTarget, filter, FilterConfig, List } from 'right-angled';
import { promise } from 'selenium-webdriver';
import { PagedPager } from 'right-angled';
import { Question } from '../models/Question';
import { Answer } from '../models/Answer';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../question/question.service';

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => AnswerTableComponent) }],
    selector : 'answer-table',
    templateUrl : './answer-table.component.html'
})

export class AnswerTableComponent{

    @Output() onAnswersChange = new EventEmitter<Answer[]>();
    
    constructor(private route : ActivatedRoute,
                public questionService : QuestionService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager){}


    getAnswers = (request): Promise<Answer[]> => {       
        const id = this.route.snapshot.paramMap.get('id');
        if(id !== "0")
        {
            return this.questionService.getAnswersByQuestionId(id)            
            .then(answers=>{
                this.onAnswersChange.emit(answers || [])
                return answers;
            });
        }
        else
        {
           return this.questionService.getAnswersByQuestionId(null)
            .then(answers=>{
                this.onAnswersChange.emit(answers || [])
                return answers;
            });
        }
    }


    @ViewChild(SelectionAreaDirective) public selectionArea: SelectionAreaDirective;
    selectedNames :string;

    public getGridSelection(): void {
        this.selectedNames = this.selectionArea.selectionService
                .getSelectedElements()
                .map((c:Question)=>c.questionText)
                .join(";");

        document.getElementById("deleteAnswerConfirm").click();
    }

    deleteItem(list : any, item : Answer)
    {
        var index = list.items.indexOf(item);
        if (index > -1) {
            list.items.splice(index, 1);
        }
        
        this.onAnswersChange.emit(list.items);
    }

    addAnswer(list: any){
        list.items.splice(0,0,new Answer());
    }

    onChangeAnswer(list:any){
        this.onAnswersChange.emit(list.items);
    }
 }