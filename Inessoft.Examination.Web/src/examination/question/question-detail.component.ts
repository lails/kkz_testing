import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Question } from "../models/Question";
import { QuestionService } from "./question.service";
import { SettingsService } from "../shared/settings.service";
import { Language } from "../models/Language";
import { CategoryService } from "../category/category.service";
import {CoreCalendarTranslate} from "base-components/core-calendar-translate/core-calendar-translate"

import {SelectItem} from 'primeng/api';
import { Category } from "../models/Category";
import { Answer } from "../models/Answer";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "shared/services";
import { ExaminationStateService } from "../ExaminationStateService";
import { CoreBreadcrumbItem } from "base-components/core-breadcrumb/CoreBreadcrumbItem";

@Component({
    selector: 'question-detail',
    templateUrl: './question-detail.component.html'
})

export class QuestionDetailComponent implements OnInit {

    questionform: FormGroup;
    question : Question = new Question();
    translate = CoreCalendarTranslate.En;

    categories : SelectItem[] = [];
    answers : Answer[];
    articleNumberRegex: RegExp = /^[0-9]{0,3}$/;
    
    isAnswersValid = true;
    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    constructor(private route : ActivatedRoute,
                private router: Router,
                private questionService : QuestionService,
                private categoryService : CategoryService,
                private fb: FormBuilder,
                private notificationService: NotificationService,
                private examinationStateService: ExaminationStateService)
    {
        this.createForm();
    }
    //#region Работа с формой

    createForm() 
    {
        this.questionform = this.fb.group({
            id: '00000000-0000-0000-0000-000000000000',
            categoryId: [this.examinationStateService.getCurrentQuestionCategoryId(), Validators.required ],
            articleNumber: '',
            isMultipleAnswer: false,
            questionText: ['', Validators.required ]
        });
    }

    setFormValues(){
        const id = this.route.snapshot.paramMap.get('id');
        
        if(id !=="0" )
        {
            this.questionService.getQuestionById(id).then((question:Question)=>{
                this.question = question;
                this.questionform.setValue({
                    id: this.question.id,
                    categoryId: this.question.categoryId || '',
                    articleNumber: this.question.articleNumber || '',
                    isMultipleAnswer:  this.question.isMultipleAnswer || false,
                    questionText:  this.question.questionText || '',
                })
            });
        }
    }

    //----------------------------------------Валидация

    public get categoryIsValid() : boolean {
        return this.questionform.controls.categoryId.errors == null;
    }
    public get questionTextIsValid() : boolean {
        return this.questionform.controls.questionText.errors == null;
    }

    //#endregion

    ngOnInit() : void {
        this.setFormValues();
        this.getCategoryValues();

        const id = this.route.snapshot.paramMap.get('id');
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/questions", "CategoryPageQuestions",false));
        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem(`/question/${id}`,"",true));
    }

    private getCategoryValues() : void {        
        this.categoryService.getCategories()
        .then(categories =>{

            if(categories.items)
            {
                this.categories = categories.items
                .filter(r=>r.isCanAddQuestions == true )
                .map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
            }

            this.categories.splice(0,0,{label:"",value:""});
        });
    }

    onAnswersChange(answers: Answer[]){
        this.answers = answers;  
    }

    validateAnswers ()
    {        var rightQuestionCount = 0;
        var isMultipleAnswers = this.questionform.controls.isMultipleAnswer.value == true;
        
        if(this.answers.length == 0)
        {            
            this.notificationService.warningWithTranslation('SpecMessagePleaceSelecRightAnswer');
            this.isAnswersValid = false;
            return;
        }

        this.answers.forEach(r=>
        {
            if(r.isRight == true)
            {
                rightQuestionCount += 1;
            }
        })

        if(isMultipleAnswers == false && rightQuestionCount > 1)
        {
            this.notificationService.warningWithTranslation('SpecMessagePleaceSelecOnlyOneRightAnswer');
            this.isAnswersValid = false;
        }
        else if(isMultipleAnswers == false && rightQuestionCount == 0)
        {
            this.notificationService.warningWithTranslation('SpecMessagePleaceSelecRightAnswer');
            this.isAnswersValid = false;
        }
        else{
            
            this.isAnswersValid = true;
        }
    }

    onSubmit(){

        if(this.questionform.valid)
        {
            this.validateAnswers();
            if(this.isAnswersValid == false){
                return;
            }

            let request = this.questionform.value;
            if(this.answers.filter(r=>!r.answerText).length > 0)
            {
                this.notificationService.warningWithTranslation('SpecMessagePleaceFillAllAnswersText');
                return;
            }
            request.answers = this.answers;
            debugger;
            if(this.question.isNew && this.question.isNew() == true)
            {
                this.questionService.saveQuestion(request).then(result=> {
                    this.router.navigate(['/question', result]).then(r=>{
                        this.setFormValues();
                        this.notificationService.saved();                        
                    });
                });
            } 
            else {
                this.questionService.updateQuestion(request).then(result=> {
                    this.notificationService.updated();
                });
            }
        }
        else{
            this.notificationService.formNotValid();
        }
    }
}