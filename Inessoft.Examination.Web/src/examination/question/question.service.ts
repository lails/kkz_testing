import { Injectable } from '@angular/core';
import { DataService } from '../../shared/http-extensions/data-service';
import { ListResponse } from 'right-angled';
import { Question } from '../models/Question';
import { Answer } from '../models/Answer';

@Injectable()
export class QuestionService {
  
  public constructor(private dataService: DataService) { }
  
  questionApi = "api/questions";

  getQuestions(request:any): Promise<ListResponse<Question>> {
      return this.dataService.callService({
        data: request,
        method: 'POST',
        url: `${this.questionApi}`
    });  
  }

  getQuestionById(id:string): Promise<Question> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.questionApi}/${id}`
    });
  }

  getAnswersByQuestionId(questionId:string): Promise<Answer[]> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.questionApi}/${questionId}/answers`
    });
  }

    
  saveQuestion(question: Question): Promise<string> {
    return this.dataService.callService({
      data: question,
      method: 'POST',
      url: `${this.questionApi}/create`
    });
  }
  updateQuestion(question: Question): Promise<string> {
    return this.dataService.callService({
      data: question,
      method: 'POST',
      url: `${this.questionApi}/update`
    });
  }
  
  deleteQuestionById(id: string): Promise<string>  {
    return this.dataService.callService({
      method: 'DELETE',
      url: `${this.questionApi}/${id}`
    });
  }
}