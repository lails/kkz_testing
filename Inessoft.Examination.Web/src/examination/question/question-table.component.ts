import { Component, OnInit, Output, EventEmitter, ViewChild, Input, SimpleChanges, forwardRef} from '@angular/core';
import { Category } from '../models/Category';
import { RTSelectionEvent, RTSelectionService, SelectionAreaDirective, ListResponse, RTFilterTarget, filter, FilterConfig } from 'right-angled';
import { promise } from 'selenium-webdriver';
import { PagedPager } from 'right-angled';
import { QuestionService } from './question.service';
import { Question } from '../models/Question';
import { NotificationService } from 'shared/services';
import { CategoryService } from '../category/category.service';
import { SelectItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { ExaminationStateService } from '../ExaminationStateService';
import { CoreBreadcrumbItem } from 'base-components/core-breadcrumb/CoreBreadcrumbItem';

@Component({
    providers: [{ provide: RTFilterTarget, useExisting: forwardRef(() => QuestionTableComponent) }],
    selector : 'question-table',
    templateUrl : './question-table.component.html'
})
export class QuestionTableComponent implements OnInit {

    @Input() selectedCategory : Category;
    pageSize:number  = 10;
    categories : SelectItem[] = [];
    canUploadQuestions : boolean = true;

    coreBreadcrumbItems : CoreBreadcrumbItem[] = [];

    //#region
        @filter(<FilterConfig>{ parameterName: 'categoryId' })        
        public get categoryId() : string {
            return this.examinationStateService.getCurrentQuestionCategoryId();
        }
        public set categoryId(v : string) {
            this.examinationStateService.setCurrentQuestionCategoryId(v);
        }
        
        @filter(<FilterConfig>{ parameterName: 'articleNumber' })
        public articleNumber: string = null;
        @filter(<FilterConfig>{ parameterName: 'questionText' })
        public questionText: string = null;
        
    //#endregion

    constructor(private questionService : QuestionService,
                public selectionService: RTSelectionService,
                public rtPager: PagedPager,
                private categoryService : CategoryService,
                private notificationService: NotificationService,
                private examinationStateService: ExaminationStateService){}

    ngOnInit(): void {
        this.getCategoryValues();

        this.coreBreadcrumbItems.push(new CoreBreadcrumbItem("/questions","CategoryPageQuestions",true));
    }

    getQuestions = (request): Promise<ListResponse<Question>> => {
        return this.questionService.getQuestions(request);
    }
    
    @ViewChild(SelectionAreaDirective) public selectionArea: SelectionAreaDirective;
    selectedQuestionNames :string;

    public getQuestionGridSelection(): void {
        this.selectedQuestionNames = this.selectionArea.selectionService
                .getSelectedElements()
                .map((c:Question)=>c.questionText)
                .join(";");
        document.getElementById("deletQuestionConfirm").click();
    }

    private getCategoryValues() : void {        
        this.categoryService.getCategories()
        .then(categories =>{

            if(categories.items)
            {
                this.categories = categories.items
                .filter(r=>r.isCanAddQuestions == true )
                .map(category => {
                    return  {
                        label : category.name,
                        value : category.id
                    };
                });
            }

            this.categories.splice(0,0,{label:"",value:""});
        });
    }

    onDeleteQuestionConfirmed(canDelete: boolean){
        if(canDelete){
            var selectIds = this.selectionArea.selectionService
                .getSelectedElements()
                .map((c:Question)=>c.id);
            selectIds.forEach(questoinId=>{
                this.deleteItem(questoinId);
            })
            
        }
    }

    deleteItem(questionId) {
        this.questionService.deleteQuestionById(questionId).then(r=>{
            this.notificationService.deleted();
            var el = document.getElementById("aplyfilter");
            el.click();
        });
    }

    ngOnChanges(changes: SimpleChanges) {
    
        if(changes && changes.selectedCategory && changes.selectedCategory.currentValue)
        {
            if(!this.selectedCategory.isNew)
            {
                this.categoryId = this.selectedCategory.id;
                var el = document.getElementById("aplyfilter");
                el.click();
            }
        }
      }
    
    downloadUserDtoTemplate(){
        window.open("api/files/getQuestionDtoTemplate");
    }
    
    onBeforeUploadUsers(e){
        this.canUploadQuestions = false;
    }

    onUpload(response){
        this.notificationService.info(`Загрузка выполнена`);
        this.canUploadQuestions = true;
    }

    onUserUploadError(error){
        this.canUploadQuestions = true;
    }
 }