import { Injectable } from '@angular/core';
import { DataService } from '../../shared/http-extensions/data-service';
import { Language } from '../models/Language';

@Injectable()
export class SettingsService {
  settingApi = "api/settings";

  public constructor(private dataService: DataService) { }

  getLanguages(): Promise<Language[]> {
      return this.dataService.callService({
        method: 'GET',
        url: `${this.settingApi}/getAllLanguages`
    });  
  }
}