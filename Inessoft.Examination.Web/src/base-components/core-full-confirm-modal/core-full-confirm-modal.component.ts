import {Component, Input, Output, EventEmitter} from  '@angular/core';


@Component ({
  selector: 'core-full-confirm-modal',
  templateUrl:'./core-full-confirm-modal.component.html',
  styleUrls: ["./style.css"]
})

export class CoreFullConfirmModalComponent{
    @Input() messageText : string;
    @Input() modalId : string;
    
    @Output() onConfirmedOut = new EventEmitter<boolean>();

    onConfirmed(){      
      this.onConfirmedOut.emit(true);
    }
}