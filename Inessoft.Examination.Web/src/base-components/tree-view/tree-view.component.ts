import {Component, Input, EventEmitter,  OnInit, Output} from  '@angular/core';
import { Category } from '../../examination/models/Category';

@Component ({
  selector: 'tree-view',
  templateUrl:'./tree-view.component.html',
  styleUrls: ['./tree-view.css']
})

export class TreeView implements OnInit{

  @Input() parentid :string;
  @Input() categories : Category[];

  @Input("headCategorySelected")
  public set headCategorySelected(v : boolean) {
      this.selectedCategory = null;
  }
  

  filteredCategory : Category[];
  selectedCategory : Category;
  
  @Output() onSelectOut = new EventEmitter<Category>();

  ngOnInit(): void {
    this.filteredCategory = this.categories.filter(r=>r.parentId == this.parentid);
  } 

  
  selectCategory(category:Category)
  {
    this.selectedCategory = category;
    this.onSelectOut.emit(category);
  }
}