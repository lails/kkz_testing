import {Component, Input, OnInit} from  '@angular/core';



@Component ({
  selector: 'input-field',
  templateUrl:'./input-field.component.html',
  styles:['.field{margin-bottom: 14px;}']
})

export class InputFieldComponent implements OnInit{

    @Input() label : string;
    @Input() type : string;
    @Input() name : string;
    @Input() placeholder : string;
    @Input() value: any;
    
    ngOnInit(): void {
    }
}