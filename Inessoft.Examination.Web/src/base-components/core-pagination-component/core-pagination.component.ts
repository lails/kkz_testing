import {Component, OnInit, Input, ViewChild} from  '@angular/core';
import { RTListsModule, PagedPager, Pager, ListResponse, FiltersService } from 'right-angled';
import { Category } from 'examination/models/Category';


@Component({
  selector: 'core-pagination',
  templateUrl: './core-pagination.component.html',
  styles:[`.pagedInput{ width: 61px; }`,
          `.pagedInput input{ margin: 0px; padding: 6px !important; }`,
          `.pagination.menu{ border: none; -webkit-box-shadow: none !important; box-shadow: none !important;}`,
          `.pagination.menu .item{ position: inherit; box-shadow: none !important;}`,
          `.pagination.menu .item:hover{background:none;color:none;}`,
          `#pagination .item:before {background: none !important;}`
          //`.pagination.menu .item:before { position: inherit; }`
        ]
})

export class CorePaginationComponent {
  @Input() pageSize: number = 10;
}