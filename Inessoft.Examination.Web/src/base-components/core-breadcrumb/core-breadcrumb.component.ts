import {Component, Input, Output, EventEmitter} from  '@angular/core';
import { CoreBreadcrumbItem } from 'base-components/core-breadcrumb/CoreBreadcrumbItem';
import { Router, ActivatedRoute } from '@angular/router';


@Component ({
  selector: 'core-breadcrumb',
  templateUrl:'./core-breadcrumb.component.html',
})

export class CoreBreadcrumbComponent{
    @Input() coreBreadcrumbItems : CoreBreadcrumbItem[];
    
    id = this.route.snapshot.paramMap.get('id');
    constructor(private route : ActivatedRoute,
                private router: Router)
    {}
}