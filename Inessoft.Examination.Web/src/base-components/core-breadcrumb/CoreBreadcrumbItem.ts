export class CoreBreadcrumbItem
{   
    constructor(link,title = "", isActive = false){
        this.link = link;
        this.title = title;
        this.isActive = isActive;
    }
    public title : string;
    public link : string;
    public isActive : boolean;
}