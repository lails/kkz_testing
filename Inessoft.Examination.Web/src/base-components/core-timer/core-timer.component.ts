import { Component, Input, OnInit, SimpleChanges, Output, EventEmitter } from "@angular/core";


@Component({
    selector : 'core-timer',
    templateUrl : 'core-timer.component.html'
})

//Прохождение тестирования
export class CoreTimerComponent{
    @Input() endDateTime : Date;

    @Output() onTimeOver = new EventEmitter();
    
    hours : number;
    minutes : number;
    seconds : number;
    timer: any;

    countdown (){
        var oneHour = 1000 * 60 * 60;
        var oneMinute = 1000 * 60;
        var oneSecond = 1000;

        var curentDateTime =  new Date().getTime();
        var endDateTimeTiks = this.endDateTime.getTime();

        var tiksDifference = endDateTimeTiks - curentDateTime;
            
        var hours = Math.trunc(tiksDifference/oneHour);
        var minutes = Math.trunc(tiksDifference/oneMinute); 
        var seconds = Math.trunc(tiksDifference/oneSecond);

        this.hours = hours;
        this.minutes = minutes - (hours * 60);
        this.seconds = seconds - ((hours * 60 * 60) + (this.minutes * 60));

        if(hours<=0 && minutes <= 0 && seconds <= 0)
        {
            this.hours = 0;
            this.minutes = 0;
            this.seconds = 0;
            this.onTimeOver.emit();
            
            alert("Время вышло!");
            
            if (this.timer) { 
                clearTimeout(this.timer); 
            }
        }
        else
        {
             this.timer = setTimeout(this.countdown.bind(this),1000);
        }        
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.endDateTime && changes.endDateTime.currentValue)
        {
            this.timer = setTimeout(this.countdown.bind(this),1000);
        }
    }
}
