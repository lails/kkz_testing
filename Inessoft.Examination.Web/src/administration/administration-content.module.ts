import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { AdministrationContentComponent } from './administration-content.component';


@NgModule({
    declarations: [
        AdministrationContentComponent
    ],
    imports:[
        BrowserModule,
        FormsModule
    ],
    bootstrap: [AdministrationContentComponent]
})

export class AdministrationContentModule{}