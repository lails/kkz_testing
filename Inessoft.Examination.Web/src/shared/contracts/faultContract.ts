﻿import { FaultLevel } from '../enums/faultLevel';

export interface FaultContract {
    faultMessage: string;
    faultLevel: FaultLevel;
    faultDetails: any;
    error: any;
}
