﻿import { Injectable } from '@angular/core';
import { TranslateService } from '../translate';

declare var alertify: any;

@Injectable()
export class NotificationService {
    private translatedMessages = {};

    public constructor(private transateService: TranslateService) {
        alertify.logPosition("bottom right");
    }

    public error(message: string, title?: string): void {
        alertify.log(`
        <div class="ui error message">
            <div class="ajs-commands">
                <div class="fa fa-close alertify-cancel" onclick="$(this).closest('.default.show').remove()" ></div>
            </div>
            <div style="display: table-cell;">
                <i style="vertical-align: baseline; vertical-align: -webkit-baseline-middle; font-size: 2em;" class="fa fa-check"></i>
            </div>
            <div style="margin-left:20px;">
                <div class="header">${!title ? '' : title}</div>            
                ${message} 
            </div>            
        </div>`, 'error');
    }

    public info(message: string, title?: string): void {
        alertify.log(`
        <div class="ui info message">
            <div class="ajs-commands">
                <div class="fa fa-close alertify-cancel" onclick="$(this).closest('.default.show').remove()" ></div>
            </div>
            <div style="display: table-cell;">
                <i style="vertical-align: baseline; vertical-align: -webkit-baseline-middle; font-size: 2em;" class="fa fa-check"></i>
            </div>
            <div style="margin-left:20px;">
                <div class="header">${!title ? '' : title}</div>            
                ${message} 
            </div>            
        </div>`, 'success');
    }

    public warning(message: string, title?: string): void {
        alertify.log(`
        <div class="ui warning message">
            <div class="ajs-commands">
                <div class="fa fa-close alertify-cancel" onclick="$(this).closest('.default.show').remove()" ></div>
            </div>
            <div style="display: table-cell;">
                <i style="vertical-align: baseline; vertical-align: -webkit-baseline-middle; font-size: 2em;" class="fa fa-exclamation-triangle"></i>
            </div>
            <div style="margin-left:20px;">
                <div class="header">${!title ? '' : title}</div>            
                ${message} 
            </div>            
        </div>`, 'warning');
    }

    public success(message: string, title?: string): void {
        alertify.log(`
        <div class="ui success message">
            <div class="ajs-commands">
                <div class="fa fa-close alertify-cancel" onclick="$(this).closest('.default.show').remove()" ></div>
            </div>
            <div style="display: table-cell;">
                <i style="vertical-align: baseline; vertical-align: -webkit-baseline-middle; font-size: 2em;" class="fa fa-check"></i>
            </div>
            <div style="margin-left:20px;">
                <div class="header">${!title ? '' : title}</div>            
                ${message} 
            </div>            
        </div>`, 'success');
    }
    close(){
        debugger;
    }
    
    public errorWithTranslation(key: string): void {
        this.withTranslation((message) => this.error(message), key);
    }

    public infoWithTranslation(key: string): void {
        this.withTranslation((message) => this.info(message), key);
    }

    public warningWithTranslation(key: string): void {
        this.withTranslation((message) => this.warning(message), key);
    }

    public successWithTranslation(key: string): void {
        this.withTranslation((message) => this.success(message), key);
    }

    public saved(): void {
        this.successWithTranslation('MessageSavedSuccess');
    }

    public updated(): void {
        this.successWithTranslation('MessageUpdatedSuccess');
    }

    public deleted(): void {
        this.warningWithTranslation('MessageDeletedSuccess');
    }

    public canceled(): void {
        this.warningWithTranslation('MessageCanceledSuccess');
    }

    public notAccessToCreate(): void {
        this.errorWithTranslation('MessageNotAccessToCreate');
    }

    public notAccessToEdit(): void {
        this.errorWithTranslation('MessageNotAccessToUpdate');
    }

    public formNotValid(): void {
        this.errorWithTranslation('MessageFormNotValid');
    }

    public notAllFieldsValid(): void {
        this.warningWithTranslation('MessageNotAllFieldsValid');
    }

    public deletingIsAnavailable(): void {
        this.warningWithTranslation('MessageDeletingIsAnavailable');
    }

    public maxFilesCount(count: number): void {
        this.withTranslation((message) => this.warning(`${message} ${count}`), 'FILE_UPLOAD_MAX_FILE_COUNT');
    }

    public maxFilesCountAtTime(count: number): void {
        this.withTranslation((message) => this.warning(message), 'FILE_UPLOAD_MAX_FILE_COUNT_AT_TIME', [count.toString()]);
    }

    private withTranslation(action: (message: string) => void, key: string, params?: string[]): void {
        if (this.transateService) {
            if (!this.translatedMessages[key]) {
                var translateResult =  this.transateService.instant(key);
                action(this.replaceParameters(translateResult, params));
            } else {
                let text = this.translatedMessages[key];
                action(this.replaceParameters(text, params));
            }
        }
    }

    private replaceParameters(template: string, params: string[]): string {
        let result = template;
        if (params && params.length) {
            for (let i = 0; i < params.length; i++) {
                let regExp = new RegExp('\\{' + i + '\\}', 'gm');
                template = template.replace(regExp, params[i]);
            }
            result = template;
        }
        return result;
    }
}
