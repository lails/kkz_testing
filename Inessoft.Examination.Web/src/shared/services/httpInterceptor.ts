﻿import { Injectable } from '@angular/core';
import { HttpEvent, HttpClient, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { LoginFailException, FaultException } from '../classes';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

@Injectable()
export class Interceptor implements HttpInterceptor {
    private _sessionExpired = false;
    
    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let interceptor = this;
        const newReq = req.clone(
            {
                headers: req.headers.append('X-Requested-With', 'XMLHttpRequest').append('Content-Type', 'application/json; charset=utf-8')
            });
        return next.handle(newReq).catch(response => {
            let err = (<HttpErrorResponse>response);
            if (err.status === 401) {
                if (interceptor._sessionExpired) {
                    setTimeout(() => { throw new LoginFailException(); }, 0);
                    return Observable.throw(err);
                } else {
                    return interceptor.refreshToken(newReq.method.toLowerCase(), next, err, newReq);
                }
            } else if (err.status === 400 && err.error && err.error.faultContract) {
                setTimeout(() => { throw new FaultException(err.error.faultContract); }, 0);
                return Observable.throw(err);
            } else {
                return Observable.throw(err);
            }
        });
    }

    private refreshToken(fname: string, next: HttpHandler, originError: any, args: any): Observable<any> {
                let client = new HttpClient(next);
                return new Observable<any>((subscriber) => {
                    let iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.setAttribute('src', '/Home/RefreshToken');
                    document.body.appendChild(iframe);
                    iframe.onload = () => {
                        setTimeout(() => {
                            try {
                                if (iframe) {
                                    const childWindow = iframe.contentWindow;
                                    // The onload callback runs twicely in Chrome
                                    if (childWindow && childWindow.document) {
                                        const title = childWindow.document.title;
                                        if (title === 'S7_INT_PARTNERS') {
                                            client.request(args).subscribe(
                                                response => {
                                                    subscriber.next(response);
                                                    subscriber.complete();
                                                },
                                                error => {
                                                    subscriber.error(error);
                                                });
                                        } else {
                                            throw new Error();
                                        }
                                    }
                                }
                            } catch (e) {
                                subscriber.error(originError);
                                this._sessionExpired = true;
                                throw new LoginFailException();
                            } finally {
                                // IE crashes when calling iframe.remove() and raise an exception "Object doesn't support property or method 'remove'"
                                if (iframe && iframe.parentNode) {
                                    iframe.parentNode.removeChild(iframe);
                                    iframe = null;
                                }
                            }
                        }, 1000);
                    };
                });
            }
}
