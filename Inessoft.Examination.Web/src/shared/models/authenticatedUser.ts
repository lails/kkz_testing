export class AuthenticatedUser {
    userName: string;
    iin : string;
    bearerToken: string;
    canAccessAdministration: boolean;
    canAccessExamination: boolean;
}