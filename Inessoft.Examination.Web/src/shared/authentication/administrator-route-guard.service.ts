import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SecurityService } from './securityService';

@Injectable()
export class AdministratorRouteGuardService implements CanActivate {
  constructor(public securityService: SecurityService, public router: Router) {}
  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      this.securityService.isUserInRole(SecurityService.AdministratorRole)
        .then((result) => {
          resolve(result);
        })
        .catch(err => {
          resolve(true);
        });
      })
  }
}