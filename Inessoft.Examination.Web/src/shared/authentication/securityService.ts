import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { DataService } from '../http-extensions/data-service';
import { AuthenticatedUser } from '../models';
import { debug } from 'util';

@Injectable()
export class SecurityService {
    
    securityApi: string = "api/security";
    securityUser: string = "securityUser";

    public static AdministratorRole = "Administrator";
    public static EmployeeRole = "Employee";
    public static ChairmanRole = "Chairman";

    public constructor(private dataService: DataService) { }

    getAuthenticatedUser(): Promise<AuthenticatedUser> {
       return this.dataService.callService( {
            method: 'GET',
            url: `${this.securityApi}/user-data`
        });
    }

    logout(): Promise<void> {
        return this.dataService.callService( {
            method: 'GET',
            url: `${this.securityApi}/logout`
        });
    }

    
    getCurrentUserData(): AuthenticatedUser { 
        if(localStorage["securityUser"])
        {
            return JSON.parse(localStorage["securityUser"]); 
        }else{
            return null;
        }
    }

    setCurrentUserData(securityUser : AuthenticatedUser): void {
         localStorage.setItem(this.securityUser,  JSON.stringify(securityUser));
    }

    setCanAccessAdministration(canAccess: boolean): void {        
        var userData = this.getCurrentUserData();
        if(userData)
        {
            userData.canAccessAdministration = canAccess;
            this.setCurrentUserData(userData);
        }        
    }
    
    setCanAccessExamination(canAccess: boolean): void {        
        var userData = this.getCurrentUserData();
        if(userData)
        {
            userData.canAccessExamination = canAccess;
            this.setCurrentUserData(userData);
        }
    }

    isUserInRole(roleName: string): Promise<boolean> {
        return this.dataService.callService( {
            method: 'GET',
            url: `${this.securityApi}/user/current/in-role/${roleName}`
        });
    }

    hasAdministratorAccess(): boolean {
        var userData = this.getCurrentUserData();
        if(userData)
        {
            return this.getCurrentUserData().canAccessAdministration;
        }
        return false;
    }

    hasEmployeeAccess(): boolean {
        var userData = this.getCurrentUserData();
        if(userData)
        {
            return this.getCurrentUserData().canAccessExamination;
        }
        return false;
    }
}