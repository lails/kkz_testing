﻿export { TranslatePipe } from './translate.pipe';
export { TranslateService } from './translate.service';
import { TranslateService } from './translate.service';
import { translations } from './translations';
export { translations } from './translations';
import { enTranslate } from './i18n/en';
import { ruTranslate } from './i18n/ru';
import { kzTranslate } from './i18n/kz';

export const APP_TRANSLATE_PROVIDERS = [
  { provide: translations, useValue: { en: enTranslate, ru: ruTranslate, kz: kzTranslate } },
  TranslateService
];

