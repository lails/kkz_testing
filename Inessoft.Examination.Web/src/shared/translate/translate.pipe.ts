﻿import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from './translate.service'; // our translate service

/**Пайп для перевода текста из ресурсных файлов */
@Pipe({
    name: 'translate'
})
export class TranslatePipe implements PipeTransform {
    constructor(public translate: TranslateService) { }

    transform(value: string): string {
        if (value) {
            return this.translate.instant(value);
        }
        return value;
    }
}
