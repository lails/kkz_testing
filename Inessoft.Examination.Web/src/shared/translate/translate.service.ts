﻿import { Injectable, Inject } from '@angular/core';
import { Cookie } from 'ng2-cookies';
import { translations } from './translations';

/**Сервис для работы с переводами текста */
@Injectable()
export class TranslateService {
    private _default_language_code = 'ru';
    private _language_cookie = 'system_language';
    private _currentLang = Cookie.check(this._language_cookie) ? Cookie.get(this._language_cookie) : this._default_language_code;

    constructor( @Inject(translations) private translations: any) {
    }

    /**Текущий язык системы */
    get currentLang() {
        return this._currentLang;
    }

    /**Установить язык системы */
    use(lang: string) {
        Cookie.set(this._language_cookie, lang);
        location.reload();
        this._currentLang = lang;
    }

    /**Получить значение из коллекции переводов для текущего языка */
    instant(key: string, options?: any) {
        if (this.translations[this.currentLang] && this.translations[this.currentLang][key]) {
            let translateResult = this.translations[this.currentLang][key];
            if (options) {
                Object.getOwnPropertyNames(options).forEach((propertyName) => {
                    translateResult = translateResult.replace(`{{${propertyName}}}`, options[propertyName]);
                });
            }
            return translateResult;
        }
        return key;
    }
}
