﻿export const ruTranslate = {
    'ButtonSave':'Сохранить',
    'ButtonCreate':'Создать',
    'ButtonAdd':'Добавить',
    'ButtonDelete':'Удалить',
    'ButtonClear':'Очистить',
    'ButtonReset':'Сбросить',
    'ApplyFilter':'Применить фильтр',
    'Apply':'Применить',
    'Filter':'Фильтр',
    'Exit':'Выйти',
    
    'BreadCrumbCreate':'Создание',
    'BreadCrumbEdit':'Редактирование',

    'PaginationFrom':'Строки с',
    'PaginationTo':'по',
    'PaginationOf':'из',
    'PaginationShowRows':'Строк на странице',

    'MessageSavedSuccess':'Запись успешно сохранена',
    'MessageUpdatedSuccess':'Запись успешно обновлена',
    'MessageDeletedSuccess':'Запись успешно удалена',
    'MessageCanceledSuccess':'Действие отменено',
    'MessageNotAccessToCreate':'Нет прав на создание',
    'MessageNotAccessToUpdate':'Нет прав на обновление',
    'MessageFormNotValid':'Не все условия формы выполнены',
    'MessageNotAllFieldsValid':'Не все поля заполнены',
    'MessageDeletingIsAnavailable':'Удаление не возможно',

    'SpecMessageSetRightDataForCategoriesTable':'Укажите верные данны для таблицы категорий экзаменов',
    'SpecMessagePleaceSelectedExam':'Пожалуйста, выберите экзамен',
    'SpecMessagePleaceAcceptAgreementBySelectedExam':'Необходимо подтвердить согласие по выбранному экзамену',
    'SpecMessagePleaceFillAllAnswersText':'Пожалуйтса, внесите текст в все ответы',
    'SpecMessagePleaceSelecRightAnswer':'Необходимо указать минимум один верный ответ',
    'SpecMessagePleaceSelecOnlyOneRightAnswer':'Необходимо указать только один верный ответ',
    'SpecMessageExaminationCategoriesNotValid':'Для назначения тестов использованы разные родительские категорий',
    'SpecMessageCategoriesTableDataNotValid':'Не верные данны для таблицы категорий',

    'MenuMain':'Главная',
    'MenuStartExamination':'Начать тестирование',
    'MenuAdministration':'Администрирование',
    'MenuCategories':'Категории',
    'MenuQuestions':'Вопросы',
    'MenuExamination':'Экзамены',
    'MenuTranslations':'Переводы',
    'MenuReports':'Отчеты',
    'MenuMonthTestReport':'Отчет по тестированию в разрезе сдаших экзамен за текущий месяц',
    'MenuCategoriesReport':'Отчет по вопросам в разрезе категорий',
    'MenuDictionaries':'Справочники',
    'MenuExaminationSettings':'Настройки тестов',
    'MenuUsers':'Пользователи',

    'QuestionPageHeader':'Вопрос',
    'QuestionPageCategiry':'Категория',
    'QuestionPageArticleName':'Номер статьи',
    'QuestionPageMultipleSelect':'Множественный выбор',
    'QuestionPageText':'Текст вопроса',
    'QuestionPageAnswers':'Ответы',
    'QuestionPageAnswersText':'Текст ответа',
    'QuestionPageAnswerRight':'Верный',

    'CategoryPageCategories':'Категории',
    'CategoryPageParentCategory':'Родительская категория',
    'CategoryPageTitle':'Наименование',
    'CategoryPageCanAddQuestion':'Разрешено добавлять вопросы',
    'CategoryPageQuestions':'Вопросы',
    'CategoryPageArticleNumber':'Номер статьи',
    'CategoryPageQuestionText':'Текст вопроса',
    'CategoryPageExamProvider':'Поставщик экзаменов',
    'CategoryPageProhibitedAddingQuestions':'Добавление вопросов запрещено',

    'MonitoringPageHeader':'Мониторинг',
    'MonitoringExaminationFilter':'Экзамен для мониторинга',
    'MonitoringExaminationUsers':'Пользователи экзамена',
    'MonitoringUserName':'Пользователь',
    'MonitoringStartDateFact':'Дата начала (факт)',
    'MonitoringEndDateFact':'Дата завершения (факт)',
    'MonitoingState':'Статус',
    'MonitoringExaminationPoints':'Оценка',
    'MonitoringOralExaminationPoints':'Оценка за устный экзамен',
    'MonitoringComment':'Комментарий',

    'IndividualReportTitle':'СПРАВКА<br/>о результатах сдачи квалификационного<br/>экзамена на должность судьи',
    'IndividualReportUserName':'Претендент',
    'IndividualReportPassedDate':'Дата сдачи',
    'IndividualReportCategoryName':'Наименование тем',
    'IndividualReportQuestionCount':'Количество вопросов',
    'IndividualReportRightAnswersCount':'Количество правильных ответов',
    'IndividualReportExaminationResult':'Результат сдачи компьютерного теста на знание законодательства',
    'IndividualReportPoints':'балл',
    'IndividualReportOralExaminationResult':'Результат проверки знаний по экзаменационным билетам',
    'IndividualReportTotalExaminationResult':'Результат квалификационного экзамена',
    'IndividualReportText':'Срок действия справки действителен в течение четырех лет со дня сдачи экзамена',
    'IndividualReportChairman':'Председатель Квалификационной комиссии при Высшем Судебном Совете Республики Казахстан',

    'ExamExams':'Экзамены',
    'ExamExam':'Экзамен',
    'ExamExamTitle':'Наименование экзамена',
    'ExamExamDateStart':'	Дата начала',
    'ExamExamDateEnd':'Дата завершения',
    'ExamExamChairMan':'Председатель',
    'ExamExamSettings':'Настройки экзамена',
    'ExamExamSetting':'Настройка экзамена',
    'ExamExamSettingTitle':'Наименование Настройки экзамена',
    'ExamExamDuration':'Время экзамена(мин)',
    'ExamExamQuestionCount':'Количество вопросов',    
    'ExamExamRequiredQuestionCount':'Проходной бал',    
    'ExamExamPointFiveFrom':'Оценка 5 от',    
    'ExamExamPointFourFrom':'Оценка 4 от',
    'ExamExamPointThreFrom':'Оценка 3 от',    
    'ExamExamSumQuestion':'Вопросов всего',
    'ExamExamQuesionCount':'Вопросов для экзамена (шт)',
    'ExamExamQuestionPercent':'Вопросов для экзамена (%)',
    'ExamExamSum':'Всего',    
    'ExamExamUsers':'Участники',
    'LoadUsers':'Загрузить пользователей',
    'LoadQuestions':'Загрузить вопросы',
    'TemplateDownload':'Скачать шаблон',
    'UserAvailableUsers':'Доступные участники',
    'UserSelectedUsers':'Выбранные участники',
    'CantAddUsersMessage':'Не удалось добавить следующих пользователей',

    'Translate':'Перевод',
    'TranslateCategories':'Переводы для категорий',
    'TranslateQuestions':'Переводы для вопросов',
    'TranslateOnlyNotTranslated':'Только не переведенные',
    'TranslatePerPage':'Переводов за раз',

    'Users':'Пользователи',
    'User':'Пользователь',
    'UserFIO':'ФИО',
    'UserContacts':'Контакты',
    'UserInfo':'Информация о пользователе',
    'UserIIN':'ИИН',
    'UserActions':'Действия',
    'UserMidleName':'Фамилия',
    'UserFirstName':'Имя',
    'UserSecondName':'Отчество',
    'UserBirthday':'Дата рождения',
    'UserPosition':'Должность',
    'UserRole':'Роль',
    'UserAddress':'Адресс',
    'UserPhone':'Номер телефона',
    'UserEmail':'Email',
    'UserActived':'Активирован',
    'UserDeleted':'Удален',
};
