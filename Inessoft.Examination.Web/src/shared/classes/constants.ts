﻿export class Constants {
    public static residentCountry = 'RU';
    public static cacheLifeTime = 10;
    public static s7Code = 'S7';
    public static validationError = 'server_validation_errors';
    public static routeMaxLength = 10;
    public static maxFileSize: number = 2 * 1024 * 1024;
    public static dateFormat = 'YYYY-MM-DD';
    public static maxGeographyCount = 3;
}
