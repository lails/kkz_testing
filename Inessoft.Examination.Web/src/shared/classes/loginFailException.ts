﻿import { BaseError } from './baseError';

export class LoginFailException extends BaseError {
    public constructor() {
        super('Login failed.');
        // Set the prototype explictilly: https://github.com/Microsoft/TypeScript/issues/12660
        Object.setPrototypeOf(this, LoginFailException.prototype);
    }
}
