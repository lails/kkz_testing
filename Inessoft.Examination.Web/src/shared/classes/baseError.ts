﻿// Note: http://learn.javascript.ru/oop-errors
export class BaseError extends Error {
    public message: string;
    public stack: any;

    public constructor(message?: string) {
        super(message);

        this.message = message;

        if ((Error as any).captureStackTrace) {
            (Error as any).captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error() as any).stack;
        }

        // Set the prototype explictilly: https://github.com/Microsoft/TypeScript/issues/12660
        Object.setPrototypeOf(this, BaseError.prototype);
    }
}
