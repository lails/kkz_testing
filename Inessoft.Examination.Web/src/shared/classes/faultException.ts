﻿import { BaseError } from './baseError';
import { FaultContract } from '../contracts/faultContract';

export class FaultException extends BaseError {
    public faultContract: FaultContract;

    public constructor(faultContract: FaultContract) {
        super(faultContract.faultMessage);
        this.faultContract = faultContract;
        // Set the prototype explictilly: https://github.com/Microsoft/TypeScript/issues/12660
        Object.setPrototypeOf(this, FaultException.prototype);
    }
}
