import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest, HttpEvent, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import './operators/cancelon.augmentation';
import { RequestSettings } from './request-settings';
import { NotificationService } from '../services';
import { FaultLevel } from '../enums';
import { Constants } from '../classes';
import { FaultContract } from '../contracts';

@Injectable()
export class DataService extends Subject<any> {
    public readonly errorStream: Subject<HttpErrorResponse> = new Subject<HttpErrorResponse>();

    private _maxErrorMessageLength = 500;

    private customErrorHandlers: Array<(error: any, action: (text: string) => void) => boolean> = [];

    protected disposeEvent: EventEmitter<void> = new EventEmitter<void>();

    constructor(public http: HttpClient, private _notify: NotificationService) {
        super();
        this.faultHandler = this.faultHandler.bind(this);
        this.customErrorHandlers.push(this.notFoundHandler);
        this.customErrorHandlers.push(this.internalServerErrorHandler);
        this.customErrorHandlers.push(this.serviceUnavailableHandler);
        this.customErrorHandlers.push(this.badGatewayHandler);
        this.customErrorHandlers.push(this.gatewayTimeoutHandler);
    }

    public callService<T>(settings: RequestSettings): Promise<T> {
        let httpRequest = this.toHttpRequest<T>(settings);
        let r = <Observable<T>>this.http.request(httpRequest).map(response => {
            this.next(response);
            return this.parseResponse(response);
        })
        .catch(this.faultHandler)
        .cancelOn(this.disposeEvent);

        return r.toPromise<T>();
    }

    public dispose(): void {
        this.disposeEvent.emit();
        this.errorStream.complete();
        this.complete();
    }
    
    protected faultHandler(response: HttpErrorResponse): Observable<any> {
        this.errorStream.next(response);
        try {
            let errorMessage: string;
            let error = response.error;
            let faultContract: FaultContract = error && error.faultContract as FaultContract || null;

            errorMessage = faultContract && faultContract.faultMessage || response.message;
            if (errorMessage.length > this._maxErrorMessageLength) {
                errorMessage = errorMessage.substring(0, this._maxErrorMessageLength) + '...';
            }

            if (errorMessage !== Constants.validationError) {
                let textToShow = errorMessage;
                let showDialog = text => this._notify.error(text);
                if (faultContract) {
                    switch (error.faultContract.faultLevel) {
                        case FaultLevel.Error:
                        case FaultLevel.Fatal:
                            showDialog = text => this._notify.error(text);
                            break;
                        case FaultLevel.Warning:
                            showDialog = text => this._notify.warning(text);
                            break;
                        default:
                            break;
                    }
                    console.log(faultContract.error);
                    showDialog(textToShow);
               } else {
                   showDialog = text => this._notify.errorWithTranslation(text);
                   for (let handler of this.customErrorHandlers) {
                        if (handler(response, showDialog)) {
                            break;
                        }
                   }
               }
            } else {
                this._notify.formNotValid();
            }
        } catch (e) {
            console.log(e);
         }

        return Observable.throw(response);
    }

    protected notFoundHandler(error: any, action: (text: string) => void): boolean {
        if (error && error.status && error.status === 404) {
            action('COMMON_ERROR_NOTFOUND');
            return true;
        }
        return false;
    }

    protected internalServerErrorHandler(error: any, action: (text: string) => void): boolean {
        if (error && error.status && error.status === 500) {
            action('COMMON_ERROR_INTERNALSERVERERROR');
            return true;
        }
        return false;
    }

    protected serviceUnavailableHandler(error: any, action: (text: string) => void): boolean {
        if (error && error.status && error.status === 503) {
            action('COMMON_ERROR_SERVICEUNAVAILABLE');
            return true;
        }
        return false;
    }

    protected badGatewayHandler(error: any, action: (text: string) => void): boolean {
        if (error && error.status && error.status === 502) {
            action('COMMON_ERROR_BADGATEWAY');
            return true;
        }
        return false;
    }

    protected gatewayTimeoutHandler(error: any, action: (text: string) => void): boolean {
        if (error && error.status && error.status === 504) {
            action('COMMON_ERROR_GATEWAYTIMEOUT');
            return true;
        }
        return false;
    }

    private parseResponse<T>(response: HttpResponse<T> | HttpErrorResponse | HttpEvent<T>): T | string {
        if (response instanceof HttpResponse) {
            return response.body;
        }
        if (response instanceof HttpErrorResponse) {
            return response.message;
        }
        return null;
    }

    private toHttpRequest<T>(settings: RequestSettings): HttpRequest<T> {
        let method: string;
        let url: string;
        let body: T;
        let params: HttpParams = new HttpParams();
        let headers: HttpHeaders;

        method = settings.method.toString();
        url = settings.url;

        if (!!settings.data) {
            if (settings.method === 'GET') {
                this.objectToParams(settings.data).map(x => {
                    params = params.append(x.key, x.val);
                });
            } else {
                body = settings.data;
            }
        }

        if (!!settings.headers) {
            headers = new HttpHeaders(settings.headers);
        }

        return new HttpRequest<T>(method, url, body, {headers: headers, params: params});
    }

    private objectToParams(obj: any, prefix?: string): ParamItem[] {
        let parameters: ParamItem[] = [];
        if (this.isPrimitive(obj)) {
            parameters.push({key: prefix, val: obj});
        } else {
            for (let prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    let value: any = obj[prop];
                    if (value !== null && value !== void 0) {
                        if (typeof value !== 'object') {
                            let key = !prefix ? prop : `${prefix}[${prop}]`;
                            let val = value;
                            parameters.push({key: key, val: val});
                        } else {
                            if (value instanceof Array) {
                                let additionalParameters: ParamItem[][] = value.map((e: string, idx: number) => this.objectToParams(e, !prefix ? `${prop}[${idx}]` : `${prefix}[${prop}][${idx}]`));
                                additionalParameters.map(x => {
                                    x.map(y => {
                                        parameters.push(y);
                                    });
                                });
                            } else {
                                let additionalParameters = this.objectToParams(value);
                                additionalParameters.map(x => {
                                    parameters.push(x);
                                });
                            }
                        }
                    }
                }
            }
        }
        return parameters;
    }

    private isPrimitive(obj: any): boolean {
        return obj === null ||
            obj === void 0 ||
            typeof obj === 'number' ||
            typeof obj === 'boolean' ||
            typeof obj === 'string';
    }
}

interface ParamItem {
    key: string;
    val: string;
}
