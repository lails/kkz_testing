import { DataService } from './data-service';

export abstract class CachedService extends DataService {
    private _cache: { [key: string]: any } = {};

    public wrapWithCache<T extends Function>(fn: T, key: string, expirationPolicy: () => Date | string, withArgs?: boolean): T {
        return <any>(function (...args: any[]): Promise<any> {
            let cacheKey = !!withArgs ? key + '__' + JSON.stringify(args) : key;

            let promise = this.getCacheValue(cacheKey) as Promise<any>;
            if (promise === null) {
                promise = fn.apply(this, args) as Promise<any>;
                promise.catch(() => {
                    this.removeCacheEntry(key);
                });
                this.setCacheValue(cacheKey, promise, expirationPolicy());
            }
            return promise;
        });
    }

    public setCacheValue(key: string, value: any, expiresAt: Date | string): void {
        this._cache[key] = { expiresAt: expiresAt, value: value };
    }

    public getCacheValue(key: string): any {
        let val = this._cache[key];
        if (typeof val === 'undefined' || val === null) {
            return null;
        }

        let now = new Date();
        if ((val.expiresAt.valueOf() - now.valueOf()) < 0) {
            delete this._cache[key];
            return null;
        }

        return val.value;
    }

    public removeCacheEntry(key: string): void {
        Object.keys(this._cache)
            .filter((prop: string) => {
                return prop === key || prop.indexOf(key + '__') === 0;
            })
            .forEach((prop: string) => {
                delete this._cache[prop];
            });
    }

    public dispose(): void {
        super.dispose();
        this._cache = null;
    }
}
