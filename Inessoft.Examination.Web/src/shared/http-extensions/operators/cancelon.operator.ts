import { Observable } from 'rxjs/Observable';
import { Operator } from 'rxjs/Operator';
import { Subscriber } from 'rxjs/Subscriber';

import { CancelOnSubscriber } from './cancelon.subscriber';

export class CancelOnOperator<T, R> implements Operator<T, R> {
    private event: Observable<any>;

    constructor(event: Observable<any>) {
        this.event = event;
    }

    public call(subscriber: Subscriber<R>, source: any): any {
        return source.subscribe(new CancelOnSubscriber(subscriber, this.event));
    }
}
