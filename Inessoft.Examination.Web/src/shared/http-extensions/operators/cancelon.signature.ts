import { Observable } from 'rxjs/Observable';

/* tslint:disable:interface-name */
export interface CancelOnSignature {
    <T>(event: Observable<any>): Observable<T>;
}
/* tslint:enable */
