export interface RequestSettings {
    url: string;
    method?: string;
    headers?: string | { [name: string]: string | string[] };
    data?: any;
}
