﻿namespace Inessoft.Examination.Authentication
{
    public static class UserRoles
    {
        public const string Administrator = nameof(Administrator);
        public const string Employee = nameof(Employee);
        public const string Chairman = nameof(Chairman);
    }
}