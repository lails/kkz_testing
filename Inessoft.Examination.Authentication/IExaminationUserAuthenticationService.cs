﻿using Inessoft.Examination.Authentication.UserIdentity;

namespace Inessoft.Examination.Authentication
{
    public interface IExaminationUserAuthenticationService
    {
        IUserIdentity Identity { get; }
    }
}