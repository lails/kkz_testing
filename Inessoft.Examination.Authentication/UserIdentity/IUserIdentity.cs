﻿using System;
using System.Collections.Generic;

namespace Inessoft.Examination.Authentication.UserIdentity
{
    public interface IUserIdentity
    {
        Guid UserId { get; }

        string UserXin { get; }

        string Name { get; }

        string CommonName { get; }

        string Email { get; }

        bool IsInRole(string role);

        bool IsInRoles(List<string> roles);

        bool IsInRole(string[] roles);

        bool IsAuthenticated { get; set; }

        string SystemLanguageCode { get; }
    }
}