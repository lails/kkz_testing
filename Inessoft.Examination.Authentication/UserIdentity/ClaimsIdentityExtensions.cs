﻿using System;
using System.Linq;
using System.Security.Claims;

namespace Inessoft.Examination.Authentication.UserIdentity
{
    public static class ClaimsIdentityExtensions
    {
        public static string GetClaimValue(this ClaimsIdentity identity, string key)
        {
            return identity.Claims?.FirstOrDefault(e => e.Type == key)?.Value;
        }

        public static Guid GetUserId(this ClaimsIdentity identity)
        {
            return Guid.Parse(identity.Claims?.FirstOrDefault(e => e.Type == ClaimTypes.NameIdentifier)?.Value ?? throw new InvalidOperationException(nameof(GetUserId)));
        }
    }
}