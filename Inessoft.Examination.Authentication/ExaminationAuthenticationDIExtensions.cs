﻿using Microsoft.Extensions.DependencyInjection;

namespace Inessoft.Examination.Authentication
{
    public static class ExaminationAuthenticationDiExtensions
    {
        public static IServiceCollection AddAuthenticationDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IExaminationUserAuthenticationService, ExaminationUserAuthenticationService>();

            return serviceCollection;
        }
    }
}