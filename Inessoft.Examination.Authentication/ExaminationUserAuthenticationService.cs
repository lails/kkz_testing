﻿using System.Security.Claims;
using Inessoft.Examination.Authentication.UserIdentity;
using Microsoft.AspNetCore.Http;

namespace Inessoft.Examination.Authentication
{
    public class ExaminationUserAuthenticationService: IExaminationUserAuthenticationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ExaminationUserAuthenticationService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IUserIdentity Identity => new ClaimBasedUserIdentity(_httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity);
    }
}